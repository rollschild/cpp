# Runtime Polymorphism

- **Polymorphism**
  - write once; reuse with different types
- Two kinds of polymorphism in C++
  - **compile-time**
  - **runtime**
- **Interface**
  - a share boundary that contains no data or code
  - defines function signatures that all implementations of the interface agrees to support
  - there is no `interface` keyword in C++; you have to define interfaces using the antiquated inheritance mechanisms
- **Object Composition**
  - a design pattern where a class contains members of other class types

## Interfaces

### Base Class Inheritance

```c++
struct DerivedClass : BaseClass {
  // ...
}
```

- derived class references can be treated as if they were of base class reference type

```c++
struct BaseClass {};
struct DerivedClass : BaseClass {};
void are_belong_to_us(BaseClass& base) {}

int main() {
  DerivedClass derived;
  are_belong_to_us(derived); // this works well
}
```

### Member Inheritance

- derived classes inherit **non-private** members from the base classes

### Virtual Methods

- `virtual` allows a derived class to override a base class's methods
- within the implementation of the derived class, use `override`
- append the `= 0` suffix to the `virtual` method definition to **require** a derived class to implement the method
- **pure virtual** methods: `virtual` and `= 0`
- You **CANNOT** instantiate a class containing _any_ pure virtual methods
- Virtual functions can incur runtime overhead
  - although cost is typically low (25% of a regular function call)
- Compiler generates **virtual function tables** (**vtables**) that contain function pointers
  - a _table of pointers_ pointing to functions
  - each class with virtual functions has its own `vtbl` identifying its virtual functions

### Pure-Virtual Classes and Virtual Destructors

- To achieve interface inheritance, derive from base classes that contain only pure-virtual methods
- **pure-virtual classes**
- Interfaces are **always** pure-virtual classes
- Remember to add virtual destructors to interfaces
  - sometimes it's possible to leak resources if destructors are not marked as virtual

### Implementing Interfaces

- To declare an interface, declare a pure virtual class
- To implement an interface, derive from it

### Using Interfaces

- Interfaces can only be dealt with in **references** or **pointers** by consumers
- Two ways to set the member:
  - **constructor injection**
    - use an interface reference
  - **property injection**
    - use a method to set a pointer member
    - allows you to change the object to which the member points
- A class hierarchy offers two kinds of benefits:
  - Interface inheritance
  - Implementation inheritance
- Concrete classes - especially classes with small representations - are much like built-in types
  - defined as local variables
  - accessed using their names
  - copied around
- Classes in class hierarchies are _different_!
  - allocated on heap using `new`
  - accessed through pointers or references
  -
