/*
 * Problems with this approach:
 *   - when adding a new logger type:
 *     - you need to write a new logger type
 *     - you need to add a new `enum` value to the `enum class`
 *     - must add a new case in the `switch`
 *     - add the new logging class as a member to `Bank`
 */
#include <cstdio>
#include <stdexcept>

struct FileLogger {
  void log_transfer(long from, long to, double amount) {
    printf("[file] %ld,%ld,%f\n", from, to, amount);
  }
};

struct ConsoleLogger {
  void log_transfer(long from, long to, double amount) {
    printf("[console] %ld -> %ld: %f\n", from, to, amount);
  }
};

enum class LoggerType {
  Console,
  File,
};

struct Bank {
  Bank() : type{LoggerType::Console} {}

  void set_logger(LoggerType new_type) { type = new_type; }

  void make_transfer(long from, long to, double amount) {
    // ...
    switch (type) {
      case LoggerType::Console: {
        consoleLogger.log_transfer(from, to, amount);
        break;
      }
      case LoggerType::File: {
        fileLogger.log_transfer(from, to, amount);
        break;
      }
      default: {
        throw std::logic_error("Unknown Logger type encountered!");
      }
    }
  }

 private:
  LoggerType type;
  ConsoleLogger consoleLogger;
  FileLogger fileLogger;
};

int main() {
  Bank bank;
  bank.make_transfer(1000, 2000, 49.95);
  bank.make_transfer(2000, 4000, 15.00);
  bank.set_logger(LoggerType::File);
  bank.make_transfer(3000, 2000, 123.00);
}
