#include <cstdio>

// Adding the virtual destructor causes the DerivedClass destructor to get
// invoked when the BaseClass pointer is deleted, which results in the
// `DerivedClass` destructor printing the message
struct BaseClass {
  virtual ~BaseClass() = default;
};

struct DerivedClass : BaseClass {
  DerivedClass() { printf("DerivedClass() invoked.\n"); }
  ~DerivedClass() { printf("~DerivedClass() invoked.\n"); }
};

int main() {
  printf("Constructing DerivedClass x.\n");
  BaseClass* x{new DerivedClass{}};
  printf("Deleting x as a BaseClass*.\n");
  delete x;
}
