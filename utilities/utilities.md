# Utilities

## Data Structures

### `tribool`

- `bool`-like type that supports **three** states (rather than two)
  - true
  - false
  - indeterminate
- `boost::logic::tribool` in `<boost/logic/tribool.hpp>`
- `tribool` implicitly converts to `bool`
  - if a `tribool` is `true`, it converts to `true`
  - otherwise it converts to `false`
- supports `operator!`
  - returns `true` if `false`
  - otherwise return `false`
- `indeterminate` supports `operator()`
  - check if argument is `indeterminate`

### `optional`

- a class template that contains a value that might or might not be present
- use case:
  - reutrn type of a function that might fail
  - the function can return an `optional` that contains the value if it succeeds
- `std::optional` in `<optional>`
- `boost::optional` in `<boost/optional.hpp>`
- `std::nullopt`: a `std::optional` type with uninitialized state
- use `operator->` and `.value()` to access the underlying value

### `pair`

- contains two objects of different types in a single object
- objects are **ordered**
  - access them via `.first` and `.second`
- supports comparison operators
- `std::pair` in `<utility>`
- `boost::pair` in `<boost/pair.hpp>`
- use **structured binding** syntax to extract references to members

### `tuple`

- takes an arbitrary number of **heterogeneous** elements
- a generalization of `pair`, but does **NOT** expose its members as `first`, `second`
- use non-member function template `get` to extract elements
- `std::tuple` and `std::get`
- `boost::tuple` and `boost::get`
- allows structured binding

### `any`

- `any` is a **class** that stores single values of any type
- it is **NOT** a class template
- to convert `any` into a concrete type, use **any cast**
- any cast is type safe
- use case:
  - some kinds of generic programming _without_ templates
- `std::any`
- `boost::any`
- use `emplace` to store a value into `any`
- use `any_cast` to extract the value

### `variant`

- a class template that stores single values whose types are restricted to the user-defined list provided as template parameters
- a type-safe `union`
- `std::variant` in `<variant>`
- `boost::variant` in `<boost/variant>`
- a `variant` can _only_ be default constructed if:
  - the first template parameter is default constructible
  - it is `monostate`
    - a type intended to communicate that a variant can have an empty state
- use `emplace` to store a value into a `variant`
- to extract value, use `get` or `get_if`
  - they are both non-member functions
- if `get` fails, it throws `bad_variant_access` exception
- if `get_if` fails, it returns `nullptr`
- `index()`
  - returns the index of the current object's type within the template parameter list
- use non-member function `std::visit` to apply a callable object to a variant
  - you do not need to get the value first using `std::get`
- `variant` can be more performant than `any`

## Date and Time

### Boost DateTime

- `<boost/date_time/gregorian/gregorian.hpp>`
  - `boost::gregorian::date`
- default constructor returns `boost::gregorian::not_a_date_time`
- `boost::gregorian::date d{1999, 12, 31};`
- `auto d = boost::gregorian::from_string("1999/12/31");`
- Invalid values will throw exceptions
  - `bad_year`
  - `bad_day_of_month`
  - `bad_month`
- `auto d_local = boost::gregorian::day_clock::local_day();`
- `auto d_universal = boost::gregorian::day_clock::universal_day();`
- date is **immutable**
  - once you construct it, it cannot be changed
  - but it supports copy construction and copy assignment
- `boost::gregorian::date_duration`
  - when you subtract two dates
  - extract the value using `.days()`
- construct `date_duration` using a `long` argument corresponding to number of days
- **date period**
  - interval between two dates
  - `boost::gregorian::date_period`

### Chrono

- `<chrono>`
- time programming
- clock returns a `time_point`
- subtracting two `time_point`s gives a `duration`
- Clocks
  - `std::chrono::system_clock`
  - `std::chrono::steady_clock`
  - `std::chrono::high_resolution_clock`
    - with the shortest tick
- `time_point`
- `time_since_epoch`
- `std::chrono::duration`
  - time between two `time_piont` objects
  - `.count()` - number of clock ticks in the duration
- `ms` is a duration literal in the `std::literals::chrono_literals` namespace
- `std::chrono::duration_cast` - cast a duration from one unit to another
- `std::this_thread::sleep_for` to sleep/wait

## Numerics

### Numeric Functions

- `<cstdlib>`
- `<cmath>`
- `<algorithm>`

### Complex Numbers

- `a + bi`
  - `a`: **real component**
  - `b`: **imaginary component**
- `std::complex` class template in `<complex>`
  - accepts a template parameter for the underlying real and imaginary component
    - must be one of the fundamental floating-point types
- supports copy construction and copy assignment

### Random Numbers

- **NEVER attempt to build your own random number generator**
- `<random>`
- `<boost/math/...>`
- if you need repeatable psuedo-random numbers: `std::mtt19937_64` - Mersenne Twister engine
- if you need cryptographically secure random numbers: `std::random_device`
- `std::mt19937_64 mt_engine{91586};`
  - call `operator()` to obtain a random number
- `std::random_device rd_engine{};`
  - `rd_engine` **is** invokable
  - but you should treat the object as opaque
  - **unpredictable** by design

#### Random Number Distributions

- **discrete** and **continuous**
- `bind()` - makes a function object that will invoke its first argument (distribution) given its second argument (engine) as its argument

### Numeric Limits

- `std::numeric_limits` in `<limits>`
- `std::numeric_limits<T>::min()`

### Boost Numeric Conversion

- `boost::converter` class template in `<boost/numeric/conversion/converter.hpp>`
- provide both target type and source type
- Advantage over `static_cast`:
  - runtime bounds checking
  - if overflowed,
    - `boost::numeric::positive_overflow` or `boost::numeric::negative_overflow` exception
- shortcut: `boost::numeric_cast`
  - function template
  - can replace `narrow_cast`

### Compile-Time Rational Arithmetic

- `std::ratio` in `<ratio>`
  - provide two template parameters: **numerator** and **denominator**

## `iterator_traits`

- `forward_list` does _NOT_ offer _random-access_ iterators
  - does offer _forward iterators_
- `iterator_traits` can be used to check which kind of iterator is supported

## Type Predicates

- `is_arithmetic`
- in `<type_traits>`

## Regular Expressions

- Use _raw string_ like `R"()"` - allows backslashes and quotes to be used directly

## Vector Arithmetic

- `valarray` in `<valarray>`
-
