#include <boost/logic/tribool.hpp>
#include <catch2/catch_test_macros.hpp>

using boost::logic::indeterminate;
boost::logic::tribool t = true, f = false, i = indeterminate;

// TEST_CASE("Boost tribool converts to bool") {
//   REQUIRE(t == true);
//   REQUIRE_FALSE(f);
//   REQUIRE(!f);
//   REQUIRE_FALSE(!t);
//   REQUIRE(indeterminate(i));
//   REQUIRE_FALSE(indeterminate(t));
// }

// TEST_CASE("Boost tribool supports boolean operations") {
//   auto t_or_f = t || f;
//   REQUIRE(t_or_f);
// }

TEST_CASE("Boost tribool works with if statements") {
  if (i)
    FAIL("Indeterminate is true");
  else if (!i)
    FAIL("Indeterminate is false");
  else {
  }  // OK
}
