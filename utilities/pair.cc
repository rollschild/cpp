#include <catch2/catch_test_macros.hpp>
#include <utility>

struct Socialite {
  const char* birthname;
};
struct Valet {
  const char* surname;
};
Socialite guangchu{"Guangchu"};
Valet shi{"Shi"};

TEST_CASE("std::pair permits access to members") {
  std::pair<Socialite, Valet> duo{guangchu, shi};
  REQUIRE(duo.first.birthname == guangchu.birthname);
  REQUIRE(duo.second.surname == shi.surname);
}

TEST_CASE("std::pair works with structured binding") {
  std::pair<Socialite, Valet> duo{guangchu, shi};
  auto& [idle, butler] = duo;
  REQUIRE(idle.birthname == guangchu.birthname);
  REQUIRE(butler.surname == shi.surname);
}
