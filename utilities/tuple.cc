#include <catch2/catch_test_macros.hpp>
#include <tuple>

struct Person {
  const char* name;
};
struct Socialite {
  const char* birthname;
};
struct Valet {
  const char* surname;
};
Socialite guangchu{"Guangchu"};
Valet shi{"Shi"};
Person jovi{"Jovi"};

TEST_CASE("std::tuple permits access to members via std::get") {
  using Trio = std::tuple<Socialite, Valet, Person>;
  Trio trio{guangchu, shi, jovi};
  auto& guangchu_ref = std::get<0>(trio);
  REQUIRE(guangchu_ref.birthname == guangchu.birthname);

  auto& person_ref = std::get<Person>(trio);
  REQUIRE(person_ref.name == jovi.name);
}
