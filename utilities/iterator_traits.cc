#include <algorithm>
#include <forward_list>
#include <iostream>
#include <iterator>
#include <ostream>
#include <vector>

template <typename C>
using ValueType = typename C::value_type;

// the iterator type of C
template <typename C>
using IteratorType = typename C::iterator;

// Iter's category
// construct a tag value indicating the kind of iterator provided
template <typename Iter>
using IteratorCategory = typename std::iterator_traits<Iter>::iterator_category;

template <typename T>
void sort(T& t);

void test(std::vector<double>& v, std::forward_list<double>& lst) {
  sort(v);
  sort(lst);
}

template <typename Random>
void sort_helper(Random beg, Random end, std::random_access_iterator_tag) {
  std::sort(beg, end);
}

template <typename Forward>
void sort_helper(Forward beg, Forward end, std::forward_iterator_tag) {
  // ValueType<Forward> is the type of Forward's elements - its value type
  // Every stdlib iterator has a member `value_type`
  std::vector<ValueType<Forward>> v{beg, end};
  std::sort(v.begin(), v.end());
  std::copy(v.begin(), v.end(), beg);
}

template <typename T>
void sort(T& t) {
  using Iter = IteratorType<T>;
  // select between the two sorting algorithms at _compile time_
  // **tag dispatch**
  sort_helper(t.begin(), t.end(), IteratorCategory<Iter>{});
}

int main() {
  std::vector<double> v{2.0, 1.1, -6, 18.0, 9.2};
  std::forward_list<double> f{2.0, 1.1, -6, 18.0, 9.2};
  test(v, f);

  for (auto ele : v) {
    std::cout << ele << " ";
  }
  std::cout << "\n";
  for (auto ele : f) {
    std::cout << ele << " ";
  }
  std::cout << std::endl;
}
