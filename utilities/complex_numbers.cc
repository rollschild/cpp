#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <complex>

TEST_CASE("std::complex has a real and imaginary component") {
  std::complex<double> a{0.5, 14.13};
  REQUIRE(std::real(a) == Catch::Approx(0.5));
  REQUIRE(std::imag(a) == Catch::Approx(14.13));
}
