#include <boost/math/constants/constants.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <random>

TEST_CASE("std::uniform_int_distribution produces uniform ints") {
  std::mt19937_64 mt_engine{102787};
  std::uniform_int_distribution<int> int_d{0, 10};
  const size_t n{1'000'000};
  int sum{};
  for (size_t i{}; i < n; i++) {
    sum += int_d(mt_engine);
  }
  const auto sample_mean = sum / double{n};
  REQUIRE(sample_mean == Catch::Approx(5).epsilon(.1));  // with high confidence
}
