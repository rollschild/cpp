#include <math.h>

#include <boost/math/constants/constants.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

TEST_CASE("boost::math offers constants") {
  using namespace boost::math::double_constants;
  auto sphere_volume = four_thirds_pi * std::pow(10, 3);
  REQUIRE(sphere_volume == Catch::Approx(4188.79));
}
