#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <limits>

TEST_CASE("std::numeric_limits::min provides the smallest finite value.") {
  auto my_cup = std::numeric_limits<int>::min();
  auto underfloweth = my_cup - 1;
  REQUIRE(my_cup < underfloweth);  // UB; PAY ATTENTION!
}
