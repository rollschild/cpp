#include <boost/date_time/gregorian/gregorian.hpp>
#include <catch2/catch_test_macros.hpp>
#include <chrono>
#include <thread>

TEST_CASE("Invalid boost::gregorian::date throws exceptions") {
  using boost::gregorian::bad_day_of_month;
  using boost::gregorian::date;

  REQUIRE_THROWS_AS(date(1999, 12, 32), bad_day_of_month);
}

TEST_CASE("boost::gregorian::date supports basic calendar functions") {
  boost::gregorian::date d{1999, 12, 31};
  REQUIRE(d.year() == 1999);
  REQUIRE(d.month() == 12);
  REQUIRE(d.day() == 31);
  REQUIRE(d.day_of_year() == 365);
  REQUIRE(d.day_of_week() == boost::date_time::Friday);
}

TEST_CASE("boost::gregorian::date supports calendar arithmetic") {
  boost::gregorian::date d1{1999, 12, 31};
  boost::gregorian::date d2{2022, 12, 30};
  auto duration = d2 - d1;
  REQUIRE(duration.days() == 23 * 365 - 1 + 6);
}

TEST_CASE("date and date_duration support addition") {
  boost::gregorian::date d1{1999, 12, 31};
  boost::gregorian::date_duration dur{23 * 365 - 1 + 6};
  auto d2 = d1 + dur;
  REQUIRE(d2 == boost::gregorian::from_string("2022/12/30"));
}

TEST_CASE("boost::gregorian::date supports periods") {
  boost::gregorian::date d1{1999, 12, 31};
  boost::gregorian::date d2{2022, 12, 30};

  boost::gregorian::date_period p{d1, d2};
  REQUIRE(p.contains(boost::gregorian::date{2000, 1, 1}));
}

TEST_CASE("std::chrono supports several clocks") {
  auto sys_now = std::chrono::system_clock::now();
  auto hires_now = std::chrono::high_resolution_clock::now();
  auto steady_now = std::chrono::steady_clock::now();

  REQUIRE(sys_now.time_since_epoch().count() > 0);
  REQUIRE(hires_now.time_since_epoch().count() > 0);
  REQUIRE(steady_now.time_since_epoch().count() > 0);
}

TEST_CASE("std::chrono supports different units of measurement") {
  using namespace std::literals::chrono_literals;
  auto one_s = std::chrono::seconds(1);
  auto thousand_ms = 1000ms;

  REQUIRE(one_s == thousand_ms);
}

TEST_CASE("std::chrono supports duration_cast") {
  using namespace std::chrono;
  auto billion_ns_as_s = duration_cast<seconds>(1'000'000'000ns);
  REQUIRE(billion_ns_as_s.count() == 1);
}

TEST_CASE("std::chrono used to sleep") {
  using namespace std::literals::chrono_literals;
  auto start = std::chrono::system_clock::now();
  std::this_thread::sleep_for(100ms);
  auto end = std::chrono::system_clock::now();
  REQUIRE(end - start >= 100ms);
}
