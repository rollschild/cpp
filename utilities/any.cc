#include <any>
#include <catch2/catch_test_macros.hpp>

struct EscapeCapsule {
  EscapeCapsule(int x) : weight_kg{x} {}
  int weight_kg;
};

TEST_CASE("std::any allows us to any_cast into a type") {
  std::any vessel;
  vessel.emplace<EscapeCapsule>(42);
  auto capsule = std::any_cast<EscapeCapsule>(vessel);
  REQUIRE(capsule.weight_kg == 42);
  REQUIRE_THROWS_AS(std::any_cast<float>(vessel), std::bad_any_cast);
}
