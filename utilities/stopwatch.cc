#include <chrono>
#include <cstdint>
#include <cstdio>

struct Stopwatch {
  Stopwatch(std::chrono::nanoseconds& result)
      : result{result}, start{std::chrono::high_resolution_clock::now()} {}

  ~Stopwatch() { result = std::chrono::high_resolution_clock::now() - start; }

 private:
  std::chrono::nanoseconds& result;
  const std::chrono::time_point<std::chrono::high_resolution_clock> start;
};

int main() {
  const size_t n = 1'000'000;
  std::chrono::nanoseconds elapsed;  // initial to be 0
  printf("initial value of elapsed: %ld.\n", elapsed.count());
  {
    Stopwatch stopwatch{elapsed};

    // so that the compiler does NOT try to optimize the loop away
    volatile double result{1.23e45};

    for (double i = 1; i < n; i++) {
      result /= i;
    }
  }

  auto time_per_division = elapsed.count() / double{n};
  printf("Took %gns per division.\n", time_per_division);
}
