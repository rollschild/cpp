#include <boost/numeric/conversion/cast.hpp>
#include <boost/numeric/conversion/converter.hpp>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <limits>

using double_to_int = boost::numeric::converter<int, double>;

TEST_CASE("boost::converter offers the static convert method") {
  REQUIRE(double_to_int::convert(3.14159) == 3);
}

TEST_CASE("boost::numeric::converter implements operator()") {
  double_to_int dtoi;
  REQUIRE(dtoi(3.14159) == 3);
  REQUIRE(double_to_int{}(3.14159) == 3);  // temporary function object
}

TEST_CASE("boost::numeric::converter checks for overflow") {
  auto yuge = std::numeric_limits<double>::max();
  double_to_int dtoi;
  REQUIRE_THROWS_AS(dtoi(yuge), boost::numeric::positive_overflow);
}

TEST_CASE("boost::numeric_cast checks for overflow") {
  auto yuge = std::numeric_limits<double>::max();

  REQUIRE_THROWS_AS(boost::numeric_cast<int>(yuge),
                    boost::numeric::positive_overflow);
}
