#include <catch2/catch_test_macros.hpp>
#include <variant>

struct EscapeCapsule {
  EscapeCapsule(int x) : weight_kg{x} {}
  int weight_kg;
};

// default constructible
struct Beast {
  // this is the default constructor
  Beast() : is_dangerous{true}, weight_kg{2000} {}
  bool is_dangerous;
  int weight_kg;
};

TEST_CASE("std::variant") {
  std::variant<Beast, EscapeCapsule> thing;
  REQUIRE(thing.index() == 0);

  thing.emplace<EscapeCapsule>(600);
  REQUIRE(thing.index() == 1);

  REQUIRE(std::get<EscapeCapsule>(thing).weight_kg == 600);
  REQUIRE(std::get<1>(thing).weight_kg == 600);
  REQUIRE_THROWS_AS(std::get<0>(thing), std::bad_variant_access);
}

TEST_CASE("std::variant operations") {
  std::variant<Beast, EscapeCapsule> thing;
  thing.emplace<EscapeCapsule>(600);
  auto lbs = std::visit([](auto& x) { return 2.2 * x.weight_kg; }, thing);
  REQUIRE(lbs == 1320);
}
