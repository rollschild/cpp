#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <map>

auto color_of_magic = "Color of Magic";
auto light_fantastic = "Light Fantastic";
auto equal_rites = "Equal Rites";
auto mort = "Mort";

TEST_CASE("std::map supports") {
  SECTION("default construction") {
    std::map<const char*, int> emp;
    REQUIRE(emp.empty());
  }

  SECTION("braced initialization") {
    std::map<const char*, int> pub_year{
        {color_of_magic, 1983},
        {light_fantastic, 1986},
        {equal_rites, 1987},
        {mort, 1987},
    };
    REQUIRE(pub_year.size() == 4);
  }
}

TEST_CASE("std::map is an associative array with") {
  std::map<const char*, int> pub_year{
      {color_of_magic, 1983},
      {light_fantastic, 1986},
  };

  SECTION("operator[]") {
    REQUIRE(pub_year[color_of_magic] == 1983);

    pub_year[equal_rites] = 1987;
    REQUIRE(pub_year[equal_rites] == 1987);
    REQUIRE(pub_year[mort] == 0);  // default constructed
  }

  SECTION("at") {
    REQUIRE(pub_year.at(color_of_magic) == 1983);
    REQUIRE_THROWS_AS(pub_year.at(mort), std::out_of_range);
  }
}

TEST_CASE("std::map supports insert") {
  std::map<const char*, int> pub_year;
  pub_year.insert({color_of_magic, 1983});
  REQUIRE(pub_year.size() == 1);

  std::pair<const char*, int> tlfp{light_fantastic, 1986};
  pub_year.insert(tlfp);
  REQUIRE(pub_year.size() == 2);

  auto [itr, is_new] = pub_year.insert({light_fantastic, 9999});
  REQUIRE(itr->first == light_fantastic);
  REQUIRE(itr->second == 1986);
  REQUIRE_FALSE(is_new);
  REQUIRE(pub_year.size() == 2);
}

TEST_CASE("std::map supports insert_or_assign") {
  std::map<const char*, int> pub_year{{light_fantastic, 9999}};
  auto [itr, is_new] = pub_year.insert_or_assign(light_fantastic, 1986);
  REQUIRE(itr->second == 1986);
  REQUIRE_FALSE(is_new);
}

TEST_CASE("remove std::map elements using") {
  std::map<const char*, int> pub_year{
      {color_of_magic, 1983},
      {mort, 1987},
  };

  SECTION("erase") {
    pub_year.erase(mort);
    REQUIRE(pub_year.find(mort) == pub_year.end());
  }

  SECTION("empty") {
    pub_year.clear();
    REQUIRE(pub_year.empty());
  }
}

TEST_CASE("std::multimap supports non-unique keys") {
  std::array<char, 64> far_out{"This is some random long string..."};
  REQUIRE(far_out.size() == 64);
  std::multimap<char, size_t> indices;
  for (size_t index{}; index < far_out.size(); index++) {
    indices.emplace(far_out[index], index);
  }

  REQUIRE(indices.count('a') == 1);

  auto [itr, end] = indices.equal_range('i');
  REQUIRE(itr->second == 2);
  itr++;
  REQUIRE(itr->second == 5);
  itr++;
  REQUIRE(itr->second == 28);
  itr++;
  REQUIRE(itr == end);
}
