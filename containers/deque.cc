#include <catch2/catch_test_macros.hpp>
#include <deque>

TEST_CASE("std::deque supports front insertion") {
  std::deque<char> deckard;
  deckard.push_front('a');
  deckard.push_back('i');
  deckard.push_front('c');
  deckard.push_back('n');

  REQUIRE(deckard.at(0) == 'c');
  REQUIRE(deckard.at(1) == 'a');
  REQUIRE(deckard.at(2) == 'i');
  REQUIRE(deckard.at(3) == 'n');
}
