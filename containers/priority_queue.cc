#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <queue>

TEST_CASE("std::priority_queue supports push/pop") {
  std::priority_queue<double> pq;
  pq.push(1.0);
  pq.push(2.0);
  pq.push(1.5);
  REQUIRE(pq.top() == Catch::Approx(2.0));

  pq.pop();
  pq.push(1.0);
  REQUIRE(pq.top() == Catch::Approx(1.5));

  pq.pop();
  REQUIRE(pq.top() == Catch::Approx(1.0));
  pq.pop();
  REQUIRE(pq.top() == Catch::Approx(1.0));

  pq.pop();
  REQUIRE(pq.empty());
}
