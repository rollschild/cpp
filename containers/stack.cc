#include <catch2/catch_test_macros.hpp>
#include <stack>

TEST_CASE("std::stack supports push/pop/top operations") {
  std::vector<int> vec{1, 3};
  std::stack<int, decltype(vec)> easy(vec);

  REQUIRE(easy.top() == 3);

  easy.pop();
  easy.push(2);
  REQUIRE(easy.top() == 2);

  easy.pop();
  REQUIRE(easy.top() == 1);

  easy.pop();
  REQUIRE(easy.empty());
}
