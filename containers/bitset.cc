#include <bitset>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>

TEST_CASE("std::bitset supports integer initialization") {
  std::bitset<4> bs(0b1010);  // remember lowest bits on the right
  REQUIRE_FALSE(bs[0]);
  REQUIRE(bs[1]);
  REQUIRE_FALSE(bs[2]);
  REQUIRE(bs[3]);
}

TEST_CASE("std::bitset supports string initialization") {
  std::bitset<4> bs1(0b1010);
  std::bitset<4> bs2("1010");
  REQUIRE(bs1 == bs2);
}
