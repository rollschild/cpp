#include <catch2/catch_test_macros.hpp>
#include <queue>

TEST_CASE("std::queue supports push/pop/front/back") {
  std::deque<int> deq{1, 2};
  std::queue<int> easy(deq);

  REQUIRE(easy.front() == 1);
  REQUIRE(easy.back() == 2);

  easy.pop();
  easy.push(3);
  REQUIRE(easy.front() == 2);
  REQUIRE(easy.back() == 3);

  easy.pop();
  REQUIRE(easy.front() == 3);
  easy.pop();
  REQUIRE(easy.empty());
}
