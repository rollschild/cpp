/*
 * Copyright 2023 Guangchu Shi
 */
#include <iostream>
#include <vector>

using std::string;
using std::vector;

template <typename T>
using Iterator = typename T::iterator;

template <typename C, typename V>
vector<Iterator<C>> find_all(C& c, V v) {
  vector<Iterator<C>> res{};
  for (auto ptr = c.begin(); ptr != c.end(); ++ptr) {
    if (*ptr == v) {
      res.push_back(ptr);
    }
  }

  return res;
}

int main() {
  string m{"My name is Guangchu."};
  for (auto p : find_all(m, 'a')) {
    if (*p != 'a') {
      std::cerr << "There is a BUG!\n";
    }
  }

  vector<string> vs{"red", "blue", "green", "purple", "green", "pink", "green"};
  for (auto p : find_all(vs, "green")) {
    if (*p != "green") {
      std::cerr << "VECTOR bug!\n";
    }
  }
  for (auto p : find_all(vs, "green")) {
    *p = "obsidian";
  }
  for (auto p : vs) std::cout << p << " ";
  std::cout << std::endl;
}
