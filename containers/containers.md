# Containers

- the **standard template library** (STL)
- **container**
  - a special data structure that stores objects in an organized way that follows specific access rules
- Three kinds of containers
  - **sequence** - store elements consecutively, like an array
  - **associative** - store sorted elements
  - **unordered associative** - store hashed objects
- they are all RAII wrappers which manage the storage durations and lifetimes of the elements they own
- The **handle-to-data** model - to manage data that can vary _in size_ during the lifetime of an object
  - `std::vector`
  - avoid allocations in general code
  - keep them buried inside the implementation of well-behaved abstractions
-

- `std::string`
  - _mutable_
  - `substr()`
  - `replace()`

## Sequence Containers

- allows sequential member access

### Arrays

- `std::array`
- fixed-size
- contiguous
- **You should use `array` instead of built-in arrays in virtually all situations**
- to get access to elements:
  - `operator[]`
  - `at`
  - `get`
- `operator[]` vs. `at`
  - both take `size_t`
  - `at` throws `std::out_of_range` exception if out of bounds
  - `operator[]` will cause UB
- `get` takes a template parameter - index must be known at compile time
  - you will get compile-time bounds checking
- `front` and `back`
  - return **references** to the first and last elements of the array
  - gives you UB if calling those methods on a zero-length array

#### Storage Model

- an `array` does not make allocations
- like a built-in array, it contains all of its elements
- copies and moves can be expensive
- each `array` is just a built-in array underneath
- Four methods to extract a pointer to to the first element of the `array`
  - the `data` method - **YOU SHOULD USE THIS ONE**
  - the other three: use `operator&` on the first element, which can be obtained by:
    - `operator[]`
    - `at`
    - `front`
- use `size()` to get size of the `array`

#### Iterators

- the interface between containers and algorithms
- Three operations:
  - `operator*` - get current element
  - `operator++` - go to next element
  - `operator=` - assign an iterator to another
- extract iterators from _all_ STL containers using `begin()` and `end()`
- `end()` points to one element _past_ the last element
  - **half-open range**
  - use `begin() == end()` to test whether you've looped to the end of the array

### Vectors

- `std::vector` in `<vector>`
- `boost::container::vector` in `<boost/container/vector.hpp>`
- holds **dynamically** sized, contiguous series of elements
- a very modest overhead..?
- `std::vector<T, Allocator>`
  - second template parameter is optional and default to `std::allocator<T>`
- more flexibility than `array`s
- **fill constructors**
  - pass a `size_t` - number of elements you want to fill
  - (optional) pass a `const` reference to an object to copy
  - use paranthesis, not braces
- can also construct a `vector` from a half-open range
- full copy/move construction/assignment support
  - copy is potentially very expensive, because these are element-wise or deep copies
  - move operations are usually very fast
- use `assign` to take an initialization list and replace all existing elements
- use `insert` to insert a single new element
  - two arguments: and iterator and an element to insert
  - it will insert **a copy of the element**
  - it will insert **before** the existing element pointed to by the iterator
  - any time you use `insert`, existing iterators become invalid
- `push_back()`
  - does not require an iterator
- `emplace` and `emplace_back`
  - generally the emplacement methods are more efficient than `insert`, since they can construct elements in place
  - general rule: use emplacement methods

#### Storage Model

- `vector` has dynamic size - it is able to resize
- keeps elements on teh **free store**
- e.g. `push_back()` does `new`s to acquire space for elements and `delete`s to free space
- memory for `vector` is _always_ contiguous
- once there is not enough space at the end of the existing vector, it will allocate a _whole new_ region of memory and move _all_ the elements of the `vector` to the new region
- **size** and **capacity**
  - `.capacity()`
- `.max_size()` - the absolute maximum capacity a `vector` could resize to
- `.reserve()` - when you know beforehand what the capacity will be
  - takes a `size_t` argument
- `.shrink_to_fit()`
- `.clear()` - delete all elements and set the size to `0`

### Niche Sequential Containers

#### Deque

- **double-ended queue**
- fast insert and remove from the front and back
- `std::deque` in `<deque>`
- `boost::container::deque` in `<boost/container/deque.hpp>`
- `deque`'s memory is usually scattered
- large resizing operations more efficient
- fast element insertion/deletion at the front
- does **NOT** have a `.data()` method
- `push_front` and `emplace_front`
- **DO NOT** try to extract complex content from deque because it has no guarantee about internal layout
- `.pop_front()`
- `.pop_back()` - constant time complexity

#### List

- fast insert/remove operations **everywhere** but with **no** random element access
- implemented as a doubley linked list
  - with **node**s
- each node has:
  - an element
  - **flink** - forward link
  - **blink** - backward link
- `remove_if`
  - takes a function object as a parameter
- also `std::forward_list`
  - singly linked list
  - slightly more efficient for storing fewer (or no) elements
- List has a _move_ constructor which makes returning the list _by value_ efficient

#### Stacks

- Three **container adapters** provided by STL:
  - **stack**
  - **queue**
  - **priority queue**
- LIFO
- `std::stack` class template in `<stack>`
- Takes two template parameters
  - underlying type of the wrapped container, such as `int`
  - type of the wrapped container, like `deque` or `vector`
- uses `deque` by default
- `.top()` - to obtain a reference to the element on top of the stack

#### Queue

- **FIFO**
- `std::queue` in `<queue>`
- can **only** use `deque` or `list` as the underlying container
  - because pushing/popping from the front of a `vector` is **inefficient**
- uses `deque` by default

#### Priority Queue

- a.k.a. **heap**
- supports `push` and `pop`
- sorted according to user-specified **comparator object**
  - a function object invokable with two parameters, returning `true` if the first arg is less than the second
- `std::priority_queue` in `<queue>`
- takes three template parameters
  - underlying type of the wrapped container - mandatory
  - type of the wrapped container - default to `vector`
  - type of the comparator object - default to `std::less` in `<functional>`
- default to max heap I'd imagine?

#### Bitsets

- stores a fixed-size bit sequence
- each bit can be manipulated
- `std::bitset` in `<bitset>`
- takes one template parameter - the desired size
- optimized for space efficiency
- `std::vector<bool>` is a specialized template class
- `boost::dynamic_bitset` - dynamic sizing at runtime
- lowest indexed elements at the **right**
- a default constructed `bitset` contains all _zero_ (false) bits

## Associative Containers

- very fast element search

### Set

- `std::set` in `<set>`
- contains **sorted**, **unique** elements called **keys**
- insert, remove, and search efficiently
- supports sorted iteration
  - you have controle over how keys sort using **comparator objects**
- `boost::container::set` in `<boost/container/set.hpp>`

#### Constructing

- Takes three template parameters:
  - key type `T`
  - comparator type - default to `std::less`
  - allocator type - default to `std::allocator<T>`

#### Move and Copy Semantics

- supports copy/move assignments
  - `set` copies are potentially very slow because each element needs to get copied
  - move operations are usually fast because elements reside in dynamic memory
- memberwise copy may _not_ be the right semantics for copy for sophisticated types such as `vector`
- when a class is a _resource handle_ - when the class is reponsible for an object accessed through a pointer,
  - the default memberwise copy is _typically_ a _disaster_!
- Copy constructor and copy assignment usually come together!
- they usually take the argument `const X&`
- Move:

```c++
X(X&& x); // move constructor
X& operator=(X&& x); // move assignment
```

```c++
// move
Vector::Vector(Vecto&& a) : elem{a.elem}, size{a.size} {
  a.elem = nullptr; // a now has no elements!
  a.size = 0;
}
```

- `&&` - rvalue reference
- we should allow assignment to a moved-from object
- `std::move()` returns an rvalue reference to its argument
- To achieve resource safety:
  - use `vector` to hold memory
  - use `thread` to hold system threads
  - use `fstream` to hold file handles
- `=delete` can be used to suppress _any_ operation
- a move operation is _NOT_ implicitly generated for a class where the destructor is explicitly declared
- _DEPRECATED_ in C++11:
  - generation of the copy constructor and the copy assignment for a class with a destructor
  - add `X(const X&) = default;` to suppress the warning/error
  - [stackoverflow answer](https://stackoverflow.com/a/51864979/9666932)
- Rule of Three:
  > If a class requires a user-defined destructor, a user-defined copy constructor, or a user-defined copy assignment operator, it almost certainly requires all three.
- Rule of Zero:
  > Classes that have custom destructors, copy/move constructors or copy/move assignment operators should deal exclusively with ownership (which follows from the Single Responsibility Principle). Other classes should not have custom destructors, copy/move constructors or copy/move assignment operators.

#### Element Access

- `find`
  - takes a `const` reference to a key and returns an iterator
- `lower_bound()`
- `upper_bound()`
- `count()` - returns number of elements matching the key
  - for `set`, it's either `0` or `1`
- `equal_range()` - returns a half-open range containing all elements matching the given key
  - returns a `std::pair` of iterators, where
    - `first` points to the matching element
    - `second` points to the element after `first`
- supports iterator `begin()` and `end()`

#### Add Elements

- Three options:
  - `insert`: copy and _existing_ element into th e `set`
  - `emplace`: in-place construct a new element into the `set`
  - `emplace_hint`: takes an iterator as first argument, as the search's starting point
    - potential speed up
- they all return `std::pair<Iterator, bool>`
  - second element indicates whether an insertion has occurred or not
  - first element points to either:
    - the newly inserted element, or
    - the existing element that prevented insertion

#### Remove Elements

- `erase`, which accepts
  - a key
  - an iterator or a half-open range
- `clear`

#### Storage Model

- typically implemented as **red-black trees**
- as long as tree is roughly balanced, a search is quicker than linear

#### Multisets

- `std::multiset` in `<set>`
- contains _sorted_, _non-unique_ keys
- `count()` can return values other than `0` or `1`
- `equal_range` can return half-open ranges containing more than one element
- `boost::container::multiset` in `<boost/container/set.hpp>`

### Unordered Sets

- `std::unordered_set` in `<unordered_set>`
- contains _unsorted_, _unique_ keys
- its internal storage model is completely different than `set` or `multiset`
  - it uses a hash table

#### Storage Model

- **hash function** (or **hasher**)
  - accepts a key and returns a unique `size_t` value called the **hash code**
- `unordered_set` organizes elements into **buckets**, which are associated by hash codes
- To find an element, `unordered_set` computes the hash code, then searches through the corresponding bucket in the hash table
- the more _hash collisions_, the larger the buckets will be
- Characteristics of the hash function
  - accepts a `Key` and returns a `size_t`
  - does **not** throw exceptions
  - equal keys yield equal hash codes
  - unequal keys yield unequal hash codes _with high probability_
- `std::hash<T>` in `<functional>`
- You need a **function object** that determines equality between a key and a bucket element
  - `std::equal_to<T>` in `<functional>`
    - which invokes `operator==` on its arguments

#### Constructing

- `std::unordered_set<T, Hash, KeyEqual, Allocator>`
  - `Hash` defaults to `std::hash<T>`
  - `KeyEqual` defaults to `std::equal_to<T>`
  - `Allocator` defaults to `std::allocator<T>`
- does **NOT** supports `lower_bound` or `upper_bound`

#### Bucket Management

- higher performance but complicated interior structure
- `unordered_set` takes a `size_t` of `bucket_count` as its first argument
  - implementation specified
- `bucket_count()`
- `max_bucket_count()`
- **load factor**
  - average number of elements per bucket
  - equal to `size() / bucket_count()`
- `max_load_factor()`
  - if exceeded, `bucket_count` would increase and cause _rehashing_
- `rehash()` - manually trigger rehashing
  - takes a `size_t` argument for the desired bucket count
- `reserve()`
  - takes a `size_t` argument for the desired _element_ count

#### Unordered Multisets

- `std::unordered_multiset` in `<unordered_set>`
- unordered, _non-unique_ keys
- `boost::unordered_multiset` in `<boost/unordered_set.hpp>`

### Maps

- `std::map` in `<map>`
- contains key-value pairs
- keys are _sorted_ and _unique_
- `set` is a special form of `map`
- supports all `set` operations
- `map` works as an **associative array**
  - takes a key rather than an integer-valued index
  - you could use a string or a `float` as a key

#### Constructing

- `map<Key, Value, Comparator, Allocator>`
  - `Key`: key type
  - `Value`: value type
  - comparator type defaults to `std::less`
  - allocator type defaults to `std::allocator<T>`
- use nested initializer lists in braced initialization

#### Storage Model

- red-black tree

#### Element Access

- `operator[]` and `at()`
  - takes a `Key` argument
  - returns a _reference_ to the corresponding value
- `at()` will throw a `std::out_of_range` exception if the given `key` does not exist
- though `operator[]` will _not_ cause UB if key does not exist
  - it will silently default construct a `Value` and insert the corresponding key-value pair into the `map`

#### Adding Elements

- `operator[]`
- `at()`
- `insert()` and `emplace()`
  - treat each key-value pair as `std::pair<Key, Value>`
  - returns a pair of an iterator and `bool`
- `insert()` vs. `insert_or_assign()`
  - `insert()` does **NOT** overwrite existing keys
  - `insert_or_assign()` **does** overwrite existing values
  - `insert_or_assign()` accepts separate key and value arguments

#### Removing Elements

- `erase()` and `clear()`

#### Multimaps

- `std::multimap` in `<map>`
- _non-unique_ keys
- does **NOT** support `operator[]` or `at()`
- access elements primarily through the `equal_range()` method

- `std::unordered_map`
- `std::unordered_multimap`
  - they both use red-black tree

## Graphs and Property Trees

- **graph**: a set of objects i which some have a pairwise relation
  - **vertices**
  - **edges**
- **property tree**: a tree structure storing nested key-value pairs

## Boost Graph Library

- `boost::adjacency_list` in `<boost/graph/adjacency_list.hpp>`
- `boost::adjacency_matrix` in `<boost/graph/adjacency_matrix.hpp>`
- `boost::edge_list` in `<boost/graph/edge_list.hpp>`

## Initializer Lists

- `std::initializer_list` in `<initializer_list>`
- when a `{}`-list is used to initialize, the compiler will create an object of type `initializer_list` to give to the program
- You can accept initializer lists in your user-defined types by incorporating it
- You should design functions to accept an `initializer_list` **by value**

## Abstract Types

- interface decoupled from the representation
- Objects must be allocated on heap (the free store)
  - accessed through _references_ or _pointers_
-
