#include <array>
#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <set>

TEST_CASE("std::set supports") {
  std::set<int> emp;
  std::set<int> fib{1, 1, 2, 3, 5};

  SECTION("default construction") { REQUIRE(emp.empty()); }

  SECTION("braced initialization") { REQUIRE(fib.size() == 4); }

  SECTION("copy construction") {
    auto fib_copy(fib);
    REQUIRE(fib.size() == 4);
    REQUIRE(fib_copy.size() == 4);
  }

  SECTION("move construction") {
    auto fib_moved(std::move(fib));
    REQUIRE(fib.empty());
    REQUIRE(fib_moved.size() == 4);
  }

  SECTION("range construction") {
    std::array<int, 5> fib_array{1, 1, 2, 3, 5};
    // notice the `cbegin` and `cend` methods here
    std::set<int> fib_set(fib_array.cbegin(), fib_array.cend());
    REQUIRE(fib_set.size() == 4);
  }
}

TEST_CASE("std::set allows access") {
  std::set<int> fib{1, 1, 2, 3, 5};

  SECTION("with find") {
    REQUIRE(*fib.find(3) == 3);
    REQUIRE(fib.find(100) == fib.end());
  }

  SECTION("with count") {
    REQUIRE(fib.count(3) == 1);
    REQUIRE(fib.count(100) == 0);
  }

  SECTION("with lower bound") {
    auto itr = fib.lower_bound(3);
    REQUIRE(*itr == 3);
  }

  SECTION("with upper bound") {
    auto itr = fib.upper_bound(3);
    REQUIRE(*itr == 5);
  }

  SECTION("with equal_range") {
    auto pair_itr = fib.equal_range(3);
    REQUIRE(*pair_itr.first == 3);
    REQUIRE(*pair_itr.second == 5);
  }
}

TEST_CASE("std::set allows insertion") {
  std::set<int> fib{1, 1, 2, 3, 8};

  SECTION("with insert") {
    fib.insert(5);
    REQUIRE(fib.find(5) != fib.end());
    auto itr = fib.upper_bound(5);
    REQUIRE(*itr == 8);
  }

  SECTION("with emplace") {
    fib.emplace(5);
    REQUIRE(fib.find(5) != fib.end());
    auto itr = fib.upper_bound(5);
    REQUIRE(*itr == 8);
  }

  SECTION("with emplace_hint") {
    auto itr = fib.find(3);
    REQUIRE(itr != fib.end());

    fib.emplace_hint(itr, 5);
    REQUIRE(fib.find(5) != fib.end());
    auto itr_5 = fib.upper_bound(5);
    REQUIRE(*itr_5 == 8);
  }
}

TEST_CASE("std::set allows removal") {
  std::set<int> fib{1, 1, 2, 3, 5};

  SECTION("with erase") {
    fib.erase(3);
    REQUIRE(fib.find(3) == fib.end());
  }

  SECTION("with clear") {
    fib.clear();
    REQUIRE(fib.empty());
  }
}

TEST_CASE("std::multiset handles non-unique elements") {
  std::multiset<int> fib{1, 1, 2, 3, 5};

  SECTION("as reflected by size") { REQUIRE(fib.size() == 5); }

  SECTION("count returns values greater than 1") { REQUIRE(fib.count(1) == 2); }

  SECTION("equal_range returns non-trivial ranges") {
    auto [begin, end] = fib.equal_range(1);
    REQUIRE(*begin == 1);
    ++begin;
    REQUIRE(*begin == 1);
    ++begin;
    REQUIRE(begin == end);
  }
}
