#include <iostream>
#include <limits>
#include <ostream>

int main() {
  std::cout << "size of long: " << sizeof(1L) << "\n";
  std::cout << "size of long long: " << sizeof(1LL) << "\n";

  std::cout << "largest float == " << std::numeric_limits<float>::max() << "\n";
  std::cout << "is char signed? " << std::numeric_limits<char>::is_signed
            << "\n";
  std::cout << std::endl;
}
