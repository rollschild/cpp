# Types

- **Built-in Types**
  - **Primitive Types**
    - **Integral Types**
      - `bool`
      - `int` and alikes
      - `char` and alikes
    - **Floating-Point Types**
  - pointers
  - references
- **User-defined Types**
  - `enum`
  - `class`
-

## Integer Types

- `short int`
- `int`
- `long int`
- `long long int`

Each integer types can be signed or unsigned.
Integer types are _signed_ and `int` by default.

`unsigned` integer types are ideal for uses that treat storage as a bit array.

Using `unsigned` instead of `int` to gain one more bit to represent positive integers is _almost NEVER_ a good idea. `unsigned` will typically be implicitly converted.

You can use the following short hands:

- `short`
- `long`
- `long long`

To enforce guaranteed integer sizes, use these in the `<cstdint>` header:

- `int8_t`
- `int16_t`
- `int32_t`
- `int64_t`
- `uint_fast16_t` - unsigned int with _at least_ 16 bits - the fastest such integer
- `int_lesat32_t` - signed int with at least 32 bits
  - just like `long`
-

By default, an integer literal's type is one of the following:

- `int`
- `long`
- `long long`

An integer literal's type is the smallest of these types that fits. You can supply _suffixes_ if you really want certain type:

- `u/U` - explicitly `unsigned`
- `l/L` - explicitly `long`
- `ll/LL`

## Floating-Point Types

C++ offers three levels of **precision**s:

- `float`: single precision
- `double`: double precision
- `long double`: extended precision

Good practice is to use `double`?

```c++
double plancks_constant = 6.62607004e-34;
```

- Format specifiers
  - `%f`: displays a `float`
  - `%e`: in scientific notation
  - `%g`: decided by `printf` - the more compact of `%e` or `%f`
  - `%l`: prepend for `double`
    - `%lf`
    - `%le`
    - `%lg`
  - `%L`: prepend for `long double`
    - `%Lf`
    - `%Le`
    - `%Lg`
  - In practice, omit the `l` prefix for `double`, because `printf` promotes `float` arguments to `double` precision

## Character Types

- `char`
  - default, always 1 byte
  - ASCII
- `char16_t`
  - 2 bytes
  - UTF-16
  - `u` prefix
- `char32_t`
  - 4 bytes
  - UTF-32
  - `U` prefix
- `signed char`
  - same as `char` but guaranteed to be signed
- `unsigned char`
  - same as `char` but guaranteed to be unsigned
  - if a value does not fit into the `char`, it will be _truncated_
    - the most significant bits that do not fit will be truncated
    - `unsigned char c = 1256;` - 1256 will be truncated to 232
- `wchar_t`
  - size is implementation-defined
  - to hold the implementation's largest character set
  - LARGE
  - `L` prefix
  - e.g. `L'ab'`
- character literals
  - surrounded by `' '`
  - prepend a prefix for anything but `char` - ???
  - it's possible to enclose _more than one_ character literal, such as `'ab'`
    - type is `int`
    - but it should be avoided
  - `\xad` is _ONE_ char
  - `\xah` is _TWO_ chars - `h` is _NOT_ hex
- Unicode
  - `\u` followed by a 4-digit Unicode code point, or
  - `\U` followed by an 8-digit one
  - `u'\uXXXX'` === `U'\U0000XXXX'` - `X` is _HEX_
- `int{c}` - the `int` can be constructed from a char `c`
- UB:

  ```c++
  char c = 255;
  int i = c; // i may be -1 - 2's complement!
  ```

- a `char` must behave _identically_ to either `signed char` or `unsigned char`
  - however, the three `char` types are _distinct_ - you _CANNOT_ mix pointers to different `char` types!
  - variables of the three `char` types can be assigned to each other,
    - _BUT_ assigning a too large value to `signed char` is _implementation defined_

## User-Defined Suffix

- `"foo bar"s` - literal of `std::string`
- `123_km` - literal of type `Distance`
- Suffixes _NOT_ starting with `_` are _reserved_ for std!

## Boolean Types

- No format specifier for `bool`
- Use `%d` instead
- Because `printf` promotes any integral value smaller than an `int` to `int`
- Logical Operators
  - _unary_
  - _binary_
  - _ternary_
- Nonzero integers will be converted to `true` and _only_ `0` and `nullptr` to `false`
- _ERROR_! - ~~`bool b{9};`~~ - narrowing!
  - you _can_ do `bool b{9 != 0};`
- In arithmetic and bitwise logical expressions, `bool`s are converted to `int`s
- A pointer can be _implicitly_ converted to `bool`
  - prefer `if (ptr)` over `if (ptr != nullptr)`
-

## std::byte

- working with raw memory
- in `<cstddef>`
- for bitwise logical operations

## size_t

- in `<cstddef>`
- guarantees the max value stored in it can represent the max size in bytes of all objects
- Usually `unsigned long long` on 64-bit archs
- `sizeof`
  - returns `size_t`
- Use `%zu` as specifier?

## Array

- contains identically typed variables
- `int array[] = {1, 2, 3, 4, 100}`
- `int array[100];`

## C-Style strings

- `char *`
- strings are contiguous blocks of characters
- zero (`\0`) terminated array of `char`
- can store strings in arrays of character types
- surround string literals in `" "`
- Print Unicode to console
  - use `wprintf` in `<cwchar>`
- `R"` - raw string - no escaped chars
-

## User-Defined Types

- Three broad categories:
  - enumerations
  - classes - the _central_ language feature of C++
  - unions
    - all members share the same memory location
    - **dangerous**

### Enumeration Types

```c++
// scoped enum
enum class Ball {
  Basketball,
  Volleyball,
  Soccerball,
}
Ball basketball = Ball::Basketball;
```

- unscoped enum:

```c++
enum Ball {
  Basketball,
}
```

- use scoped enums wherever possible
- `enum class` and `enum` are two _different_ types!
  - `enum class EnumClass` is of type `EnumClass`, _NOT_ of integral types

### Plain-Old-Data Classes

- POD
- C compatible
- guaranteed to be sequential in memory
- **order members from largest to smallest within POD definitions**

### Unions

- all members in the same place
- **DANGEROUS! AVOID AT ALL COST**

### Fully Featured C++ Classes

- methods
- Access Controls
  - all `struct` members are public by default
- `class`
  - members are **prviate** by default
- class invariant
  - can be enforaced by throwing execptions during the construction phase of the class
- functions defined in a class are _inlined_ _by default_
-

## Initialization

- _braced initialization_
  - **always applicable**
- fully-featured classes are always initialized
- Problems:
  - **most vexing parse**
  - **narrowing conversions**
- **Use braced initializers everywhere**
  - a.k.a. **uniform initialization**

## Destructor

- called before object being destroyed
- almost never called explicitly
- NO args
- Possible operations in a destructor:
  - release file handles
  - flush network sockets
  - free dynamic objects

## Unspecified

- Use `numeric_limits` to test hardware arch
- functions in `<limits>` are `constexpr`
- Use static assertions
  - `static_assert(4 <= sizeof(int), "sizeof(int) too small!");`
- `ptrdiff_t` - in `<cstddef>`
  - type of the result of subtracting two pointers
  - signed integral type

## Alignment

- `alignof()` - returns the alignment of its argument _type_

```c++
int a[20];

// alignment of an array of int
// most likely to be 4
auto aa = alignof(decltype(a));
```

- when `alignof(T)` is not allowed, try using the type specifier `alignas: alignas(T)`
  - "align just like T"

## Declarations

- Most of the _declarations_ are also _Definitions_
  - if it takes memory to represent something, that memory is _set aside_ by its definition
- _Five_ parts of a declaration:
  - _optional_ prefix specifier (`static` or `virtual`)
  - _base_ type (`vector<double>` or `const int`)
  - a _declarator_ _optionally_ including a name (`p[7]`)
  - _optional_ suffix function specifiers (`const` or `noexcept`)
  - _optional_ initializer or function body
- `char*kings[]` - an array of pointers to `char`
- `char(*kings)[]` - a pointer of an array of `char`

### Names

- _Nonlocal_ names _starting_ with an underscore `_` are reserved for special facilities in the implementation and the run-time environment
- Names containing a double underscore `__` _or_ an underscore followed by an uppercase letter `_Foo` are also reserved
  > Choose names to reflect the **meaning** of an entity rather than its implementation.

> Do _NOT_ encode type information in a name.

> Use ALL*CAPTITALS for macros; \_NEVER* for non-macros

- Hiding names: a name can be redefined to a different entity within a block
  - a.k.a. _shadowing_
  - try not to use too short names such as `i` or `x` for global/local variables
    - statement-scope may be fine?
- The scope of a name that is _not a class member_ starts at its point of declaration:
  - _after_ the complete declarator, and
  - _before_ the initializer
- ^ means a name can be used to specify its own initial value: `int x = x;`

### Initialization

- Generally, recommended to use \***\*list initialization\*\*** `{}`
  - it does _NOT_ allow narrowing
- _BUT_, when using `auto` to deduce the type from the initializer, try _NOT_ to use `{}`
  - `auto z{99};` - `z` is of type `initializer_list<int>`
  - prefer `=` when using `auto`
- _ALWAYS_ initialize?

> The only really good case for an uninitialized variable is a large input buffer
> By redundantly initializing, we would have suffered a significant performance hit

- If no initializer specified, the following will be initialized to `{}` of the appropriate type
  - global variable
  - namesapce variable
  - local `static`
  - `static` member
- `int* p{new int{10}};` - `*p` is `10`
- `vector<double> v(10, 3.3);` - initialized via _constructor_ - 10 elements all initialized to 3.3

### Deducing types

- _Two_ mechanisms to deduce a type from an expression:
  - `auto`
  - `decltype(expr)`
- `auto` - deduces type of an object from its initializer
- `decltype(expr)` - deduces type of something that is _not_ a simple initializer, such as:
  - return type for a function
  - type of a class member
- The type of an expression is _NEVER_ a reference - references are _implicitly dereferenced_ in expressions

  ```c++
  int& v;
  auto x = v; // x is of type int
  auto& y = v; // y is of type int&
  ```

- `char c{12345};` would _ERROR_ - narrowing!
- `decltype(expr)` - the declared type of `expr`
- Use the **suffix return type syntax** to be able to express the return type in terms of the _arguments_

## Objects and Values

- \***\*lvalue\*\*** - an expression that refers to an object
- **temporary value**
  - such as value returned by a function
- Rules:
  ![image-of-lvalue-and-rvalue](lvalue-rvalue.jpg)
- Lifetime:
  - starts when its constructor _completes_
  - ends when its destructor _starts executing_
- type alias:
  - `using Pchar = char*;`
  - `using PF = int(*)(double);`
  - `using int32_t = int;` - the `_t` suffix is conventional for "aliases" (or, `typedef`s)
-
