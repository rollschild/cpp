#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <iterator>

struct Book {
  char name[256];
  int year;
  int pages;
  bool hardcover;
};

int primitive_types() {
  unsigned int a = 3669732608;
  // printf("In hex form: %x\n", a);

  unsigned int b = 69;
  // printf("In octal form: %o\n", b);

  // unsigned long maximum = 0;
  // unsigned long values[] = {10, 50, 20, 40, 0};
  int64_t maximum = 0;
  int64_t values[] = {10, 50, 20, 40, 0};
  size_t n_elements = sizeof(values) / sizeof(int64_t);
  // size_t n_elements = std::size(values);
  /* for (size_t i = 0; i < 5; i++) {
   *   if (values[i] > maximum) {
   *     maximum = values[i];
   *   }
   * } */
  for (int64_t value : values) {
    if (value > maximum) maximum = value;
  }

  printf("Max value is %lu in an array of %zu elements\n", maximum, n_elements);

  return 0;
}

int array() {
  char english[] = "this is a string";
  char16_t chinese[] = u"\u4e66\u4e2d";
  printf("some English: %s\n", english);

  return 0;
}

int pod() {
  Book cpp_crash_course{};
  cpp_crash_course.pages = 733;
  return 0;
}

struct Clock {
  Clock(int new_year) {
    if (!set_year(new_year)) {
      // Exceptions are better suited for this
      // On this topic later
      year = 2022;
    }
  }
  void add_year() { year++; }
  bool set_year(int new_year) {
    if (new_year < 2022) return false;
    year = new_year;
    return true;
  }
  int get_year() { return year; }

 private:
  int year;
};

class ClockClass {
  int year;

 public:
  void add_year() { year++; }
  bool set_year(int new_year) {
    if (new_year < 2022) return false;
    year = new_year;
    return true;
  }
  int get_year() { return year; }
};

struct Taxonomist {
  Taxonomist() { printf("No args!\n"); }
  Taxonomist(char x) { printf("char: %c\n", x); }
  Taxonomist(int x) { printf("int: %d\n", x); }
  Taxonomist(float x) { printf("float: %f\n", x); }
};

int main() {
  Clock clock{2025};
  printf("clock's default year: %d\n", clock.get_year());
}
