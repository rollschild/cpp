/*
 * Copyright 2023 Guangchu Shi
 */
#include <cstddef>
#include <iostream>
#include <vector>

template <typename T>
struct Matrix {
  std::vector<T> matrix;
  size_t size;
  Matrix(size_t sz, T init_value)
      : size{sz}, matrix{std::vector<T>(sz, init_value)} {}
  void print() {
    for (auto e : matrix) {
      std::cout << e << " ";
    }
    std::cout << std::endl;
  }
  size_t get_size() const { return size; }
  void set(int pos, T val) { matrix[pos] = val; }
  T get_val(int pos) const { return matrix[pos]; }
  template <typename U>
  auto operator+(const Matrix<U>& rhs) -> Matrix<decltype(T{} + U{})> const {
    decltype(T{} + U{}) init{};
    Matrix<decltype(T{} + U{})> res{size, init};
    for (auto i = 0; i < size; ++i) {
      res.set(i, matrix[i] + rhs.get_val(i));
    }
    return res;
  }
};

int main() {
  Matrix<int> mi{6, 3};
  Matrix<char> mc{6, 'b'};

  auto res = mi + mc;

  res.print();

  return 0;
}
