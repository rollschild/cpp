# Filesystems

## Filesystem Concepts

- A **file** is a filesystem object that supports input/output and holds data
- **directories** are considered files
- **hard link**: a directory entry that assigns a name to an existing file
- **symbolic link** (**symlink**):
  - assigns a name to a path, which might or might not exist
- **canonical path**:
  - unambiguously identifies a file's location,
  - doesn't contain special names `.` or `..`
  - doesn't contain symlinks
- **absolute path**:
  - any path that unambiguously identifiesa file's location
- **canonical path** vs. **absolute path**
  - a canonical path **cannot** contain `.` or `..`

## `std::filesystem::path`

- in `<filesystem>`
- for modeling a path

### Constructing Paths

- the `path` class supports comparison
  - with other `path` objects
  - with `string` objects
  - using `operator==`
- to check whether the `path` is empty: `.empty()`
- `std::filesystem::path` has an `operator<<` that prints quotation marks at the beginning & end of its path
  - internally it uses `std::quoted` in `<iomanip>`

### Modifying Paths

- File described by the path does **not** need to exist
  - just the path being manipulated

## Files and Directories

- the `path` class does **not** actually interact with the filesystem
  - instead, `<filesystem>` contains non-member functions for that

## Directory Iterators

- Two classes to iterate over the elements of a directory:
  - `std::filesystem::directory_iterator` - does **NOT** enter subdirecotries
  - `std::filesystem::recursive_directory_iterator` - enters subdirecotries

## `fstream` Interoperation

- File streams (`basic_ifstream`, `basic_ofstream`, `basic_fstream`) can be constructed using `std::filesystem::path` or `std::filesystem::directory_entry` in addition to string types
-
