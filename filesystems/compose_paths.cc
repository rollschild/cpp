#include <filesystem>
#include <iostream>

using namespace std;

int main() {
  try {
    const auto temp_path = filesystem::temp_directory_path();
    const auto relative = filesystem::relative(temp_path);

    cout << boolalpha << "Temp dir path: " << temp_path
         << "\nTemp dir absolute: " << temp_path.is_absolute()
         << "\nCurrent path: " << filesystem::current_path()
         << "\nTemp dir's relative path: "
         << relative  // temp_path's path relative to the current path
         << "\nRelative dir absolute: " << relative.is_absolute()
         << "\nChanging current dir to temp.";

    filesystem::current_path(temp_path);
    cout << "\nCurrent dir: " << filesystem::current_path();
  } catch (const exception& e) {
    cerr << "Error: " << e.what();
  }
}
