#include <filesystem>
#include <iostream>

using namespace std;

int main() {
  filesystem::path path{
      R"(/home/rollschild/projects/cpp/build/CMakeCache.txt)"};
  cout << path << endl;

  path.make_preferred();
  cout << path << endl;

  path.replace_filename("CMakeCacheCooy.txt");
  cout << path << endl;

  path.remove_filename();
  cout << path << endl;

  path.clear();
  cout << path << endl;  // ""
  cout << "Is empty? " << boolalpha << path.empty() << endl;
}
