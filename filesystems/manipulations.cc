#include <filesystem>
#include <iostream>

using namespace std;
using namespace std::filesystem;
using namespace std::chrono;

void write_info(const path& p) {
  if (!exists(p)) {  // check whether path `p` exists
    cout << p << " does not exist!" << endl;
    return;
  }

  const auto last_write = last_write_time(p).time_since_epoch();
  cout << "last_write: " << last_write.count() << endl;
  // in hours since epoch
  const auto in_hours = duration_cast<hours>(last_write).count();
  cout << p << "\t" << in_hours << "\t" << file_size(p) << "\n";
}

int main() {
  const path empty_txt_path{R"(/home/rollschild/projects/cpp/build/empty.txt)"};
  // const auto non_exist_txt_path = {
  //     R"(/home/rollschild/projects/cpp/build/non_exist.txt)"};

  // /tmp
  // use `/` to concatenate two path objects
  const auto non_exist_txt_path = temp_directory_path() / "non_exist.txt";

  try {
    write_info(empty_txt_path);
    write_info(non_exist_txt_path);

    cout << "Copying... " << empty_txt_path.filename() << "\n"
         << " to " << non_exist_txt_path.filename() << "\n";
    copy_file(empty_txt_path, non_exist_txt_path);
    write_info(non_exist_txt_path);

    cout << "Resizing... " << non_exist_txt_path.filename() << "\n";
    resize_file(non_exist_txt_path, 1024);
    write_info(non_exist_txt_path);

    cout << "Removing... " << non_exist_txt_path.filename() << "\n";
    remove(non_exist_txt_path);
    write_info(non_exist_txt_path);
  } catch (const exception& e) {
    cerr << "Exception: " << e.what() << endl;
  }
}
