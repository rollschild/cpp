#include <filesystem>
#include <iostream>

using namespace std;

int main() {
  // construct a path using a **raw string literal**
  // to avoid having to excape slashes
  const filesystem::path pwd{
      R"(/home/rollschild/projects/cpp/filesystems/decompose_paths.cc)"};
  cout << "Root name: " << pwd.root_name();
  cout << "\nRoot directory: " << pwd.root_directory();
  cout << "\nRoot path: " << pwd.root_path();
  cout << "\nRelative path: " << pwd.relative_path();
  cout << "\nParent path: " << pwd.parent_path();
  cout << "\nFile name: " << pwd.filename();
  cout << "\nStem: " << pwd.stem();
  cout << "\nExtension: " << pwd.extension();
  cout << endl;
}
