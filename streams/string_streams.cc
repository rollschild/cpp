#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <sstream>
#include <string>

TEST_CASE("ostringstream produces strings with str") {
  std::ostringstream ss;
  ss << "This is the first. ";
  ss << "This is the second. ";
  ss << "This is the third...";
  const auto sentence = ss.str();

  ss.str("New sentence.");
  const auto new_sentence = ss.str();

  REQUIRE(sentence ==
          "This is the first. This is the second. This is the third...");
  REQUIRE(new_sentence == "New sentence.");
}

TEST_CASE("istringstream supports construction from a string") {
  std::string numbers{"1 3.141592657 2"};
  std::istringstream ss{numbers};
  int a;
  float b, c, d;
  ss >> a;
  ss >> b;
  ss >> c;
  REQUIRE(a == 1);
  REQUIRE(b == Catch::Approx(3.141592657));
  REQUIRE(c == Catch::Approx(2));

  // the stream is exhausted and the output operator fails,
  // ss converts to `false`
  REQUIRE_FALSE(ss >> d);
}

TEST_CASE("stringstream supports all string stream operations") {
  std::stringstream ss;
  ss << "Zed's DEAD";

  std::string who;
  ss >> who;
  int what;
  ss >> std::hex >> what;

  REQUIRE(who == "Zed's");
  REQUIRE(what == 0xdead);
}
