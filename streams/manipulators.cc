#include <iomanip>
#include <iostream>

using namespace std;

int main() {
  cout << "This is a " << boolalpha << true << " sentence.";
  cout << "\nAnd this is not: " << noboolalpha << false << "!" << endl;
  cout << hex << setw(4) << "\n"
       << 0x1 << "\n"
       << 0x10 << "\n"
       << 0x100 << "\n"
       << 0x10000 << endl;
}
