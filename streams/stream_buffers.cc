#include <iostream>
#include <iterator>
#include <string>

using namespace std;

int main() {
  ostreambuf_iterator<char> itr{cout};
  *itr = 'H';
  ++itr;
  *itr = 'i';
  ++itr;
  *itr = '!';
  ++itr;
  *itr = '\n';

  istreambuf_iterator<char> cin_itr{cin.rdbuf()};
  istreambuf_iterator<char> end{};

  cout << "What is your name? ";

  // construct a string using the range-based constructor
  const string name{cin_itr, end};
  cout << "\nByebye... " << name;
}
