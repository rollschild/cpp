#include <iostream>
#include <string>

struct Entry {
  std::string name;
  int number;
};

std::ostream& operator<<(std::ostream& os, const Entry& e) {
  return os << "{\"" << e.name << "\"," << e.number << "}";
}

std::istream& operator>>(std::istream& is, Entry& e) {
  char c1;
  char c2;

  if (is >> c1 && c1 == '{' && is >> c2 && c2 == '"') {
    std::string name;
    while (is.get(c1) && c1 != '"') {
      name += c1;
    }

    if (is >> c1 && c1 == ',') {
      int number{};
      if (is >> number >> c1 && c1 == '}') {
        e = {name, number};
        return is;
      }
    }
  }

  is.setstate(std::ios_base::failbit);
  return is;
}

int main() {
  Entry person1{"Jovi", 1234};
  std::cout << person1 << std::endl;

  std::cout << "Your name is: ";
  Entry person2;
  std::cin >> person2;
  std::cout << "You entered: " << person2 << std::endl;
}
