find_package(Catch2 3 REQUIRED)

add_executable(input_operator)
target_sources(input_operator PRIVATE input_operator.cc)

add_executable(stream_state)
target_sources(stream_state PRIVATE stream_state.cc)

add_executable(manipulators)
target_sources(manipulators PRIVATE manipulators.cc)

add_executable(user_defined_types)
target_sources(user_defined_types PRIVATE user_defined_types.cc)

add_executable(string_streams)
target_sources(string_streams PRIVATE string_streams.cc)
target_link_libraries(string_streams PRIVATE Catch2::Catch2WithMain)
include(Catch)
catch_discover_tests(string_streams)

add_executable(file_streams)
target_sources(file_streams PRIVATE file_streams.cc)
target_link_libraries(file_streams PRIVATE Catch2::Catch2WithMain)
include(Catch)
catch_discover_tests(file_streams)

add_executable(stream_buffers)
target_sources(stream_buffers PRIVATE stream_buffers.cc)
# target_link_libraries(stream_buffers PRIVATE Catch2::Catch2WithMain)
# include(Catch) catch_discover_tests(stream_buffers)

add_executable(random_access)
target_sources(random_access PRIVATE random_access.cc)

add_executable(phonebook)
target_sources(phonebook PRIVATE phonebook.cc)
