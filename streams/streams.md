# Streams

## Streams

### Stream Classes

- A **stream** models a stream of data
- primary mechanism usded by C++ to perform I/O
- The primary types in STL:
  - `std::basic_ostream` in `<ostream>`
  - `std::istream` in `<istream>`
  - `std::basic_iostream` in `<iostream>`
- By default, a whitespace character, such as a whitespace, terminates the read
- Convention:
  - a wrapper (RAII) class should own (and eventually deletes) an `istream` passed as a pointer, but _NOT_ an `istream` passed as a reference
- `operator>>` applied to a `string` reads _UNTIL_ it hits whitespace
- use `isspace` is _much faster_ than testing for individual whitespace characters
  - because it's implemented as a table lookup
  - similar functions:
    - `isdigit()`
    - `isalpha()`
    - `isalnum()`
-

#### Global Stream Objects

- in `<iostream>`
- wrap the input, output, and error streams stdin, stdout, and stderr

#### Formatted Operations

- All formatted I/O passes through two functions:
  - the standard stream operators
  - `operator<<` and `operator>>`
- `operator<<` - output operator (or inserter)
- standard stream operators generally return a reference to the stream
  - `ostream& operator<<(ostream&, char);`
- `operator>>` - input operator (or extractor)
- `istream>>` _skips_ whitespace by default
- `istream.get()` does _not_ skip whitespace by default
  - returns `true` if reading is success
  - returns `false` if no characters can be read from `cin`!

#### Unformatted Operations

- when working with binary data or writing code that needs low-level access to streams

#### Special Formatting for Fundamental Types

- all fundamental types, in addition to `void` and `nullptr`, have input and output operator overloads
- Avoid reading into C-style strings!
  - use `std::string` instead

### Stream State

- whether I/O failed
- each stream type exposes the constant static memebers referred to collectively as its **bits** - indicating a possible stream state:
  - `goodbit` - `good()`
  - `badbit` - `bad()`
  - `eofbit` - `eof()`
  - `failbit` - `fail()`
- To throw exceptions when certain fail bits occur,
  - use the stream's `exceptions` method
  - wrapping a single argument corresponding to the bit you want to throw exceptions
  - if multiple bits are wanted, join them together using `|`

### Buffering and Flushing

### Manipulators

- special objects that modify how streams interpret input/output
- `std::ws` modifies an `istream` to skip over whitespaces
- General rule:
  - use `std::endl` when your program has finished outputting text to the stream for a while
  - use `\n` when you know your program will output more text soon
- Manipulators apply to _all_ subsequent objects

### User-Defined Types

- by implementing certain non-member functions
- `ostream& operator<<(ostream& s, const YourType& m);`
- `istream& operator>>(istream& s, YourType& m);`

### String Streams

- read from and write to character sequences

#### Output String Streams

- derived from the class template `std::basic_ostringstream` in `<sstream>`
  - `using ostringstream = basic_ostringstream<char>;`
  - `using wostringstream = basic_ostringstream<wchar_t>;`
- supports a `str()` method
  - without arguments, returns a copy of the internal buffer as `basic_string`
  - with an argument, replaces its buffer's contents with the argument

#### Input String Streams

- derived from `std::basic_istringstream` in `<sstream>`
  - `using istringstream = basic_istringstream<char>;`
  - `using wistringstream = basic_istringstream<wchar_t>;`

#### String Streams supporting input and output

- `basic_stringstream`
  - `using stringstream = basic_stringstream<char>;`
  - `using wstringstream = basic_stringstream<wchar_t>;`
- _All_ string streams are moveable

### File Streams

- the file stream classes are RAII wrappers
  - impossible to leak resources
- support move semantics
  - can have tight control over where files are in scope

#### Opening files with streams

- Two ways to open a file with any file stream:
  - use the `open` method, which accepts
    - a `const char* filename`, and
    - an optional `std::ios_base::openmode` bitmask
  - use the stream's constructor
- use `is_open()` method to check whether a file is open

#### Output File Streams

- derived from `std::basic_ofstream` in `<fstream>`
  - `using ofstream = basic_ofstream<char>;`
  - `using wofstream = basic_ofstream<wchar_t>;`

#### Input File Streams

- derived from `std::basic_ifstream` in `<fstream>`
  - `using ifstream = basic_ifstream<char>;`
  - `using wifstream = basic_ifstream<wchar_t>;`

#### Handling Failure

- file stream fail silently
- You _must_ check the `is_open()` method to determine whether the stream successfully opened the file, when trying to use a file stream to open a file

### Stream Buffers

- streams do **not** read/write directly
  - they use stream buffer classes
- stream buffer classes are templates that send/extract characters

#### Writing Files to stdout

- `cout << my_ifstream.rdbuf()`

#### Error

- `std::cerr`
  - an _UNbuffered_ output stream

#### Output Stream Buffer Iterators

- template classes that expose an output iterator interface that translates writes into output operations on the underlying stream buffer
- to construct an output stream buffer iterator, use `ostreambuf_iterator`
  - it takes a single output stream argument and a single template parameter corresponding to the constructor argument's template parameter (the character type)

#### Input Stream Buffer Iterators

- to construct, use `istreambuf_iterator`
  - takes a stream buffer argument
    - you must call `rdbuf()`
  - this argument is _optional_
    - default constructor of `istreambuf_iterator` corresponds to the end-of-range iterator of input iterator

### Random Access

- For input streams:
  - `tellg()`
  - `seekg()`
  -
