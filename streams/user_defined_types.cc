#include <deque>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
ostream& operator<<(ostream& s, vector<T> v) {
  s << "Size: " << v.size() << "\nCapacity: " << v.capacity()
    << "\nElements:\n";
  for (const auto& element : v) {
    s << "\t" << element << "\n";
  }
  return s;
}

template <typename T>
istream& operator>>(istream& s, deque<T>& t) {
  T element;
  while (s >> element) {
    t.emplace_back(move(element));
  }
  return s;
}

int main() {
  const vector<string> characters{
      "Kobe Bryant", "Michael Jordan", "Allen Iverson",
      "Gary Peyton", "John Stockton",
  };

  cout << characters << endl;

  const vector<bool> bits{true, false, false, true};
  cout << boolalpha << bits << endl;

  cout << "Give me numbers: ";
  deque<int> numbers;
  cin >> numbers;
  int sum{};
  cout << "Cumulative sum:\n";
  for (const auto& element : numbers) {
    sum += element;
    cout << sum << "\n";
  }
}
