#include <iostream>
#include <string>

int main() {
  std::cin.exceptions(std::istream::badbit);
  std::string word;
  size_t count{};

  try {
    while (std::cin >> word) {
      count++;
    }
    std::cout << "Discovered " << count << " words.\n";
  } catch (const std::exception& e) {
    std::cerr << "Error occurred reading from stdin: " << e.what();
  }
}
