#include <catch2/catch_approx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <fstream>
#include <iostream>
#include <limits>

using namespace std;

ifstream open(const char* path, ios_base::openmode mode = ios_base::in) {
  ifstream file(path, mode);
  if (!file.is_open()) {
    string err{"Unable to open file "};
    err.append(path);
    throw runtime_error{err};
  }
  // tell the resulting ifstream to throw an exception whenever its `badbit`
  // gets set in the future
  file.exceptions(ifstream::badbit);
  return file;
}

int main() {
  ofstream file{"file_stream.txt", ios::out | ios::app};
  file << "Time is an illusion." << endl;
  file << "The entire universe is an illusion as well."
       << " Yeah, I said that." << endl;

  ifstream i_file{"numbers.txt"};
  auto maximum = numeric_limits<int>::min();

  int value;
  while (i_file >> value) {
    maximum = maximum < value ? value : maximum;
  }

  cout << "Max found was: " << maximum << endl;
}
