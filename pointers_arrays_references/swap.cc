#include <iostream>
#include <ostream>
#include <string>
#include <utility>
#include <vector>

// This version of swap need not make any copies
// it will use move operations whenever possible
// But for it (without overloading) only swaps lvalues
template <typename T>
void swap(T& a, T& b) {
  // T tmp{static_cast<T&&>(a)};
  // a = static_cast<T&&>(b);
  // b = static_cast<T&&>(tmp);

  // Less verbose implementation:
  T tmp{std::move(a)};
  a = std::move(b);
  b = std::move(tmp);
}

template <typename T>
void swap(T&& a, T& b) {
  T tmp{std::move(a)};
  a = std::move(b);
  b = std::move(tmp);
}

template <typename T>
void swap(T& a, T&& b) {
  T tmp{std::move(b)};
  b = std::move(a);
  a = std::move(tmp);
}

int main() {
  std::string a{"string a..."};
  std::string b{"string b..."};
  swap(a, b);
  std::cout << "a: " << a << "\n";
  std::cout << "b: " << b << "\n";
  std::cout << std::endl;

  std::vector<int> v0{10, 11, 12};

  // This will not compile if no overloads for swap
  swap(v0, std::vector<int>{0, 1});

  for (auto& ele : v0) {
    std::cout << ele << " ";
  }
  std::cout << std::endl;
}
