#include <iostream>

template <typename T>
int byte_diff(T* p, T* q) {
  return reinterpret_cast<char*>(q) - reinterpret_cast<char*>(p);
}

int main() {
  int vi[10];
  short vs[10];

  std::cout << vi << ' ' << &vi[1] << ' ' << vi - &vi[1] << ' '
            << &vi[1] - &vi[0] << ' ' << byte_diff(&vi[0], &vi[1]) << '\n';
  std::cout << vs << ' ' << &vs[1] << ' ' << &vs[1] - &vs[0] << ' '
            << byte_diff(&vs[0], &vs[1]) << '\n';
}
