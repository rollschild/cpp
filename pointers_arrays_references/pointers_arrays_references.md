# Pointers, Arrays, and References

## Intro

### Pointers

- _Few_ machines can directly address an individual bit
- The smallest object that can be independently allocated and pointed to using a built-in pointer type is `char`
- `bool` occupies _at least as much space as_ `char`
- To store smaller values more compactly, use:
  - bitwise logical operations
  - bit-fields in structures, or
  - `bitset`
- `void*` - pointer to an object of unknown type
  - a pointer to _any_ type of object can be assigned to `void*` _except for_:
    - pointer to function
    - pointer to class member
    - _BUT_ not the other way around!
  - `void*` can be assigned to another `void*`
  - `void*`s can be compared for _equality_ and _inequality_
  - `void*` can be _explicitly_ converted to another type
  - other operations - _UNSAFE_
- `void*` _CANNOT_ be directly dereferenced
- `void*` _CANNOT_ be directly incremented (size of the underlying object _unknown_)
- use `static_cast<T*>(void*)` to explicitly convert
- In general, _NOT_ safe to use a pointer that has been converted (cast) to a type that differs from the type of the object pointed to
- The primary use for `void*`:
  - pass pointers to functions that are _not_ allowed to make assumptions about the type of the object
  - return untyped objects from functions
- `nullptr` can be assigned to _any_ pointer type
  - but _NOT_ other built-in types
- _NO_ object is allocated with address `0`
- `0` is the _most common_ representation of `nullptr`

### Arrays

- number of elements of the array (not allocated using `new`), the _array bound_, must be a **constant expression**
  - use `vector` if the bound is variable
- Ideal for a simple _fixed-length_ sequence of objects of _a given type_ _in memory_
- can either be on stack or on heap
- _NEVER_ `delete[]` if array is on stack!
- Most common array: zero-terminated array of `char`
  - C-style string
- `char*` & `const char*` point to `\0`-terminated array of `char`s
- _NO_ built-in copy operation for arrays
- _CANNOT_ initialize one array with another
- _NO_ array assignment
- Arrays _CANNOT_ be passed by value
- type of a string literal: `const char[N]`
  - NOTICE here the type has `const`!
- String literals are _IMMUTABLE_!
  - allows for significant optimizations for storing/accessing string literals
- If a _mutable_ string is desired, use non-`const` array
  - `char p[] = "Some chars..";`
- String literal is _statically_ allocated
  - safe to be returned from a function
- Whether tow identical string literals are allocated as one array or two is _implementation-defined_
- empty string - `""`
  - has type `const char[1]`
- Raw String Literals
  - where, a backslash is just a backslash
  - a double quote is _just_ a double quote
  - `R"(ccc)"`
  - the parentheses `()` allow for _unescaped_ double quotes
  - _can_ contain a newline
  - e.g. a regex pattern: two words separated by a backslash (`\`)
    - `string s = R"(\w+\\\w+)";`
- Large Character Sets
  - `L"somelargechars"` - of type `const wchar_t[]`
  - `LR"somelongrawstring"` - _raw string_ of type `const wchar_t[]`
  - terminated by `L'\0'`
- UTF-8, UTF-16, and UTF-32
  - terminated by `'\0'`, `u'\0'`, and `U'\0'`

## Pointers into Arrays

- Name of an array can be used as pointer to its initial element
- Taking a pointer to the element one beyond the end of an array is _guaranteed to work_!
- There is _NO_ implicit/explicit conversion from a pointer to an array!!!
- `a[j] == *(&a[0] + j) == *(a + j) == *(j + a)`
- When subtracting a pointer `p` from another `q`, `q - p`, the result is _the number of array elements in the sequence `[p:q)` - an INTEGER_
- range-`for` works for arrays with _known_ size
- Arrays _CANNOT_ directly be passed by value
  - actually passed as a pointer to its first element
  - When used as a function argument, the first dimension of an array is simply treated as a pointer - any array bound specified is simply ignored
- If you want to pass a sequence of elements _without_ losing size info, a built-in array should _NOT_ be used
- when passing multidimensional array as an arg to a func, the second dimension must be known at compile-time

## Pointers and `const`

- `const int v[] = {1, 2, 3, 4};` - `v[i]` is a `const`
- `const T* p` - pointer `p` to `const T`
- `T* const p` - `const` pointer `p` to `T`
- Address of a non-`const` variable _can_ be assigned to a pointer to `const`
- Address of a `const` _CANNOT_ be assigned to an _unrestricted_ pointer

## Pointers and Ownership

- Resources: acquired, then released
  - `new` and `delete`
  - `fopen()` and `fclose()`
- It's usually a good idea to immediately place a pointer that represents ownership in a resource handle class, such as `vector`, `string`, and `unique_ptr`
  - so that it's safe to assume that every pointer that is _not_ within a resource handle is _not_ an owner and must _NOT_ be `delete`d

## References

- An alias for an object
- Usually implemented to hold a machine address of an object
- Does _NOT_ impose performance overhead compared to pointers
- A reference _ALWAYS_ refers to the object to which it was initialized
- \***\*lvalue references\*\*** & \***\*rvalue references\*\***
- `extern int& r;` is valid - `r` initialized elsewhere
- _NO_ operator actually operates on a reference
- `&rr` - get a pointer to the object denoted by reference `rr`
- _CANNOT_ have a pointer to a reference
- _CANNOT_ define array of references
  - why?
- _CANNOT_ have a reference to a reference
- Because references are _NOT_ objects - they don't occupy the memory so they don't have an address
  - array would be implicitly converted to pointers to references - ILLEGAL!
- Initialization of a reference:
  - initializer should be an **lvalue**
  - however, initializer for `const T&` need _NOT_ be lvalue or type of `T`

```c++
double& dr = 1; // ERROR
const double& cdr{1}; // OK
```

- The last line can be implemented as:

```c++
double temp = double{1};      // first create a temporary with the right value
const double& cdr {temp};     // then use the temporary as the initializer for cdr
```

- Rvalue reference
  - An rvalue reference can bind to an rvalue, but _NOT_ to an lvalue
  - An rvalue reference is exactly opposite to an lvalue reference
  - `&&` - rvalue reference

```c++
string f();

string&& rr1{f()}; // rvalue reference, bind to rvalue
string&& rr2{"Some string"}; // rr2 refers to a temporary holding a strign
const string& cr{"Hey..."};
```

- `const` lvalue reference vs. rvalue reference
  - Use rvalue references to implement a "destructive read" for _optimization_ of what would otherwise have required a _copy_
  - Use `const` lvalue reference to prevent modification of an argument
- `static_cast<T&&>(x)` - rvalue of type `T&&` for `x`
- stdlib provides `move()` for this ^
  - `std::move(x)` means `static_cast<X&&>(x)`
- References to references
  - _ONLY_ valid for:
    - an alias, such as
    ```c++
    using rr_i = int&&;
    using lr_i = int&;
    using rr_rr_i = rr_i&&; // int&&
    ```
  - lvalue reference _ALWAYS_ wins
  - lvalue reference refers to an lvalue!
  - _INVALID_! - `int&& & r = i;`
- A reference is _NOT_ an object!
- In many cases, a reference can be optimized away by compiler
- It's allowed (but is it safe though?) to assume that a reference is valid
-
