#include <iostream>
#include <utility>
#include <vector>

using std::pair;
using std::vector;

template <typename K, typename V>
class Map {
 private:
  vector<pair<K, V>> elem;

 public:
  V& operator[](const K& key);
  pair<K, V>* begin() { return &elem[0]; }
  pair<K, V>* end() { return &elem[0] + elem.size(); }
};

template <typename K, typename V>
V& Map<K, V>::operator[](const K& key) {
  for (auto& ele : elem) {
    if (key == ele.first) return ele.second;
  }
  elem.push_back({key, V{}});  // add new pair
  return elem.back().second;
}
