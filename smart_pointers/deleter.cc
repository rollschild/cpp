#include <cstdio>
#include <memory>

// function pointer?
using FileGuard = std::unique_ptr<FILE, int (*)(FILE*)>;

void write(FileGuard file) { fprintf(file.get(), "Hey there!"); }

int main() {
  // if using cmake, this HAL9000 files exists in build/ directory
  auto file = fopen("HAL9000", "w");
  if (!file) return errno;
  FileGuard file_guard{file, fclose};
  // file opened here
  // you need to transfer ownership of the file
  // because the function takes a FileGuard by value
  write(std::move(file_guard));
  // file closed here
  return 0;
}
