#include <catch2/catch_test_macros.hpp>
#include <memory>

struct DeadMen {
  DeadMen(const char* m = "") : message{m} { oaths++; }
  ~DeadMen() { oaths--; }

  const char* message;
  static int oaths;
};

int DeadMen::oaths{};
using SharedOathbreakers = std::shared_ptr<DeadMen>;

TEST_CASE("SharedPtr can be used in copy") {
  auto aragorn = std::make_shared<DeadMen>();
  SECTION("construction") {
    auto son{aragorn};
    REQUIRE(DeadMen::oaths == 1);
  }
  SECTION("assignment") {
    SharedOathbreakers son;
    son = aragorn;
    REQUIRE(DeadMen::oaths == 1);
  }
  SECTION("assignment, and original gets discarded") {
    auto son = std::make_shared<DeadMen>();
    REQUIRE(DeadMen::oaths == 2);

    // here, son deletes its DeadMen because it has sole ownership
    son = aragorn;

    REQUIRE(DeadMen::oaths == 1);
  }
}
