#include <boost/smart_ptr/scoped_array.hpp>
#include <boost/smart_ptr/scoped_ptr.hpp>
#include <catch2/catch_test_macros.hpp>

struct DeadMen {
  DeadMen(const char* m = "") : message{m} { oaths++; }
  ~DeadMen() { oaths--; }

  const char* message;
  static int oaths;
};

int DeadMen::oaths{};
using ScopedOathbreakers = boost::scoped_ptr<DeadMen>;

TEST_CASE("ScopedPtr evaluates to") {
  SECTION("true when full") {
    ScopedOathbreakers aragorn{new DeadMen{}};
    REQUIRE(aragorn);
  }
  SECTION("false when empty") {
    ScopedOathbreakers aragorn;
    REQUIRE_FALSE(aragorn);
  }
}

TEST_CASE("ScopedPtr is an RAII wrapper") {
  REQUIRE(DeadMen::oaths == 0);
  ScopedOathbreakers aragorn{new DeadMen{}};
  REQUIRE(DeadMen::oaths == 1);
  {
    ScopedOathbreakers legolas{new DeadMen{}};
    REQUIRE(DeadMen::oaths == 2);
  }
  REQUIRE(DeadMen::oaths == 1);
}

TEST_CASE("ScopedPtr supports pointer semantics, like") {
  auto message = "The way is shut down...";
  ScopedOathbreakers aragorn{new DeadMen{message}};
  SECTION("operator*") { REQUIRE((*aragorn).message == message); }
  SECTION("operator->") { REQUIRE(aragorn->message == message); }
  SECTION("get(), which returns a raw pointer") {
    REQUIRE(aragorn.get() != nullptr);
  }
}

TEST_CASE("ScopedPtr supports comparison with nullptr") {
  SECTION("operator==") {
    ScopedOathbreakers legolas{};
    REQUIRE(legolas == nullptr);
  }
  SECTION("operator!=") {
    ScopedOathbreakers aragorn{new DeadMen{}};
    REQUIRE(aragorn != nullptr);
  }
}

TEST_CASE("ScopedPtr supports swap") {
  auto message1 = "this is message one";
  auto message2 = "this is message two";
  ScopedOathbreakers smart_ptr1{new DeadMen{message1}};
  ScopedOathbreakers smart_ptr2{new DeadMen{message2}};

  smart_ptr1.swap(smart_ptr2);

  REQUIRE(smart_ptr1->message == message2);
  REQUIRE(smart_ptr2->message == message1);
}

TEST_CASE("ScopedPtr reset") {
  ScopedOathbreakers aragorn{new DeadMen{}};
  SECTION("destructs the owned object") {
    aragorn.reset();
    REQUIRE(DeadMen::oaths == 0);
  }

  SECTION("can replace an owned object") {
    auto message = "this is a message...";
    auto new_dead_men = new DeadMen{message};  // raw pointer
    REQUIRE(DeadMen::oaths == 2);
    aragorn.reset(new_dead_men);
    REQUIRE(DeadMen::oaths == 1);
    REQUIRE(aragorn->message == new_dead_men->message);
    REQUIRE(aragorn.get() == new_dead_men);
  }
}

void by_ref(const ScopedOathbreakers&) {}
void by_value(ScopedOathbreakers) {}

TEST_CASE("ScopedPtr can") {
  ScopedOathbreakers aragorn{new DeadMen};
  SECTION("be passed by reference") { by_ref(aragorn); }
  // SECTION("NOT be copied") {
  //   // DOES NOT COMPILE
  //   by_value(aragorn);
  //   auto son_of_aragorn = aragorn;
  // }
  // SECTION("NOT be moved") {
  //   // DOES NOT COMPILE
  //   by_value(std::move(aragorn));
  //   auto son_of_aragorn = std::move(aragorn);
  // }
}

TEST_CASE("ScopedArray supports operator[]") {
  boost::scoped_array<int> squares{new int[5]{0, 4, 9, 16, 25}};
  squares[0] = 1;
  REQUIRE(squares[0] == 1);
  REQUIRE(squares[1] == 4);
  REQUIRE(squares[2] == 9);
}
