# Smart Pointers

## Smart Pointers

- Make sure each dynamic object gets destructed **exactly once**
- Use RAII to handle dynamic memory management
  - acquire dynamic storage in the constructor
  - release dynamic storage in the destructor
- Use **smart pointers** to achieve that ^
- smart pointers are class templates that behave like pointers and implement RAII for dynamic objects

## Smart Pointer Ownership

- each smart pointer has an **ownership** model

## Scoped Pointers

- a **scoped pointer** expresses _non-transferable_, _exclusive ownership_ over a single dynamic object
- **non-transferable**: the scoped pointers cannot be moved from one scope to another
- **exclusive ownership**: they cannot be copied; no other smart pointers can have ownership of a scoped pointer's dynamic object
- `boost::scoped_ptr`
- in the `scoped_ptr` destructor, it checks whether it owns an object
  - if it does, it deletes the dynamic object
- supports pointer semantics
  - `get()` method: extracts a raw pointer from a `scoped_ptr`
- also implements `operator==` and `operator!=`
  - but are **only** defined when compared with a `nullptr`
- **object swap**
  - switch the dynamic object owned by one `scoped_ptr` with the dynamic object owned by another `scoped_ptr`
  - the `swap()` method
- **RARELY**, you want to destruct an object owned by `scoped_ptr` _before_ the pointer dies
  - e.g. replace its owned object with a new dynamic object
  - use the `reset()` method
- you **can** pass a `scoped_ptr` by reference
- you **CANNOT** pass a `scoped_ptr` by value
- **cannot** use the copy constructor of `scoped_ptr`
- **cannot** use the copy assignment operator
- **cannot** move a `scoped_ptr` with `std::move`
- Generally, `boost::scoped_ptr` incurs no overhead compared with raw pointers
- `boost::scoped_array`
  - scoped pointer for dynamic arrays
  - implements `operator[]`

## Unique Pointers

- **unique pointer** has _transferable_, _exclusive_ ownership over a single dynamic object
- you **can** move unique pointers
- **cannot** be copied
  - because `unique_ptr` has a **deleted** copy constructor
- available as `std::unique_ptr` in the `<memory>` header
- `std::unique_ptr<int> my_ptr{new int{42}};`
- `auto my_ptr = make_unique<int>(42);`
- initialized to be empty
- `unique_ptr` has built-in dynamic array support
  - use the array type as the template parameter
  - `std::unique_ptr<int[]>`
  - **DO NOT** initialize a `std::unique_ptr<T>` with a dynamic array `T[]`
- `std::unique_ptr` has a second, _optional_ template parameter called its **deleter** type
  - it's what gets called when the unique pointer needs to destroy its owned object
  - `std::unique_ptr<T, Deleter=std::default_delete<T>>`
  - the unique pointer will **ignore** deleter's return value

## Shared Pointers

- **shared pointer** has _transferable_ and _non-exclusive_ ownership over a single dynamic object
- you can move them - transferable
- you can copy them - non-exclusive
- non-exclusive
  - a `shared_ptr` checks whether any other `shared_ptr` objects also own the object before destroying it
  - the last owner is the one to release the owned object
- `std::shared_ptr` in `<memory>` - this is **THE ONE** you should use
- `boost::shared_ptr` in `<boost/smart_ptr/shared_ptr.hpp>`
- `std::shared_ptr<int> my_ptr{new int{42}};`
- `auto my_ptr = std::make_shared<int>(42);` - you **SHOULD** generally use this
  - shared pointers require a **control block**, which
    - keeps track of several quantities:
      - number of shared owners
  - when using `make_shared`, the control block and the owned dynamic object can be allocated simultaneously
  - if you first use `opeartor new` and _then_ allocate a shared pointer, you will be making _two_ allocations instead of one
- **allocator**: allocates, creates, destroys, and deallocates objects
  - default is `std::allocator`, a template class in `<memory>`
    - allocates memory from dynamic storage and takes a template parameter
  - both `shared_ptr` and `make_shared` have an allocator type template parameter
    - _three_ template parameters in total:
      - the pointed-to type
      - the deleter type
      - the allocator type
    - but you only ever need the _pointed-to type_ parameter

```c++
// a full-featured example:
std::shared_ptr<int> sh_ptr{
  new int{10},
  [](int* x) {delete x;},
  std::allocator<int>{}
};
```

- you actually **CANNOT** use custom deleter/allocator with `make_shared`
  - if you want, use `std::allocate_shared`
    - it takes an allocator as the first argument and forwards the remainder of the arguments to the owned object's constructor
    - `auto sh_ptr = std::allocate_shared<int>(std::allocator<int>{}, 10);`
- cannot use custom deleter because:
  - `make_shared` uses `new` to allocate space for the owned object and the control block; the appropriate deleter for `new` is `delete`
  - custom deleter **cannot** generally know how to deal with the control block, only with the owned object
- a `shared` array is a shared pointer that owns a dynamic array and supports `operator[]`

## Weak Pointers

- **weak pointer** - a special kind of smart pointer that has **no** ownership over the referred object
- it allows you track an object and convert the weak pointer into a shared pointer _only if_ the tracked object still exists
- movable and copyable
- common usage:
  - **cache**
    - keeps weak pointers to objects so they destruct once all other owners release them
    - periodically, the cache can scan its stored weak pointers and trim those with no other owners
- `std::weak_ptr`
- `boost::weak_ptr`
- constructing:
  - the default constructor constructs empty weak pointer
  - to construct a weak pointer that tracks a dynamic object, it must be constructed by either a shared pointer or another weak pointer

```c++
auto sp = std::make_shared<int>(42);
std::weak_ptr<int> wp{sp};
```

- weak pointers invoke the `lock()` method to get temporary ownership of the tracked object
- `lock` always creates a shared pointer
  - if the tracked object is alive, the returned shared pointer owns the tracked object
  - if it's no longer alive, the returned shared pointer is empty
- `std::enable_shared_from_this`
  - class template that allows instances to create shared pointers referring to themselves
  - `shared_from_this`
  - `weak_from_this`

## Intrusive Pointers

- an **intrusive pointer** is a shared pointer to an object with an embedded reference count
- `boost::intrusive_ptr`

## Allocators

- low-level objects that service requests for memory
- Generally speaking the default `std::allocate` is sufficient
  - it uses `operator new(size_t)`
  - allocates raw memory on the heap
  - deallocates raw memory using `operator delete(void*)`
- To customize an allocator, you need (at least) the following:
  - an appropriate default constructor
  - a `value_type` member corresponding to the template parameter
  - _every_ stdlib container provides `value_type` as the name of its value type
  - a template constructor that can copy an allocator's internal state while dealing with a change in `value_type`
  - an `allocate` method
  - a `deallocate` method
  - `operator==`
  - `operator!=`
