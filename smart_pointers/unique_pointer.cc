#include <catch2/catch_test_macros.hpp>
#include <memory>

struct DeadMen {
  DeadMen(const char* m = "") : message{m} { oaths++; }
  ~DeadMen() { oaths--; }

  const char* message;
  static int oaths;
};

int DeadMen::oaths{};

using UniqueOathbreakers = std::unique_ptr<DeadMen>;

TEST_CASE("UniquePtr can be used in move") {
  auto aragorn = std::make_unique<DeadMen>();
  SECTION("construction") {
    auto son{std::move(aragorn)};  // transfers ownership
    REQUIRE(DeadMen::oaths == 1);
  }
  SECTION("assignment") {
    auto son = std::make_unique<DeadMen>();
    REQUIRE(DeadMen::oaths == 2);

    // transfers ownership
    // destroys the currently owned object
    son = std::move(aragorn);
    REQUIRE(DeadMen::oaths == 1);
    REQUIRE(aragorn.get() == nullptr);
    REQUIRE(son.get() != nullptr);
  }
}

TEST_CASE("UniquePtr to array supports operator[]") {
  std::unique_ptr<int[]> squares{new int[6]{0, 4, 9, 16, 25, 36}};
  squares[0] = 1;
  REQUIRE(squares[0] == 1);
  REQUIRE(squares[1] == 4);
  REQUIRE(squares[2] == 9);
}
