#include <catch2/catch_test_macros.hpp>
#include <memory>

struct DeadMen {
  DeadMen(const char* m = "") : message{m} { oaths++; }
  ~DeadMen() { oaths--; }

  const char* message;
  static int oaths;
};

int DeadMen::oaths{};

TEST_CASE("WeakPtr lock() yields") {
  auto message = "this is a message...";
  SECTION("a shared pointer when tracked object is alive") {
    auto aragorn = std::make_shared<DeadMen>(message);
    std::weak_ptr<DeadMen> wp{aragorn};
    auto sh_ptr = wp.lock();  // obtain temporary ownership here?
    REQUIRE(sh_ptr->message == message);
    REQUIRE(sh_ptr.use_count() == 2);  // number of owners
  }
  SECTION("empty when shared pointer empty") {
    std::weak_ptr<DeadMen> wp;
    {
      auto aragorn = std::make_shared<DeadMen>(message);
      wp = aragorn;  // use the assignment operator
      // now wp tracks the object owned by aragorn
    }
    // aragorn falls out of scope and dies
    auto sh_ptr = wp.lock();  // now wp tracks a dead object
    REQUIRE(nullptr == sh_ptr);
  }
}
