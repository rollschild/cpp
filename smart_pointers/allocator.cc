#include <catch2/catch_test_macros.hpp>
#include <memory>
#include <new>

static size_t n_allocated, n_deallocated;

template <typename T>
struct CustomAllocator {
  using value_type = T;
  CustomAllocator() noexcept {}

  template <typename U>
  CustomAllocator(const CustomAllocator<U>&) noexcept {}

  T* allocate(size_t n) {
    auto p = operator new(sizeof(T) * n);
    ++n_allocated;
    return static_cast<T*>(p);  // cast void* to the relevant pointer type
  }
  void deallocate(T* p, size_t n) {
    operator delete(p);
    ++n_deallocated;
  }
};

template <typename T1, typename T2>
bool operator==(const CustomAllocator<T1>&, const CustomAllocator<T2>&) {
  return true;  // just return true?
}

template <typename T1, typename T2>
bool operator!=(const CustomAllocator<T1>&, const CustomAllocator<T2>&) {
  return false;
}

struct DeadMen {
  DeadMen(const char* m = "") : message{m} { oaths++; }
  ~DeadMen() { oaths--; }

  const char* message;
  static int oaths;
};

int DeadMen::oaths{};

TEST_CASE("Allocator") {
  auto message = "this is a message...";
  CustomAllocator<DeadMen> alloc;
  {
    auto aragorn = std::allocate_shared<DeadMen>(alloc, message);
    REQUIRE(aragorn->message == message);
    REQUIRE(n_allocated == 1);
    REQUIRE(n_deallocated == 0);
  }
  REQUIRE(n_allocated == 1);
  REQUIRE(n_deallocated == 1);
}
