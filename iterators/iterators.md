# Iterators

- **iterator**: an interface to a type that knows how to traverse a particular sequence and exposes simple, pointer-like operations to elements
- Three operations supported by all iterators:
  - `operator*`: access the current element for read/write
  - `operator++`: go to next
  - copy construct

## Iterator Categories

### Output iterators

- only support write and increment but nothing else

#### Insert Iterators

- an output iterator that wraps a container and transforms writes(assignments) into insertions
- Three insert iterators in `<iterator>`
  - `std::back_insert_iterator`
  - `std::front_insert_iterator`
  - `std::insert_iterator`
- Three functions to build insert iterators:
  - `std::back_inserter`
  - `std::front_inserter`
  - `std::inserter`

### Input Iterators

- three operations supported:
  - read
  - increment
  - equality checking
- **cannot** go back

### Forward Iterators

- an input iterator with additional features
- can traverse multiple times
- can default construct
- can copy assign
- can replace input iterators in _all_ places

### Bidirectional Iterators

- a **forward iterator** that can also iterate **backward**
- can replace forward iterators or input iterators in _all_ cases
- Allows backward iteration via
  - `operator--`
  - `operator-(int)`
- Supported by:
  - `array`
  - `list`
  - `deque`
  - `vector`
  - all ordered associative containers

### Random-Access Iterators

- can replace bidirectional, forward, and input iterators in _all_ cases
- allows random access via `operator[]`
- supported by:
  - `array`
  - `vector`
  - `deque`

### Contiguous Iterators

- a random-access iterator with elements adjacent in memory
- supported by `vector` and `array`
- **NOT** supported by `list` or `deque`

## Auxiliary Iterator Functions

- You should use **auxiliary iterator functions** from the `<iterator>` header to manipulate iterators if you are writing generic code

### `std::advance`

- allows you to increment or decrement by the desired amount
- accepts an iterator reference and an integer value corresponding to the distance
- `void std::advance(InputIterator& itr, Distance d);`
- does **NOT** perform bounds checking

### `std::distance`

- computes the distance between two input iterators
- `Distance std::distance(InputIterator itr1, InputIterator itr2);`
- Make sure `itr2` comes _after_ `itr1`

### `std::iter_swap`

- swap the values pointed to by two forward iterators
- `Distance std::iter_swap(ForwardIterator itr1, ForwardIterator itr2);`

## Aditional Iterator Adapters

### Move Iterator Adapters

- `std::make_move_iterator` in `<iterator>`

### Reverse Iterator Adapters

- almost all containers expose reverse iterators with:
  - `rbegin`
  - `rend`
  - `crbegin`
  - `crend`
- you can convert a normal iterator into a reverse iterator manually
  - `std::make_reverse_iterator`
-
