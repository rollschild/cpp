# Structures, Unions, and Enumerations

- `enum` - a type with a set of _named constants_
- `enum class` - scoped enumeration
  - an `enum` where the enumerators are within the scope of the enum and _no implicit conversions_ to other types are provided

## `struct`

- Objects of structure types can be (re)assigned (if not `const` I'd assume?)
- `==` and `!=` are _NOT_ available _by default_
- name of a type becomes available for use _immediately_ after it has been encountered
  - however it's _NOT_ possible to declare new objects of `struct` until its complete declaration has been seen
- Name of a `struct` _can_ be used before the type is defined as long as that use does not require:
  - the name of a member, or
  - the size of the structure to be known
- It's _possible_ to declare a `struct` and a non-`struct` with the same name in the same scope
- `struct` is just a `class` where members are `public` by default
- you do _NOT_ need a constructor simply to initialize members in order
- \***\*Plain Old Data (POD)\*\***
  - `is_pod<T>::value` - an stdlib type property predicate, in `<type_traits>`
- \***\*Field\*\***
  - `char` is the smallest object that can be independently allocated and addressed
  - possible to bundle several tiny variables together as \***\*fields\*\*** in a `struct`
  - \***\*bit-field\*\***
  - by specifying the number of bits the member occupies
  - unnamed fields are allowed - help with the layout - better alignment
  - a field must be of integral or enumeration type
  - using fields to pack several variables into a single byte does _NOT_ necessarily save space!
    - in fact, it's typically _much faster_ to access a `char`/`int` than to access a field
    - a bit-field with size zero means _Start in a new allocation unit_
      - the following field starts at a word boundary

## `union`

- an _anonymous_ `union` is an object, _not_ a type

## `enum`

- an enumeration is represented by some integer type and each enumerator by some integer value
  - the type used to represent an enumeration - \***\*underlying type\*\***
    - must be one of the _signed_/_unsigned_ integer types
    - default is `int`

```c++
enum class Warning:int {green, yellow, orange, red}; // sizeof(Warning) == sizeof(int)
enum class Warning:char {green, yellow, orange, red}; // sizeof(Warning) == 1
static_cast<int>(Warning::green) == 0
```

- use `static_cast` to cast to underlying types
- plain enums will cause **namespace pollution**
- plain `enum` cannot be declared if its underlying type is not specified:
  - `enum Color:char;`
-
