# Basic Setup of C++ Dev Environment

- the **Compiler Tool Chain**
  - **preprocessor**
    - basic source code manipulation
    - produces a single translation unit
  - **compiler**
    - reads a translation unit and generates an **object file**
  - **linker**
    - generates programs from object files
    - finds the libs included in the source code
- **Separate Compilation**
  - declarations and definitions are separate and compiled separately
