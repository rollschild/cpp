#include <cstdio>

int step_function(int x) {
  int result = 0;
  if (x < 0) {
    result = -1;
  } else if (x > 0) {
    result = 1;
  }
  return result;
}

int main() {
  int num1 = 42;
  int res1 = step_function(num1);

  int num2 = 0;
  int res2 = step_function(num2);

  int num3 = -32767;
  int res3 = step_function(num3);

  printf("num1: %d, step: %d\n", num1, res1);
  printf("num2: %d, step: %d\n", num2, res2);
  printf("num3: %d, step: %d\n", num3, res3);
  // return here in `main` is optional
  return 0;
}
