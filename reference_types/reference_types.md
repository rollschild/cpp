# Reference Types

- Reference types store memory addresses of object
- Two fundametal types:
  - pointer
  - reference

## Pointers

- pointing & dereferencing
- `&`: address-of operator
- `*`: dereference operator
- `&variable` could be differen each time you run the program
  - **address space layout randomization**
  - security feature
  - to combat **return-oriented programs**
  - how many hex digits you will get depends on the arch of CPU
    - x86: 32-bit general purpose registers
    - x64: 64-bit registers
- `->`: member of pointer operator
  - Two _simultaneous_ operations:
    - dereferences a pointer
    - accesses a member of the pointed-to object
- Handling **decay**
  - often you pass arrays as two args:
    - pionter to the first array element
    - array's length
- Pointer Arithmetic
- you **CANNOT** dereference a `void*`
- use `std::byte` pointer
- `nullptr` is a special literal value of a pointer
  - doesn't point to anything
  - no memory left to allocate
- pointers have an implicit conversion to `bool`
  - any pointer that is **not** `nullptr` converts implicitly to `true`
  - `nullptr` converts to `false`
- For a function, return `nullptr` in the case of failure
  - memory allocation situations

## References

- safer & more convenient version of pointers
  - **CANNOT** be assigned to null
  - **CANNOT** be _reseated_/reassigned

## Pointers vs. References

- If you must change what the reference type refers to
  - Use a pointer

## Forward-Linked Lists

- elements can be discontinuous in memory
- last element holds a `nullptr`
- `this` pointer: access the current object

## const

- specifies that a variable (usually a reference/pointer) will **NOT** be modified
- Usecases
  - make an argument `const`
    - an efficient mechanism to pass an object into a function for **read-only** use
  - `const` methods
    - promise not to modify the current object's state within the `const` method
    - read-only methods
    - holders of `const` references **CANNOT** invoke methods that are not `const`
  - `const` member variables
    - the `const` member means that the pointed-to value cannot be modified

## Member Initializer Lists

- member initializers allow you to set the value of `const` fields at **runtime**
- is the primary mechanism to initialize class members
- **All** member initializations execute **before** the constructor's body
- **Order member initializers in the same order as they appear in the class definition, because their constructors will be called in this order**

## auto

- initialization with `auto`
- general rule, use `auto` always
-
