#include <cstdio>

struct Clock {
  Clock(int new_year) {
    if (!set_year(new_year)) {
      // Exceptions are better suited for this
      // On this topic later
      year = 2022;
    }
  }
  void add_year() { year++; }
  bool set_year(int new_year) {
    if (new_year < 2022) return false;
    year = new_year;
    return true;
  }
  int get_year() const { return year; }

 private:
  int year;
};
struct Compound {
  Compound(const char* name, int year) : name{name}, clock{year} {}
  void announce() const {
    printf("Name is %s and year is %d.\n", this->name, clock.get_year());
  }
  const char* name;
  Clock clock;
};

void member_initializer_lists() {
  Compound c1{"Compound 1", 2022};
  Compound c2{"Compound 2", 2023};
  c1.announce();
  c2.announce();
}

void basic_pointer() {
  int what{};
  int* what_ptr = &what;
  printf("what: %d\n", what);
  printf("&what: %p\n", what_ptr);
  printf("pointed to by what_ptr: %d\n", *what_ptr);
}
void class_pointers() {
  Clock clock{2018};
  Clock* clock_ptr = &clock;
  printf("default year: %d\n", clock_ptr->get_year());
  clock_ptr->set_year(2024);
  printf("address of clock: %p\n", clock_ptr);

  // below two are the same
  printf("new year: %d\n", clock_ptr->get_year());
  printf("new year: %d\n", (*clock_ptr).get_year());
}

struct College {
  char name[256];
};
void print_name(College* college_ptr) {
  printf("%s College\n", college_ptr->name);
}
void print_names(College* colleges, size_t n_colleges) {
  for (size_t i = 0; i < n_colleges; i++) {
    printf("%s College\n", colleges[i].name);
  }
}
void array_pointers() {
  College best_colleges[] = {{"UMD"}, {"Georgia Tech"}};
  print_name(best_colleges);
  print_names(best_colleges, sizeof(best_colleges) / sizeof(College));
}
struct Pointer {
  Pointer* ptr{};
};

struct ListNode {
  ListNode* next{};  // nullptr initially
  void insert_after(ListNode* new_node) {
    new_node->next = next;
    next = new_node;
  }
  char prefix[2]{};
  int operating_number{};
};
void singly_linked_list() {
  ListNode node1, node2, node3;
  node1.prefix[0] = 'A';
  node1.prefix[1] = 'B';
  node1.operating_number = 123;
  node1.insert_after(&node2);

  node2.prefix[0] = 'B';
  node2.prefix[1] = 'C';
  node2.operating_number = 234;
  node2.insert_after(&node3);

  node3.prefix[0] = 'C';
  node3.prefix[1] = 'D';
  node3.operating_number = 345;

  for (ListNode* cursor = &node1; cursor; cursor = cursor->next) {
    printf("node: %c%c-%d\n", cursor->prefix[0], cursor->prefix[1],
           cursor->operating_number);
  }
}

int main() {
  array_pointers();
  char str[] =
      "abcde";  // length is 6 because of the null terminator at the end
  printf("length of str is %zu\n", sizeof(str) / sizeof(char));

  Pointer pointer;
  printf("is nullptr %d\n", pointer.ptr == nullptr);

  singly_linked_list();

  member_initializer_lists();
}
