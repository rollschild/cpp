#include <cstdint>
#include <cstdio>

/* template <typename Fn>
 * void transform(Fn fn, const int* in, int* out, size_t length) {
 *   for (size_t i{}; i < length; i++) {
 *     out[i] = fn(in[i]);
 *   }
 * }
 *
 * void lambda_naive() {
 *   const size_t len{4};
 *   int base[]{1, 2, 3, 4}, a[len], b[len], c[len];
 *   transform([](int x) { return 1; }, base, a, len);
 *   transform([](int x, int y = 1) { return x + y; }, base, b, len);
 *   transform([](int x) { return x * x; }, base, c, len);
 *
 *   for (size_t i{}; i < len; i++) {
 *     printf("Element %zu: %d %d %d\n", i, a[i], b[i], c[i]);
 *   }
 *
 * } */

template <typename Fn, typename T>
void transform(Fn fn, const T* in, T* out, size_t len) {
  for (size_t i{}; i < len; i++) {
    out[i] = fn(in[i]);
  }
}
void lambda_generic() {
  constexpr size_t len{4};
  int base_int[]{1, 2, 3, 4}, a[len];
  float base_float[]{10.f, 20.f, 30.f, 40.f}, b[len];
  auto translate = [](auto x) { return x * x; };
  transform(translate, base_int, a, len);
  transform(translate, base_float, b, len);

  for (size_t i{}; i < len; i++) {
    printf("Element %zu: %d %f\n", i, a[i], b[i]);
  }
}

int main() { lambda_generic(); }
