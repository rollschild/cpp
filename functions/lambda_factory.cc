#include <cstdint>
#include <cstdio>

struct LambdaFactory {
  LambdaFactory(char in) : to_count{in}, total{} {}
  auto make_lambda() {
    return [this](const char* str) {
      size_t index{}, result{};
      while (str[index]) {
        if (str[index] == to_count) result++;
        index++;
      }
      total += result;
      return result;
    };
  }

  const char to_count;
  size_t total;
};

int main() {
  LambdaFactory factory{'s'};
  auto lambda = factory.make_lambda();
  printf("total: %zu\n", factory.total);
  auto sally = lambda("Sally sells seashells by the seashore.");
  printf("Sally: %zu\n", sally);
  printf("total: %zu\n", factory.total);
  auto sailor = lambda("Sailor went to sea to see what he could see.");
  printf("Sailor: %zu\n", sailor);
  printf("total: %zu\n", factory.total);
}
