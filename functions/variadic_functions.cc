#include <cstdarg>
#include <cstdint>
#include <cstdio>

/* int sum(size_t n, ...) {
 *   va_list args;
 *   va_start(args, n);
 *   int result{};
 *   while (n--) {
 *     auto next_element = va_arg(args, int);
 *     result += next_element;
 *   }
 *   va_end(args);
 *   return result;
 * } */

/* // compile-time computation
 * template <typename T>
 * constexpr T sum(T x) {
 *   return x;
 * }
 *
 * template <typename T, typename... Args>
 * constexpr T sum(T x, Args... args) {
 *   return x + sum(args...);
 * } */

// fold expression
template <typename... T>
constexpr auto sum(T... args) {
  return (... + args);
}

int main() { printf("answer is %d\n", sum(7, 12, 45, -1, 2, 100, 9, 11)); }
