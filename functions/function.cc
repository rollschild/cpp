#include <cstdio>
#include <functional>

void empty_function() {
  std::function<void()> func;  // this `func` has no callable object
  try {
    func();
  } catch (const std::bad_function_call& e) {
    printf("Exception: %s\n", e.what());
  }
}

void static_func() { printf("This is a static function.\n"); }

int main() {
  empty_function();

  // template parameter indicates that a callable object contained
  // by `func` takes no arguments and returns `void`
  std::function<void()> func{[] { printf("A lambda...\n"); }};
  func();
  func = static_func;
  func();
}
