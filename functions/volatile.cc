#include <cstdio>

struct Distillate {
  int appy() volatile { return ++applications; }

 private:
  int applications{};
};

int main() {
  volatile Distillate ethanol;
  printf("%d\n", ethanol.appy());
  printf("%d\n", ethanol.appy());
  printf("%d\n", ethanol.appy());
  printf("%d\n", ethanol.appy());
  const int num = ethanol.appy();
  printf("num: %d\n", num);
}
