#include <cstdint>
#include <cstdio>

// CountIf is the **partial application** of `x` to `count_if`
struct CountIf {
  CountIf(char x) : x{x} {}
  size_t operator()(const char* str) const {
    size_t index{}, result{};
    while (str[index]) {
      if (str[index] == x) result++;
      index++;
    }
    return result;
  }

 private:
  const char x;
};

size_t count_if(char x, const char* str) {
  size_t index{};
  size_t result{};
  while (str[index]) {
    if (str[index] == x) result++;
    index++;
  }
  return result;
}

int main() {
  CountIf s_counter{'s'};
  auto sally = s_counter("Sally sells seashells by the seashore.");
  printf("Sally: %zu\n", sally);
  auto sailor = s_counter("Sailor went to sea to see what he could see.");
  printf("Sailor: %zu\n", sailor);

  auto buffalo = CountIf{'f'}(
      "Buffalo buffalo Buffalo buffalo "
      "buffalo buffalo Buffalo buffalo.");
  printf("Buffalo: %zu\n", buffalo);
}
