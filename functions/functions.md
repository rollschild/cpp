# Functions

## Function Declarations

- `prefix-modifiers return-type func-name(arguments) suffix-modifiers;`
- modifiers alter a function's behavior in some way

### Prefix Modifiers

- `static`
  - if function is not a member of a class
    - it has internal linkage - it will **not** be used outside of this translation unit
  - if a method (member of a class)
    - it is not associated with any instantiation of the class but with the class itself
- `virtual`
  - the method can be overriden
  - use `override` to actually override
- `constexpr`
  - function should be evaluated at compile time, if possible
- `[[noreturn]]`
  - function will not return
  - helps compiler to optimize the code
- Instructions compiled from a function call:
  1. place arguments into registers and on the call stack
  2. push return address onto call stack
  3. jump to the called function
  4. jump to the return address after the called function completes
  5. cleanup the callstack
- `inline`
  - _inlining a function_
    - copy and paste the contents of the function directly into the execution path
    - eliminate the 5 steps above

### Suffix Modifiers

- `noexcept`
  - _never_ throw an exception
  - for optimization
- `const`
  - the method does **not** modify any instance of its class
  - allows `const` references types to invoke the method

#### `final` and `override`

- `final`
  - a method cannot be overriden by a child class
  - the opposite of `virtual`
  - can also apply `final` to an entire class
    - disallows the class from being a parent (inheritable) entirely
- `final` and `override` etc are just **modifiers**, not keywords
  - their meaning depends on context
- whenever using interface inheritance,
  - mark implementing classes `final`
  - can encourage the compiler to perform the **devirtualization** optimization
    - when virtual calls are devirtualized, the compiler eliminates the runtime overhead associated with a virtual call
- you **CANNOT** invoke a non-`const` method on a `const` object
- `volatile`
  - a method can be invoked on `volatile` objects
- `const` and `volatile`:
  - **const/volatile qualification**
- `volatile` objects require all memory accesses are treated as observable side effects

## `auto` Return Types

- use the `auto` function return type carefully!
  - because function definitions are documentation
  - it's best to provide concrete return types when available

## `auto` and Function Templates

- the primary usecase for `auto` type deduction is with function templates
- you can append an expression that evaluates to the function's return type

```c++
auto my-function(arg1-type arg1, arg2-type arg2, ...) -> type-expression {
  // ...
}
```

- it's commonly paired with `decltype`
- `decltype` - yields another expression's resultant type
  - `decltype(expression)`
  - outside of the world of generic programming with templates, `decltype` is rare

## Overload Resolution

- the process that compiler executes when matching a function invocation with its proper implementation

## Variadic Functions

- `printf`
- by placing `...` as the final parameter in the argument list
- access individual arguments using the util functions in the `<cstdarg>` header
  - `va_list`
  - `va_start`
  - `va_end`
  - `va_arg`
  - `va_copy`
- **NOT SAFE**
- better to use variadic templates

## Variadic Templates

```c++
// template parameter pack
template <typename... Args>
// Args is a **function parameter pack**
return-type func-name(Args... args) {
  // ...
}
```

- special operations on **function parameter pack**
  - `sizeof...(args)` - obtain the parameter pack's size
  - expand/unpack the parameter pack: `other_function(args...)`

### Programming with Parameter Packs

- It's not possible to index into a parameter pack directly
- You should invoke the function template form within itself
  - **compile-time recursion**

```c++
template <typename T, typename... Args>
void my_func(T x, Args... args) {
  // use `x`, then recurse with the rest
  my_func(args...);
}

// indicates when to stop the recursion
template <typename T>
void my_func(T x) {
  // use `x` but do NOT recurse
}
```

### Fold Expressions

- **fold expression** computes the result of using a binary operator over all arguments of a parameter pack
- `(... binary-operator parameter-pack)`

## Function Pointers

- **functional programming**
  - function evaluation
  - immutable data
  - _pass a function as a parameter to another function_
    - pass a function pointer
    - but you **CANNOT** modify the pointed-to function

### Declare a Function Pointer

- `return-type (*pointer-name)(args-type1, arg-type2, ...);`
- you **can** reassign function pointers

### Type Aliases

- `using alias-name = return-type(*)(arg-type1, arg-type2, ...);`

## Function-Call Operator

- make user-defined types callable/invocable by overloading the function-call operator `operator()`
  - allows any combination of argument types, return types, and modifiers (except `static`)
- such a type is **function type**
- instance of a function type is **function object**

```c++
struct type-name {
  return-type operator()(arg-type1 arg1, arg-type2 arg2, ...) {
    // ...
  }
}
```

- when compiler evaluates a function-call expression, it invokes the function-call operator on the first operand, passing the remaining operands as arguments
- can employ function objects as partial applications

## Lambda Expressions

- **lambda expressions** construct _unnamed function objects_ succinctly

### Usage

- Five components
  - **captures**
    - the _partially applied_ parameters
  - **parameters**
  - **body**
  - **specifiers**
  - **return type**
- `[captures] (parameters) modifiers -> return-type {body}`
- only **captures** and **body** are required
- lambda expressions produce function objects

### Generic Lambdas

- lambda expression templates
- specify `auto` for one or more parameters
- normally the compiler deduces a lambda's return type for you
- use `->` to take over
  - `[](int x, double y) -> double {return x + y;}`
  - `[](auto x, double y) -> decltype(x + y) {return x + y;}`

### Lambda Captures

- lambda can capture by reference or by value
  - by valube by default

#### Default Capture

- **default capture** vs. **named capture**
- default capture: capture all automatic variables used within a lambda
- by value: `[=]` - capture _all_ local names by value
- by reference: `[&]` - captures _all_ local names by reference
- can also mix a default capture with a named capture

```c++
auto s_counter = [&, to_count](const char* str) {
  // ...
}
```

- initializer expressions in capture lists
  - `[&by_ref, my_new_var=by_value]`
- a.k.a. **init capture**

#### Capture `this`

- by **value**: `[*this]`
- by **reference**: `[this]`

### `constexpr` Lambda Expressions

- _all_ lambda expressions are `constexpr` as long as it can be invoked at compile time
- `[](int x) constexpr {return x * x;}`
- you should mark a lambda `constexpr` if you want to make sure it meets all `constexpr` requirements
  - no dynamic membory allocations
  - no calling non-`constexpr` functions

## `std::function`

- from the `<functional>` header
- a polymorphic wrapper around a callable object
- it's a generic function pointer
- you can store any of the following into a `std::function`:
  - static function
  - function object
  - lambda

### Declaring a Function

- `std::function<return-type(arg-type1, arg-type2, etc.)>`
- you can construct a `function` with callable objects, as long as that object supports the function semantics implied by the template parameter of `function`
- using a `function` can incur runtime overhead
  - `function` might need to make a dynamic allocation to store the callable object
  - compiler also has difficulty optimizing away `function` invocations
    - you will incur _indirect_ function call
    - indirect function calls require additional pointer dereferences

## `main` and the Command Line

- the `main` function: the program's entry point
- **command line parameters**: environment-provided arguments

### The Three `main` Overloads

```c++
int main();
int main(int argc, char* argv[]);
int main(int argc, char* argv[], impl-parameters); // not common
```

- `argc`: non-negative number - number of elements in `argv`
  - calulated automatically
- `argv`: array of pointers to num-terminated strings
- Usually, OS passes the full path to the program's executable as the first command line argument
-
