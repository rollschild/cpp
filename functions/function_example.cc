#include <cstdint>
#include <cstdio>
#include <functional>

// CountIf is the **partial application** of `x` to `count_if`
struct CountIf {
  CountIf(char x) : x{x} {}
  size_t operator()(const char* str) const {
    size_t index{}, result{};
    while (str[index]) {
      if (str[index] == x) result++;
      index++;
    }
    return result;
  }

 private:
  const char x;
};

size_t count_spaces(const char* str) {
  size_t index{}, result{};
  while (str[index]) {
    if (str[index] == ' ') result++;
    index++;
  }
  return result;
}

// WTF is this
// declare a `std::function` array with static storage duration
std::function<size_t(const char*)> funcs[]{
    count_spaces,  // a static function pointer
    CountIf{'e'},  // a function object
    [](const char* str) {
      size_t index{};
      while (str[index]) index++;
      return index;
    },  // a lambda
};

auto text = "Sailor went to sea to see what he could see.";

int main() {
  size_t index{};
  for (const auto& func : funcs) {
    printf("func #%zu: %zu\n", index++, func(text));
  }
}
