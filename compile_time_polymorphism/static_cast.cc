#include <cstdio>

short increment_as_short(void* target) {
  auto as_short = static_cast<short*>(target);
  *as_short += 1;
  return *as_short;
}

int main() {
  short num{655};
  auto increased = increment_as_short(&num);
  printf("%d is the increased number.\n", increased);
}
