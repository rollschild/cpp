# Compile Time Polymorphism

- C++ achieves compile-time polymorphism through **templates**
- **Template instantiation** is the process of creating a class or function from a template
  - also the _result_ of the template instantiation process
    - **concrete class** or **concrete type**

## Declaring Templates

- `template<typename T>`
  - can use both `typename` and `class`, they mean the same thing

```c++
template<typename X, typename Y, typename Z>
struct MyTemplateClass {
  X foo(Y&);
private:
  Z* member;
};
```

```c++
template<typename X, typename Y, typename Z>
X my_template_function(Y& arg1, const Z* arg2) {
  // ...
}
```

- Instantiating templates
  - `template_class_name<t_arg1, t_arg2, ...> my_concrete_class{ ... }`
  - `auto result = template_function_name<t_arg1, t_arg2, ...>(f_arg1, f_arg2, ...);`

## Named Conversion Functions

- `named-conversion<desired-type>(object-to-cast)`

### const_cast

- cast the `const` away

```c++
void carbon_thaw(const int& encased_colo) {
  // encased_colo++ // this will not work
  auto& hibernation_sick_solo = const_cast<int&>(encased_colo);
  hibernation_sick_solo++; // this works
}
```

### static_cast

- reverse a well-defined implicit conversion
- is needed because generally implicit casts are **not** reversible

### reinterpret_cast

- `reinterpret_cast` gives you control over how to interpret memory
- **Converts between types by reinterpreting the underlying bit pattern**
- `auto result = reinterpret_cast<desired-pointer-type>(memory-address-the-result-should-point-to)`

### narrow_cast

- **narrowing**
- loss in information

- **Template Type Deduction**
  - generally, you can omit the template parameters when invoking a template function
  - _mostly_ works
  - sometimes, template arguments cannot be deduced
    - if a template function's return type is a template argument that is entirely independent of other function and template arguments

## SimpleUniquePointer

- `std::unique_ptr`
  - member of the family of RAII templates called smart pointers

## Type Checking in Templates

- Templates are type safe
- The type checking happens very late in the compilation process
- C++ templates share similarities with **duck-typed languages**
- **Duck-typed language**
  - defer type checking until _runtime_

## Concepts

- **Concepts** constrain template parameters, allowing for parameter checking at the point of instantiation rather than the point of first use
- a concept is a template
- it's a constant expression involving template arguments, evaluated at compile time
- Think of a concept as a big **predicate**
  - a function that evaulates to `true` or `false`

### Defining a Concept

```c++
template<typename T1, typename T2, ...>
concept bool ConceptName() {
  // ...
}
```

### Type Traits

- utilities for inspecting type properties
  - **type traits**
- each type trait is a template class that takes a single template parameter, the type you want to inspect

```c++
std::is_integral<float>::value // false
```

### Requirements

- Type traits tell you _what_ types are
- **Requirements** tell you _how_ the template will use them
- ad hoc constraints on template parameters

```c++
requires (arg-1, arg-2, ...) {
  {expression1} -> return-type1;
  {expression2} -> return-type2;
  // ...
}
```

- example:

```c++
requires (T t, U u) {
  {t == u} -> bool;
  {u == t} -> bool;
  {t != u} -> bool;
  {u != t} -> bool;
}
```

### Using Concepts

- Predefined concepts:
  - the [origin](https://github.com/asutton/origin) library
- concepts are fairly heavyweight mechanisms for enforcing type safety
- you can embed requires expressions directly intot the template definition

### static_assert

- temporary stopgap for concepts in pre-C++20 era
- evaluates at compile time
- `static_assert(boolean-expression, optional-message);`
- the expression must be constant expression
- can embed one or more `static_assert` expressions in the bodies of templates
- `std::is_arithmetic`: `+`, `-`, `*`, `/`

## Non-Type Template Parameters

- **type template parameter**
  - for yet-to-be-specified types
  - `T`
- **non-type template parameters**
  - for yet-to-be-specified values
  - can be any of the following:
    - integral type
    - lvalue reference type
    - pointer (or pointer-to-member) type
    - `std::nullptr_t` - (type of `nullptr`)
    - `enum class`

## Variadic Templates

- takes in an unknown number of arguments
- compiler knows these arguments at template instantiation time
- Syntax
  - the final template parameter: `typename... arguments`
  - `...`: `arguments` is a **parameter pack type**
- **parameter pack**
  - a template argument that accepts zero or more function arguments

## Advanced Usages

- **Type Function**
  - takes types as arguments and returns a type
  - **metagrogramming** or **template metaprogramming**

## Template Source Code Organization

- Implement templates entirely within header files
