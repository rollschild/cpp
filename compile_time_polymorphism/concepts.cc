#include <concepts>
#include <cstdio>
#include <stdexcept>
#include <type_traits>

template <typename T>
concept Averageable = std::is_default_constructible<T>::value &&
    requires(T a, T b) {
  { a + b } -> std::same_as<T>;
  {
    a / size_t { 1 }
    } -> std::convertible_to<T>;
};

template <Averageable T>
T mean(const T* values, size_t length) {
  T result{};  // T must be default constructible
  for (size_t i{}; i < length; i++) {
    result += values[i];  // T supports operator `+=`
  }
  return result / length;  // dividing T by size_t also yields a T
}

void concepts_with_requires() {
  const double nums_d[]{1.0f, 2.0f, 3.0f, 4.0f};
  const auto result1 = mean(nums_d, 4);
  printf("double: %f\n", result1);

  const float nums_f[]{1.0, 2.0, 3.0, 4.0};
  const auto result2 = mean(nums_f, 4);
  printf("float: %f\n", result2);

  const size_t nums_c[]{1, 2, 3, 4};
  const auto result3 = mean(nums_c, 4);
  printf("size_t: %zu\n", result3);
}

template <typename T>
// ensures T is copyable
requires std::is_copy_constructible<T>::value T get_copy(T* pointer) {
  if (!pointer) throw std::runtime_error{"Null-pointer dereference!"};
  return *pointer;
}

void ad_hoc_requires() {
  struct Highlander {
    Highlander() = default;
    // its copy constructor is deleted
    Highlander(const Highlander&) = delete;
  };
  Highlander connor;
  auto connor_ptr = &connor;
  // auto connor_copy = get_copy(connor_ptr);
}

template <typename T>
T mean_v2(T* values, size_t length) {
  static_assert(std::is_default_constructible<T>(),
                "Type must be default constructible!");
  static_assert(std::is_copy_constructible<T>(),
                "Type must be copy constructible!");
  static_assert(std::is_arithmetic<T>(),
                "Type must support addition and division!");
  static_assert(std::is_constructible<T, size_t>(),
                "Type must be constructible from size_t!");
}

// accepts a reference to an `int` array of 10
int& get_naive(int (&arr)[10], size_t index) {
  if (index >= 10) throw std::out_of_range{"Out of bounds!"};
  return arr[index];
}

template <typename T, size_t Length>
T& get(T (&arr)[Length], size_t index) {
  if (index >= Length) throw std::out_of_range{"Out of bounds!"};
  return arr[index];
}
// compile time bounds checking
// by taking size_t Index as another non-type template parameter
template <size_t Index, typename T, size_t Length>
T& get_v2(T (&arr)[Length]) {
  static_assert(Index < Length, "Out-of-bounds access!");
  return arr[Index];
}

void check_bounds() {
  int fib[]{1, 1, 2, 0};
  printf("%d, %d, %d\n", get_v2<0>(fib), get_v2<1>(fib), get_v2<2>(fib));

  get_v2<3>(fib) = get_v2<1>(fib) + get_v2<2>(fib);
  printf("%d\n", get_v2<3>(fib));
}

/* template <typename T, typename... Arguments>
 * SimpleUniquePointer<T> make_simple_unique(Arguments... arguments) {
 *   return SimpleUniquePointer<T>{new T{arguments...}};
 * } */

int main() { check_bounds(); }
