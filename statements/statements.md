# Statements

- A statement does _NOT_ have a value
- A semicolon is by itself a statement, the **empty statement**
- assignments and function calls are expressions

## Compound Statements

- a.k.a. **blocks**
- each block declares a new scope, the **block scope**
- objects with automatic storage duration declared within a block scope have lifetimes bound by the block

## Declaration Statements

- **declaration**
- `static_assert` is a declaration

### Functions

- **function declaration**
  - a.k.a. **signature** or **prototype**
  - specifies function's inputs and outputs
  - `void func_name(uint32_t&);`
- **non-member functions** or **free functions**
  - always declared outside of `main()`
- **function definition**
  - includes the function declaration as well as function body

### Namespaces

- prevents naming conflicts
- by default all symbols declared go to the **global namespace**
- use a **namespace block** to place symbols within a non-global namespace

```c++
namespace Block {
  // symbols
}
```

- namespaces can be nested in two ways

```c++
// 1.
namespace Outer {
  namespace Inner {

  }
}

// 2.
namespace Outer::Inner {

}
```

- use scope resolution
- Using Directives
  - `using`
    - imports a symbol into a block/namespace
  - `using namespace` - import/introduce _all_ symbols from a given namespace into the global namespace
- Usually it's a bad idea to have too many `using namespace` in a single translation unit
- **NEVER** put `using namespace` within a header file
  - every source file that includes the header file will dump all the symbols from that `using` into the _global namespace_

### Type Aliasing

- type aliases **cannot** change the meaning of an existing type name
- `using type-alias = type-id;`
- type aliases can appear in any scope:
  - block
  - class
  - namespace
- can also use type aliases on templates
  - can perform **partial application** - fixing some number of arguments to a template, producing another template with fewer template parameters
  - can define a type alias for a template with a fully specified set of template parameters

### Structured Bindings

- Unpack objects into their constituent elements
- Any type whose **non-static** data members are **public** can be unpacked
- `auto [object-1, object-2, ...] = plain-old-data;`

### Attributes

- Attributes apply implementation-defined features to an expression statement
- `[[ ]]`
- some standard attributes:
  - `[[noreturn]]`
  - `[[deprecated("reason")]]`
  - `[[fallthrough]]`
    - a `switch` case intends to fall through
    - avoid compiler errors
  - `[[nodiscard]]`
  - `[[maybe_unused]]`
  - `[[carries_dependency]]`
- attributes convey useful information to the compiler

## Selection Statements

- `if` and `switch`

### `if`

- Bind an object's scope to an `if` statement by adding an `init-statement` to `if` and `else if` declarations

```c++
if (init-statement; condition-1) {
  // only if condition-1 is true
} else if (init-statement; condition-2) {
  // only if condition-2 is true
}
```

- can make an `if` statement `constexpr`
  - a.k.a. `constexpr if` statements
  - evaluated at compile time
  - code blocks that correspond to `true` conditions get emitted; others ignored
- apparently you can mix them together?

```c++
if constexpr (condition-1) {
  // compile only if condition-1 is true
} else if constexpr (condition-2) {
  // compile only if condition-2 is true
}
// ...
} else {
  // compile only if none of the conditions is true
}
```

- It's usually a _good_ practice to declare a variable in a condition:
  - `if (double d = fn(args))`

### `switch`

```c++
switch (init-expression; condition) {
  case (case-a): {
    // handle case-a
  } break;
  case (case-b): {
    // handle case-b
  } break;
  default: {
    // handle default
  }
}
```

- the expression in the `case` labels must be a constant expression of _integral_ or _enumeration_ type
- a value _may not_ be used more than once for `case`-labels

## Iteration Statements

- Four kinds
  - `while`
  - `do-while`
  - `for`
  - range-based `for`
- range-based for loops:
  - the `rage-declaration` declares a name variable

```c++
for (range-declaration : range-expression) {
  // executes
}
```

- Range Expressions
  - can define your own types that are valid range expressions
  - every range exposes a `begin` and `end` method
    - both methods return **iterators**
    - an **iterator** is an object that supports
      - `operator!=`
      - `operator++`
      - `operator*` - dereference
- for built-in array `T v[N]`, compiler uses `v` and `v+N` as `begin(v)` and `end(v)`

```c++
const auto e = range.end();
for (auto b = range.begin(); b != e; ++b) {
  const auto& element = *b;
}
```

## Jump Statements

- `break`
- `continue`
- `goto`
  - its target is a label
  - **label**s are identifiers you can add to _any_ statement
    - they just give statements a name
  - **DO NOT use `goto`**
- they transfer control flow

## Comments

- A good comment states what a piece of code is supposed to do (the intent)
- Code (only) states what it does and how it does it
-
