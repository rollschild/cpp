#include <cstdio>

namespace Outer::Inner {
enum class Color {
  Mauve,
  Pink,
  Russet,
};

struct Thing {
  const char* name;
  Color shade;
};

bool is_more_mauvey(const Thing& thing) { return thing.shade == Color::Mauve; }
}  // namespace Outer::Inner

using String = const char[256];
using NamespacedColor = Outer::Inner::Color;

int main() {
  const auto my_color{NamespacedColor::Mauve};
  String saying{
      "This is my saying"
      "This is another saying"};
  if (my_color == NamespacedColor::Mauve) {
    printf("saying: %s\n", saying);
  }
}
