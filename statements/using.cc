#include <cstdio>

namespace Outer::Inner {
enum class Color {
  Mauve,
  Pink,
  Russet,
};

struct Thing {
  const char* name;
  Color shade;
};

bool is_more_mauvey(const Thing& thing) { return thing.shade == Color::Mauve; }
}  // namespace Outer::Inner

using namespace Outer::Inner;

int main() {
  const Thing your_thing{"My thing", Color::Mauve};
  if (is_more_mauvey(your_thing)) {
    printf("This, %s, is your thing: %d!\n", your_thing.name, your_thing.shade);
  }
}
