#include <cstdint>
#include <cstdio>

void randomize(uint32_t&);

struct RandomNumberGenerator {
  explicit RandomNumberGenerator(uint32_t seed) : iterations{}, number{seed} {}
  uint32_t next();
  size_t get_iterations() const;

 private:
  uint32_t number;
  size_t iterations;
};

void run_randomize() {
  size_t iterations{};
  uint32_t number{0x4c4347};
  while (number != 0x4c4343) {
    randomize(number);
    ++iterations;
  }
  printf("%zu\n", iterations);
}

int main() {
  RandomNumberGenerator rng{0x4c4347};
  while (rng.next() != 0x4c4343) {
    // do nothing
  }
  printf("%zu\n", rng.get_iterations());
}

void randomize(uint32_t& x) {
  x = 0x3FFFFFFF & (0x41C64E6D * x + 12345) % 0x80000000;
}

uint32_t RandomNumberGenerator::next() {
  ++iterations;
  number = 0x3FFFFFFF & (0x41C64E6D * number + 12345) % 0x80000000;
  return number;
}

size_t RandomNumberGenerator::get_iterations() const { return iterations; }
