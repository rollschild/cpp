#include <cstdio>
#include <stdexcept>
#include <type_traits>

template <typename T>
auto value_of(T x) {
  // compile-time evaluation!
  if constexpr (std::is_pointer<T>::value) {
    if (!x) throw std::runtime_error{"Null pointer dereference!"};
    return *x;
  } else {
    return x;
  }
}

int main() {
  unsigned long level{8998};
  auto level_ptr = &level;
  auto& level_ref = level;
  printf("Power level = %lu\n", value_of(level_ptr));  // 8998
  ++*level_ptr;
  printf("Power level = %lu\n", value_of(level_ref));  // 8999
  ++level_ref;
  printf("One last try: %lu\n", value_of(level++));  // 9000
  printf("Over now: %lu\n", value_of(level));        // 9001

  try {
    level_ptr = nullptr;
    value_of(level_ptr);
  } catch (const std::exception& e) {
    printf("Exception: %s\n", e.what());
  }
}
