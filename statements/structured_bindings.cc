#include <cstdio>

struct TextFile {
  bool success;
  const char* data;
  size_t n_bytes;
};

TextFile read_text_file(const char* path) {
  const static char contents[]{"Some text to begin with..."};
  return TextFile{true, contents, sizeof(contents)};
}

int main() {
  if (const auto [success, contents, length] = read_text_file("statements.md");
      success) {
    printf("Read %zu bytes: %s\n", length, contents);
  } else {
    printf("Failed to open statements.md!\n");
  }
}
