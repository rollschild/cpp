#include <cstdio>

enum class Color {
  Mauve,
  Pink,
  Russet,
};

struct Result {
  const char* name;
  Color color;
};

Result observe_shrub(const char* name) { return Result{name, Color::Russet}; }

int main() {
  const char* description;
  switch (const auto result = observe_shrub("Jovi"); result.color) {
    case Color::Mauve: {
      description = "mauve";
    } break;
    case Color::Pink: {
      description = "pink";
    } break;
    case Color::Russet: {
      description = "russet";
    } break;
    default: {
      description = "no color matched!";
    }
  }
  // description = "something else"; // can be reseated?
  printf("The color is: %s\n", description);
}
