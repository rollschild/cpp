# Concurrency And Parallelism

- **concurrency**: two or more tasks running in a given time period
- **parallelism**: two or more tasks running at the same _instant_

## Resource Management

- **Resource** - something that must be _acquired_ and later _released_
  - memory
  - locks
  - sockets
  - thread handles
  - file handles
- A resource should _not_ _outlive_ an object responsible for it
- shared pointers are _copied_, _NOT_ moved
- When to use smart pointers vs. containers?
  - A pointer is _NOT_ needed if returning a collection of objects from a function
  - `shared_ptr` is needed when sharing an object
  - `unique_ptr` is needed when referring to a polymorphic object
-

## Concurrent Programming

- std supports concurrent execution of multiple threads in a single address space
  - a suitable memory model
  - a set of atomic operations
- `thread.join()`
  - make sure `thread` completes before going out of scope
- **concurrent** programs have multiple _threads of execution_ (or _threads_), which are sequences of instructions
- OS acts as a scheduler to determine when a thread executes its next instruction
- each process has one or more threads, which _typically_ share resources such as memory
- no guarantee of the order of the processing of threads

### Asynchronous Tasks

- `std::async` in `<future>`
- _Keep tasks completely separate except where they communicate in simple and obvious ways_
- `ref(arg)` - in `<functional>`
  - treat `arg` as a reference, rather than as a object
- Use `defer_lock` to avoid deadlock
-

#### `async`

- Arguments
  - (optional) lauch policy `std::launch`
    - `std::launch::async` - runtime creates a new thread to launch the task, or
    - `std::launch::deferred` - runtime waits until you need the task's result before executing
      - _lazy evaluation_
    - defaults to `async|deferred` - up to the implementation
  - task to be executed - a function object
- returns a `std::future` object
- `std::future<FuncReturnType> std::async([policy], func, Args&&... args);`
- Do _NOT_ even think of using `async()` for tasks that share resources needing locking
  - you do _NOT_ even know how many `thread`s will be used because it's up to `async()` to decide

#### future

- **future**
  - a class template that holds the value of an async task
  - has a single template parameter - the type of the async task's return value
- _future_ can be interacted with in _three_ ways
  - query the `future` about its validity using `valid` method
    - a _valid_ `future` has a shared state associated with it
    - async tasks have a shared state so that they can communicate with the results
    - any `future` returned by `async` will be valid until the async task's return value is retrieved
    - at which point the shared state's lifetime ends
- if a `future` is constructed using the default constructor, it is _not_ associated with a shared state
- `.get()` - obtain value from a valid `future`
  - if the async task has not been completed, the call to `.get()` will _block_ the currently executed thread
- check whether an async task has completed using either
  - `std::wait_for`, or
  - `std::wait_until`
  - if passing a `duration` object of `chrono`, use `std::wait_for`
  - if passing a `time_point` object of `chrono`, use `std::wait_until`
  - both return a `std::future_status`, which takes one of _three_ values:
    - `future_status::deferred` - the async task will be evaluated lazily - task will execute when `.get()` is called
    - `future_status::ready` - task completed & result ready
    - `future_status::timeout` - task is not ready
- if task completes before the specified waiting period, `async` will return early

#### `promise`

- Its purpose is to pass values (or exceptions) to `future`s
  - provide simple "put" operations to match `future`'s `get()`:
    - `set_value()`
    - `set_exception()`

#### `packaged_task`

- connects `future` with `promise` on `thread`s
- a `packaged_task` _CANNOT_ be copied
  - need to `move(packaged_task)`!

### Sharing and Coordinating

- **race condition**
- **Unsynchronized access to mutable shared data**
- Three tools to tackle this ^ :
  - **Mutex** - Mutual Exclusion Algorithm
  - **Atomic**
  - _Condition Variables_

#### Mutex

- prevents multiple threads from accessing resources simultaneously
- Mutexes are **synchronization primitives** that support two operations:
  - lock
  - unlock
- `<mutex>`
- `<shared_mutex>`
  - `std::shared_mutex`: several threads can own the mutex at once
    - when multiple readers can access the shared data but a writer needs exclusive access
  - `std::shared_timed_mutex`: shared mutual exclusion and locking with a timeout
- call `lock` or `try_lock`
- Mutex Implementation:
  - _spin lock_
    - simple but CPU expensive
  - _asynchronous procedure calls_
    - more efficient
- stdlib provides RAII classes for handling mutexes in the `<mutex>` header
  - `std::lock_guard`
  - `std::scoped_lock`
  - `std::unique_lock`
  - `std::shared_lock`
- RAII does _NOT_ incur any additional runtime costs
- _BUT_ mutual exclusion locks _do_ involve additional runtime costs
  - acquiring/releasing locks is relatively expensive
  - _SUBOPTIMAL_

#### Atomic

- An operation occurs in an indivisible unit
  - another thread cannot observe the operation halfway through
- `std::atomic` in `<atomic>`
- _lock-free concurrent programming_
- _much faster_ than acquiring mutex

#### Condition Variables

- provide basic support for communicating using external events
  - `condition_variable` in `<condition_variable>`
- A synchronization primitive
- Blocks one or more threads until notified
- Another thread can notify the condition variable
  - after notification, the condition variable can unblock one or more threads
- A typical/popular pattern:
  - Acquire some mutex shared with awaiting threads
  - Modify the shared state
  - Notify the condition variable
  - Release the mutex
- Any threads _waiting_ on the condition variable:
  - Acquire the mutex
  - Wait on the condition variable (this releases the mutex)
  - When another thread notifies the condition variable, this thread wakes up and can perform some work
    - *re*acquires the mutex automatically
  - Releases the mutex

### Low-Level Concurrency Facilities

- `<thread>`
  - low-level facilities for concurrent programming
  - best _not_ to use `thread` directly
  - instead, use higher-level abstractions, like tasks
- `std::this_thread::yield`
- `std::this_thread::get_id`
- `std::this_thread::sleep_for`
- `std::this_thread::sleep_until`

## Parallel Algorithms

- Pay attention any time an algorithm produces side effects beyond the target sequence!
- **RED FLAG**:
  - Any algorithm that passes a function object to the algo.
  - If the function object has shared mutable state, the executing threads will have shared access
  - race condition might occur!
