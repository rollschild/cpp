#include <functional>
#include <iostream>
#include <thread>
#include <vector>

using std::cout;
using std::endl;
using std::thread;
using std::vector;

void f(vector<double>& v) {
  for (auto ele : v) {
    cout << ele << " ";
  }
  cout << endl;
}

struct F {
  vector<double>& v;
  F(vector<double>& vv) : v{vv} {}

  void operator()() {
    for (auto ele : v) {
      cout << ele << " ";
    }
    cout << endl;
  }
};

int main() {
  vector<double> vec1{1, 2, 3, 4, 5};
  vector<double> vec2{6, 7, 8, 9, 0};

  // a function object is constructed for the thread to execute
  thread t1{f, std::ref(vec1)};
  thread t2{F{vec2}};

  t1.join();
  t2.join();
}
