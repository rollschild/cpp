#include <condition_variable>
#include <future>
#include <iostream>
#include <mutex>

using namespace std;

// deposit_cans completes _before_ eat_cans can start
void goat_rodeo() {
  mutex m;
  condition_variable cv;
  const size_t iterations{1'000'000};
  int tin_cans_available{};

  auto eat_cans = async(launch::async, [&] {
    unique_lock<mutex> lock{m};

    // will release the mutex and then block until two conditions are met:
    //   - `cv` awakens this thread
    //   - one million cans are available - to avoid spurious wakeups
    cv.wait(lock, [&] { return tin_cans_available == 1'000'000; });
    for (size_t i{}; i < iterations; ++i) {
      tin_cans_available--;
    }
  });

  auto deposit_cans = async(launch::async, [&] {
    scoped_lock<mutex> lock{m};
    for (size_t i{}; i < iterations; ++i) {
      tin_cans_available++;
    }
    cv.notify_all();
  });

  eat_cans.get();
  deposit_cans.get();

  cout << "Tin cans: " << tin_cans_available << "\n";
}

int main() {
  goat_rodeo();
  goat_rodeo();
  goat_rodeo();
  // Got all 0's on NixOS
}
