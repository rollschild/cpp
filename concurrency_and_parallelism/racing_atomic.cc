#include <atomic>
#include <future>
#include <iostream>

using namespace std;

// Unsynchronized access to mutable shared data
void goat_rodeo() {
  const size_t iterations{1'000'000};
  atomic_int tin_cans_available{};
  mutex tin_can_mutex;

  auto eat_cans = async(launch::async, [&] {
    for (size_t i{}; i < iterations; ++i) {
      tin_cans_available--;
    }
  });

  auto deposit_cans = async(launch::async, [&] {
    for (size_t i{}; i < iterations; ++i) {
      tin_cans_available++;
    }
  });

  eat_cans.get();
  deposit_cans.get();

  cout << "Tin cans: " << tin_cans_available << "\n";
}

int main() {
  goat_rodeo();
  goat_rodeo();
  goat_rodeo();
  // Got all 0's on NixOS
}
