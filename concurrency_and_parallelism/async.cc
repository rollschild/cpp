#include <catch2/catch_test_macros.hpp>
#include <future>
#include <string>

using namespace std;

TEST_CASE("async returns valid future") {
  using namespace literals::string_literals;
  auto the_future =
      async([] { return "female"s; });  // async task that returns a string

  REQUIRE(the_future.valid());
}

TEST_CASE("future invalid by default") {
  future<bool> default_future;
  REQUIRE_FALSE(default_future.valid());
}

TEST_CASE("async returns the return value of the function object") {
  using namespace literals::string_literals;
  auto the_future = async([] { return "female"s; });
  REQUIRE(the_future.get() == "female");  // this will block the thread
}

TEST_CASE("get may throw") {
  auto except = async([] { throw runtime_error{"Exception thrown!"}; });
  REQUIRE_THROWS_AS(except.get(), runtime_error);
}

TEST_CASE("wait_for indicates whether a task is ready") {
  using namespace literals::chrono_literals;
  auto sleepy = async(launch::async, [] { this_thread::sleep_for(100ms); });
  const auto not_ready_yet = sleepy.wait_for(25ms);
  REQUIRE(not_ready_yet == future_status::timeout);

  // NOT guaranteed to pass
  // OS is repsonsible for scheduling threads,
  // it might schedule the sleeping thread later than the specified duration
  const auto now_ready = sleepy.wait_for(80ms);
  REQUIRE(now_ready == future_status::ready);
}
