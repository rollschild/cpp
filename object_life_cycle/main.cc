#include <cstdio>
#include <cstring>
#include <stdexcept>

static int power_rating = 200;

void power_up_rating(int step) {
  // NOTICE heat is undeclared here
  // printf("heat? %d\n", heat);
  power_rating += step;
  const auto heat = power_rating * 20;
  if (heat > 10000) {
    printf("WARNING!\n");
  }
}
void power_static() {
  printf("power_rating: %d\n", power_rating);
  power_up_rating(100);
  printf("power_rating: %d\n", power_rating);
  power_up_rating(500);
  printf("power_rating: %d\n", power_rating);
}

void power_local_static(int step) {
  static int power_rating_local = 200;
  // first time is 200, next time this function is called it's 300
  printf("power_rating_local: %d\n", power_rating_local);

  power_rating_local += step;
  printf("power_rating_local: %d\n", power_rating_local);
}

struct PowerRating {
  static int power_rating;
  static void power_up(int step) {
    printf("before up: %d\n", power_rating);
    power_rating += step;
    printf("after up: %d\n", power_rating);
  }
};

// this is how you initialize it
int PowerRating::power_rating = 200;

struct Tracer {
  Tracer(const char* name) : name{name} { printf("%s constructed.\n", name); }
  ~Tracer() { printf("%s destructed.\n", name); }

 private:
  const char* const name;
};

static Tracer t1{"Static variable"};
thread_local Tracer t2{"Thread-Local variable"};

void object_life_cycle() {
  const auto t2_ptr = &t2;
  printf("A\n");
  Tracer t3{"Automatic variable"};
  printf("B\n");
  const auto* t4 = new Tracer{"Dynamic variable"};
  printf("C\n");

  // you would need to explicitly do this:
  // to destroy this storage
  // otherwise it will LEAK
  // and this desctructor will print FIRST
  delete t4;
}

struct Forget {
  void forget(int x) {
    if (x == 0xFACE) {
      throw std::runtime_error{"Exception thrown!"};
    }
    printf("Forgot 0x%x\n", x);
  }
};

struct SimpleString {
  // max length of the string, which _includes_ the null terminator
  SimpleString(size_t max_size) : max_size{max_size}, length{} {
    // this < 0 check is actually not necessary
    // because size_t is guaranteed to be unsigned and non-negative
    if (max_size <= 0) {
      throw std::runtime_error{"Max size must be at least 1!"};
    }
    buffer = new char[max_size];
    buffer[0] = 0;  // null byte?
    printf("SimpleString constructed!\n");
  }

  // copy constructor
  SimpleString(const SimpleString& other)
      : max_size{other.max_size},
        buffer{new char[other.max_size]},
        length{other.length} {
    std::strncpy(buffer, other.buffer, max_size);
  }

  // use-defined copy assignment operator
  // it returns a reference to the result, which is always `*this`
  SimpleString& operator=(const SimpleString& other) {
    if (this == &other) return *this;

    const auto new_buffer = new char[other.max_size];
    delete[] buffer;
    buffer = new_buffer;
    length = other.length;
    max_size = other.max_size;
    std::strncpy(buffer, other.buffer, max_size);

    return *this;
  }

  // Move constructor
  // Prefer to use `noexcept` move constructor
  SimpleString(SimpleString&& other) noexcept
      : max_size{other.max_size}, buffer(other.buffer), length(other.length) {
    other.length = 0;
    other.buffer = nullptr;
    other.max_size = 0;
  }

  // Move assignment
  SimpleString& operator=(SimpleString&& other) noexcept {
    if (this == &other) return *this;

    delete[] buffer;
    buffer = other.buffer;
    length = other.length;
    max_size = other.max_size;
    other.buffer = nullptr;
    other.length = 0;
    other.max_size = 0;

    return *this;
  }

  /*
   * Pair the allocation and deallocation of `buffer` with the constructor and
   * destructor, guaranteed that there will be no memory/storage leak the
   * **resource acquisition is initialization** (RAII) pattern a.k.a.
   * **constructor acquires, desctructor releases**
   */
  ~SimpleString() {
    printf("SimpleString about to be destroyed!\n");
    delete[] buffer;
  }

  void print(const char* tag) const { printf("%s: %s", tag, buffer); }
  bool append_line(const char* x) {
    const auto x_len = strlen(x);
    if (x_len + length + 2 > max_size) return false;
    std::strncpy(buffer + length, x, max_size - length);
    length += x_len;
    buffer[length++] = '\n';
    buffer[length] = 0;
    return true;
  }

 private:
  size_t max_size;
  char* buffer;
  size_t length;
};

struct SimpleStringOwner {
  SimpleStringOwner(const char* x) : string{10} {
    if (!string.append_line(x)) {
      throw std::runtime_error{"Not enough memory!"};
    }
    string.print("Constructed");
  }
  // copy constructor?
  SimpleStringOwner(const SimpleString& my_string) : string{my_string} {}
  ~SimpleStringOwner() { string.print("About to be destroyed!"); }

 private:
  SimpleString string;
};

void simple_string() {
  SimpleString string{115};
  string.append_line("abc");
  string.append_line("I'm good.");
  string.print("A");
}
void simple_string_owner() {
  SimpleStringOwner x{"x"};
  printf("x is alive!\n");
}

void throw_c() {
  SimpleStringOwner c{"cccccccccc"};  // this will throw
}
void throw_b() {
  SimpleStringOwner b{"b"};  // this will throw
  throw_c();
}

void copy() {
  SimpleString a{50};
  a.append_line("We apologize for the");
  SimpleString a_copy{a};
  a.append_line("inconvenience.");
  a_copy.append_line("inconvenience.");
  a.print("a");
  a_copy.print("a_copy");
}
void copy_assignment() {
  SimpleString a{50};
  a.append_line("We apologize for the");
  SimpleString a_copy{50};
  a_copy.append_line("inconvenience.");
  a.print("a");
  a_copy.print("a_copy");
  a_copy = a;
  a.print("a");
  a_copy.print("a_copy");
}

void ref_type(int& x) { printf("lvalue reference: %d\n", x); }
void ref_type(int&& x) { printf("rvalue reference: %d\n", x); }
void value_categories() {
  auto x = 1;
  ref_type(x);
  ref_type(std::move(x));  // rvalue
  ref_type(2);
  ref_type(x + 2);
}

void move_assignment() {
  SimpleString a{50};
  a.append_line("We apologize for the");
  SimpleString b{50};
  b.append_line("inconvenience.");
  a.print("a");
  b.print("b");

  // a is lvalue; you need ot cast it to rvalue first
  b = std::move(a);
  b.print("b");
}

int main() {
  // PowerRating::power_up(100);
  // PowerRating::power_up(300);

  // object_life_cycle();

  // Forget forget;
  // try {
  //   forget.forget(0xC0DE);
  //   forget.forget(0xFACE);
  //   forget.forget(0xC0FFEE);
  // } catch (const std::runtime_error& e) {
  //   printf("exception encountered: %s\n", e.what());
  // }

  // simple_string();
  // simple_string_owner();

  // try {
  //   SimpleStringOwner a{"a"};
  //   throw_b();
  //   SimpleStringOwner d{"d"};
  // } catch (const std::exception& e) {
  //   printf("Exception: %s\n", e.what());
  // }

  // copy();
  // copy_assignment();

  // value_categories();

  move_assignment();
}
