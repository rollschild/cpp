# Object Life Cycle

## Object

- an **object** is a region of storage that has a type and a value
- when a variable is declared, an object is created

## Allocation, Deallocation, Lifetime

- **allocation** vs. **deallocation**
- **lifetime**
  - a _runtime_ property that is bound by the object's storage duration
  - begins once its constructor completes
  - ends just _before_ a destructor is invoked
- Stages:
  1. object's duration begins; storage allocated
  2. object's constructor called
  3. object's lifetime begins
  4. can be used in the program
  5. lifetime ends
  6. destructor called
  7. object's storage duration ends; storage deallocated

## Memory Management

- **automatic memory manager** / **garbage collector**
- at runtime, programs create objects

## Automatic Storage Duration

- an **automatic object** is allocated at the beginning of an enclosing code block
  - and deallocated at the end
  - **scope**
  - this object has **automatic storage duration**
- function parameters are automatic

## Static Storage Duration

- **static object**
  - declared using `static` or `extern`
  - declared at the same level as functions: global or namespace scope
- static objects with global scope have **static storage duration**
  - allocated when the program starts
  - deallocated when it stops
- `static` sepcifies **internal linkage**
- `extern` specifies **external linkage**
  - accessible to other translation units

## Local Static Variables

- declared at function scope
- lifetime begins upon the _first_ invocation of the enclosing function
- lifetime ends when the program exits

## Static Members

- members of a `class` that are **NOT** associated with a particular instance of the class
- static storage duration
- `Class::static_member`
- **Must initialize static members at global scope**
- **Cannot initialize a static member within a containing class definition**
  - exception:
    - you _can_ declare/define integral types within a class definition as long as they are `const`
- static members have **ONLY ONE** instance

## Thread-Local Storage Duration

- **thread of execution**
  - the sequence of instructions that a thread executes
- **thread-safe** code
  - **mutable global variables** - one common issue
  - Fix: **thread storage duration**
    - give each thread its own _copy_ of the variable
    - add `thread_local` to `static`/`extern`

## Dynamic Storage Duration

- allocated/deallocated on request
- a.k.a. **allocated objects**
- To allocate a dynamic object:
  - `new`
    - creates the object
    - returns a **pointer** to the newly minted object
- use `delete` to deallocate dynamic objects
  - `delete ptr`
  - `delete` expression _always_ returns `void`
  - In practice, compilers typically do **NOT** clean up memory after the object is deleted
    - bug: **use after free**

### Dynamic Arrays

- `new Type[n_elements]{init-list}`
- returns pointer to the first element of the newly allocated array
- to deallocate: `delete[] array_ptr`

## Exceptions

- Types that communicate an error condition
- When exceptions are **in flight**, they search for an **exception handler**
  - objects that fall out of scope during this process are _destroyed_
- Return an error code
  - as part of a functino's prototype
  - when an error occurs that can be dealt with locally, or
  - when the error is expected to occur during the normal course of a program's execution
- Most objects are `throw`able
- Use `std::runtim_error` from the `<stdexcept>` header
  - accepts a null-terminated `const char*`
  - retrieve this message using `what` method

### stdlib Exception Classes

- `std::exception` is the super class
- Three sub groups:
  - logic errors
  - runtime errors
  - language support errors
- `logic_error` has several subclasses:
  - `domain_error`
  - `invalid_argument`
  - `length_error`
  - `out_of_range`
- `runtime_error` subclasses:
  - `system_error`
    - use the `.code()` method
      - returns an `enum class` of `std::errc`
      - `bad_file_descriptor`
      - `permission_denied`
  - `overflow_error`
  - `underflow_error`
- other errors inherit directly from `exception`
  - `bad_alloc`

### Handling Exceptions

- rules are based on class inheritance
- a `catch` block handles the exception if
  - the thrown exception's type matches the `catch` handler's exception type, or
  - if the thrown exception's type **inherits** from the `catch` handlers's exception type
- special handler:

```c++
try {
  throw "some error"; // DO NOT do this
} catch (...) {
  // handler
}
```

- a safe one:

```c++
try {
  // throw something
} catch (const std::logic_error& ex) {
  // handler
} catch (const std::runtim_error& ex) {
  // handler
} catch ( const std::exception& ex) {
  // handle any exception that derives from std::exception
  // this is *NOT* logic_error or runtime_error
} catch (...) {
  // PANIC
}
```

- you can **rethrow** in the `catch` block by doing just `throw;`
  - performance penalties
  - messy code

### User-Defined Exceptions

- also inherit from `std::exception`
- `noexcept`
  - mark any function that cannot possibly throw an exception
  - BE CAREFUL when using this!
  - if there _is_ error thrown in a `noexcept`, C++ runtime will exit the program via `abort`
    - by calling `std::terminate`
    - you **CANNOT** recover

```c+
bool is_odd(int x) noexcept {
  return 1 == (x % 2);
}
```

### Call stacks and Exceptions

- call stack is a _runtime_ structure that stores information about active functions
- **stack frame**
- **Treat destructors as they were `noexcept`**

## The SimpleString Class

- use `strlen` from `<cstring>`
  - `size_t strlen(const char* str);`
  - it takes the `null` terminated C style string, but returns the length that does **NOT** include the `null` terminator
- **All members are destructed after the object's destructor is invoked**
- an object's member constructors are called before the object's constructor

## Exceptions and Performance

- exception handling can be slow

## Alternatives to Exceptions

- Manually force class invariants by exposing some method that communicates whether the class invariants could be established
- **structured binding declaration**
  - return multiple values from a function
  - `return {val_1, val_2};`
  - `auto [val_1, val_2] = returning_func();`

## Copy Semantics

- Pass by value
  - for user-defined POD types
    - each member value is copied into the parameter
    - a **member-wise copy**

### Copy Constructors

- Two ways to copy an object:
  - use **copy construction**
  - **copy assignment operator**
- Copy construction
  - creates a copy and assigns it to a brand-new object
  - **deep copy**
  - the copy constructor is invoked when passing the class/struct into a function _by value_
    - performance impact can be substantial
  - try to pass a `const` reference whenever possible

```c++
struct SimpleString {
  SimpleString(const SimpleString& other); // notice that `other` is `const`
};
SimpleString a;
SimpleString a_copy{a};

```

- Copy assignment operator
  - `object_b = object_a;` - **copy assigns** `object_a` to `object_b`
- Rules to implement copy assignment:
  - free the current `buffer` of `this`
  - copy `other` as you would do in copy construction

### Default Copy

- Any time a class manages a resource, be extremely careful with default copy semantics!
- Best Pratice:
  - Explicitly declare that the default copy assignment and copy construction are acceptable for such classes using the `default` keyword

```c++
struct Replicant {
  Replicant(const Replicant&) = default;
  Replicant& operator=(const Replicant&) = default;
};
```

- Some classes simply **CANNOT** or **SHOULD NOT** be copied
  - if managing a file, or
  - if represents a mutual exclusion lock for concurrent programming
  - To suppress the compiler from generating a copy construction and a copy assignment operator
    - use the `delete` keyword

### Copy Guidelines

- Correctness
- Independence
- Equivalence

## Move Semantics

- Transfer ownership
  - **move**
- After the move, the two objects are separate objects with separate storage and _potentially_ separate lifetimes
- After the move, the moved-from object is in **moved-from** state
  - on which **only two** operations can be performed
    - **(re)assign**
    - **destruct**
- Specify your own **move constructors** and **move assignment operators**
- Compiler has safeguards against dangers broughy by **move**s
  - **lvalue**
  - **rvalue**

### Value Categories

- Every expression has two characteristics:
  - **type**
  - **value category**
- **Value Category**
  - defines what operations are valid for the expression
- Possible value categories:
  - **glvalue** - generalized lvalue
  - **prvalue** - pure rvalue
  - **xvalue** - expiring value
  - **lvalue** - a **glvalue** that is not an **xvalue**
  - **rvalue** - **prvalue** or **xvalue**
- Generally speaking (not always true though):
  - **lvalue** is any value that has a name
  - **rvalue** is anything that is not an lvalue
- Use **lvalue references** and **rvalue references** to communicate to compiler that a function accepts lvalues or rvalues
- rvalue reference: `&&`
- Use `std::move` from `<utility>` to cast an lvalue to an rvalue
  - it does **NOT** actually move anything - it **casts**
- Move construction
  - remember to clear the moved-from object
  - executing the move constructor is **a lot** less expensive than executing the copy constructor
  - **YOU SHOULD** use `noexcept` move constructors
  - often, compiler cannot use exception-throwing move constructors and will use copy constructors instead
  - `SimpleStringOwner(SimpleString&& x) : string{std::move(x)} {}`
