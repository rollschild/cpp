# Intro to C++

- `stblib`
  - three parts:
    - containers
    - iterators
    - algorithms
- The zero-overhead principle:
  - **what you don't use, you don't pay for**
- Differences/improvements to C
  - pointers vs. references
    - `*` vs. `&`
    - under the hood they are equivalent because they are both zero-overhead
      abstraction
    - references cannot be null
      - it does **NOT** mean references are always valid
    - references cannot be _reseated_
  - `namespace`
    - `using namespace`
  - `__cplusplus`
    - a special identifier defined by C++, but not available in C

```c++
#ifdef __cplusplus
extern "C" {
#endif
```

- **lambdas**
  - a.k.a. **unnamed**/**anonymous** functions

```c++
[capture] (arguments) {body}
```

- **Generic Programming**
  - use `template`s
- **Object Life Cycle**
  - constructor and destructor
- **Resource Allocation Is Initialization** (RAII)
  - a.k.a. **constructor acquires, destructor releases**
- smart pointers
  - `unique_ptr`
  -
