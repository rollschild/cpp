#include <cstdio>
#include <iostream>

constexpr unsigned short select_middle(int a) {
  static_assert(sizeof(int) == 4, "Unexpected int size!");
  static_assert(sizeof(short), "Unexpected short size!");

  return (a >> 8) & 0xFFFF;
}

int main() {
  // notice the unsigned vs. signed here
  unsigned int x{0xFF00FF00};
  short y = select_middle(x);

  printf("middle 16 bits of %x is %x\n", x, y);
}
