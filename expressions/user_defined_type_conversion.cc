#include <cstdio>

struct ReadOnlyInt {
  ReadOnlyInt(int val) : val{val} {}

  // notice the `explicit` keyword
  explicit operator int() const { return val; }

 private:
  const int val;
};

int main() {
  ReadOnlyInt answer{42};
  // auto res = answer * 10; // you need to explicitly cast it!
  auto res = static_cast<int>(answer) * 10;
  printf("res: %d\n", res);
}
