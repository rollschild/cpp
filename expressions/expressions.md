# Expressions

- **Expressions** are computations that produce results and side effects
- using a short `string` does _not_ require any use of free store
- max number of characters for a short `string` is implementation-dependent;
  - 14 maybe?

## Operators

- logical operators
  - take arithmetic and pointer types as operands then convert them to `bool`
- bitwise logical operators
  - apply to objects of integral types:
    - `char`
    - `short`
    - `int`
    - `long`
    - `long long`
    - `unsigned` of above
    - `bool`
    - `wchar_t`
    - `char16_t`
    - `char32_t`
    - plain `enum`, _NOT_ `enum class`, can be implicitly converted to an integer type
- Typical use of bitwise logical operators: implement a \***\*small set\*\*** (a bit vector)

  - each bit of an unsigned integer represents one **member** of the set
  - number of bits limits number of members
  - `&` - intersection
  - `|` - union
  - `^` - symmetric difference
  - `~` - complement
  - `|=` - add to the state
  - `enum` can be used to name the members of such a set:

    ```c++
    enum ios_base::iostate {
      goodbit = 0, eofbit = 1, failbit = 2, badbit = 4,
    };

    // to report reaching end-of-input:
    state |= eofbit;

    // state flags are observable from outside the stream impl
    int old = cin.readstate(); // returns the state
    // ...use cin...
    if (cin.readstate() ^ old) { // has anything changed?
      // ...
    }
    ```

  - low level only! - use `set` or `bitset` for general usecases

- `&` has a _higher_ precedence than `|`
- `^` - xor
- `~0` === `-1` in 2's complement representation
  - returns `true` only if two operands are different
- Alternative representation of logical operators:
  - `and` - `&&`
  - `and_eq` - `&=`
  - `bitand` - `&`
  - `bitor` - `|`
  - `compl` - `~`
  - `not` - `!`
  - `not_eq` - `!=`
  - `or` - `||`
  - `or_eq` - `|=`
  - `xor` - `^`
  - `xor_eq` - `^=`
- arithmetic operators
- Unary Arithmetic Operators
  - `+` - unary plus
  - `-` - unary minus
  - they both **promote** their operands to `int`
    - if the operand is `bool`, `char`, or `short int`, the result of the expression is `int`
- Binary Arithmetic Operators
  - also **promotes**
    - adding two `char`s will result in `int`
  - floating-point promotion rules:
    - if an operand is `long double`, promote the other to `long double`
    - if an operand is `double`, promote the other to `double`
    - if an operand is `float`, promote the other to `float`
    - if none of the rules above apply, check whether either is signed
      - if so, both become signed
  - the largest operand promotes the other operand
    - if an operand is `long long`, the other is promoted to `long long`
    - if an operand is `long`, the other is promoted to `long`
    - if an operand is `int`, the other is promoted to `int`
  - prefer `auto`
- cast vs. promotion
  - casting is when you have an object of one type and need to convert it to another type
  - promotion is the set of rules for interpreting literals
- Assignment Operators
  - result of the assignment is the value of the variable assigned to
- promotion rules do **NOT** really apply: type of the assigned to operator will **NOT** change
- indirection operator `*x`
- address-of operator `&x`
- Ternary Conditional Operator
- The Comma Operator
  - the expressions evaluate from left to right
  - the rightmost expression is the return value

### Operator Overloading

- for user-defined types

### Overloading Operator `new`

- `new` by default allocates memory on the **free store**
  - a.k.a. **heap**
  - objects create can live _after_ the function in which they were created returns
  - managed by `HeapAlloc` on Windows and `malloc` on \*Nix
- If a type has a default constructor (such as `complex<>`), the initializer can be left out
- object created by `new` exists _until_ _explicitly_ destroyed by `delete`
  - then its space occupied can be _reused_ by `new`
- BUT built-in types are by default _uninitialized_
  - make sure to use default initialization, `{}`
- `delete` can _only_ be applied to:
  - pointer returned by `new`
  - `nullptr` - no effect
- If the object being deleted is of a class with a destructor, the destructor is called by `delete` _before_ the object's memory is released for reuse
- Three main problems with free store:
  - \***\*leaked objects\*\***
  - \***\*premature deletion\*\***
  - \***\*double deletion\*\***
- Rule of thumb when using `new`s: NO NAKED `new`s
  - `new` belongs in constructors
  - `delete` belongs in destructors
- To deallocate space (allocated by `new`), `delete` and `delete[]` must _be able to determine_ size of the object allocated
  - an object allocated by `new` will occupy (slightly) more space than a static object
  - at minimum, the object's size
  - usually two or more words (8 bytes each) per allocation
  - can be significant if allocating a lot of small objects on heap
- `new` throws `bad_alloc` when it cannot find store to allocate
  - _BUT_ `new` is _NOT_ guaranteed to throw when running out of _physical_ memory
  - when there is virtual memory, the program can consume a lot of disk space before finally throwing `bad_alloc`
- use `set_new_handler(the_new_handler)` to specify what `new` should do upon memory exhaustion
- In some environments (Windows kernel or embedded) there is no heap available by default
- In game development or high-frequency trading, heap allocations involve too much latency
  - heap management is delegated to the OS
- Avoid using heap has severe limits:
  - precludes the use of stdlib containers
- Fix/workaround: overload the free store operations and take control over allocations
  - by overloading operator `new`

#### The `<new>` Header

- provides the following four operators:
  - `void* operator new(size_t);`
  - `void operator delete(void*);`
  - `void* operator new[](size_t);`
  - `void operator delete[](void*);`
- return type of `new` is `void*`
  - heap operators deal in raw, uninitialized memory
- Issues with heap management:
  - **memory fragmentation**
- Buckets
  - chop allocated memory into _buckets_ of a fixed size
  - functions provided by Windows for allocating dynamic memory:
    - `VirtualAllocEx`
      - low-level
      - never allocates fewer than 4096 bytes (a **page**)
    - `HeapAlloc`
      - higher level
      - default on Visual Studio
- One way to allocate a `Heap` is to declare it at namespace scope so it has static storage duration
- Placement Operators
  - if you do not want to override _all_ heap allocations
  - `void* operator new(size_t, void*);`
  - `void operator delete(size_t, void*);`
  - `void* operator new[](void*, void*);`
  - `void operator delete[](void*, void*);`
- the `size_t` argument is implicit - _every_ `operator new()` has a `size_t` as its first argument
  - you can manually manipulate an objects lifetime
  - but you **CANNOT** use `delete` to release the resulting dynamic objects
    - must call the object's destructor, exactly once
- `nothrow new` - `nothrow` versions of `new`
  ```c++
  void f(int n) {
    int* p = new(nothrow) int[n];
    if (p == nullptr) {
      // handle allocation error
    }
    // ...
    operator delete(p, nothrow);
  }
  ```
  - `nothrow` is the name of an object of std type `nothrow_t`
  - declared in `<new>`
  - implementations:
    - `void* operator new(size_t sz, const nothrow_t&) noexcept;`
    - `void operator delete(void* p, const nothrow_t&) noexcept;`
    - `void* operator new[](size_t sz, const nothrow_t&) noexcept;`
    - `void operator delete[](void* p, const nothrow_t&) noexcept;`
  - returns `nullptr` rather than throwing `bad_alloc` if not sufficient memory to allocate

#### Arena

```c++
class Arena {
 public:
  virtual void* alloc(size_t) = 0;
  virtual void free(void*) = 0;

  // ...
};

void* operator new(size_t sz, Arena* a) {
  return a->alloc(sz);
}

// In another file
extern Arena* Persistent;
extern Arena* Shared;

void g(int i) {
  X* p = new(Persistent) X(i); // X in persistent storage
  X* q = new(Shared) X(i); // X in shared memory
  // ...
}

void destroy(X* p, Arena* a) {
  p->~X(); // call destructor
  a->free(p); // free memory
}
```

### Evaluation Order vs. Precedence

- **precedence**: a compile time concept that drives how operators bind to operands
- **evaluation order**: runtime concept that drives the scheduling of operator execution
- C++ has **NO** clearly specified execution order for _operands_
- operators `,`, `&&`, and `||`
  - guarantee that the left-hand operand is evaluated _before_ their right-hand operand
- `b = ++a + a;` has undefined behavior
- example:

  ```c++
  void cpy(char* p, const char* q) {
    while (*p++ = *q++);
  }
  ```

- the _most efficient_ way of copying zeor-terminated char string: `strcpy` from `<string.h>`
  ```c++
  char* strcpy(char*, const char*);
  ```
- the comma operator `a, b` guarantees `a` executes _before_ `b`
- the constructor arguments in a `new` expression evaluate before the call to the _allocator_ function

## User-Defined Literals

- the `<chrono>` header uses literals extensively to give programmers a clean syntax for using time types
  - `700ms`

## Type Conversions

### Implicit

- the **promotion** is a form of implicit conversion that _preserves_ values
- Before an arithmetic operation is performed, **integral promotion** is used to create `int`s out of shorter integer types
- braced initializer does **NOT** permit narrowing conversions

```c++
auto x = 2.7182818284590452353602874713527L;
uint8_t y = x; // silent truncation
uint8_t y2{x}; // does not allow narrowing conversion
```

#### Integer-to-Integer Conversion

- if destination is `signed`,
  - as long as the value can be represented
  - if cannot, it's undefined behavior
- if destination is `unsigned`,
  - as many bits as can fit into the type

#### Floating-Point-to-Floating-Point

- Loss of precision occurs if an integral value cannot be represented exactly as a value of the floating type
- if destination value cannot fit source value, it's UB
- use braced initializer!
  - it saves you from conversions that lose information
- safe conversions:
  - `float` to `double`
  - `double` to `long double`
- UB
  - `long double` to `float`
- `numeric_limits<long double>::max()` - in `<limits>`
- `DBL_MAX` and `FLT_MAX`
  - defined in `<cfloat>`

#### Pointer and Reference Conversions

- _Any_ pointer to an object type can be implicitly converted to a `void*`
  - NOTE:
    - pointer to a function or pointer to a member _CANNOT_ be implicitly converted to `void*`
- A _constant expression_ that evaluates to `0` can be implicitly converted to a null pointer of _any_ pointer type
- `T*` can be implicitly converted to `const T*`
- `T&` can be implicitly converted to `const T&`

#### Conversion to `bool`

- poniters, integers, and floating-point numbers can _all_ be implicitly converted to `bool`

#### Pointers to `void*`

- always

### Explicit Type Conversion

- a.k.a. **cast**
- braced initialization `{}`
  - fully type safe
  - non-narrowing
- Use `narrow_cast<>()` if narrowing conversions are unavoidable

### C-Style Casts

- `(desired-type)object-to-cast`
- **DANGEROUS**
- use `staic_casts`, `const_casts`, and `reinterpret_casts`

### User-Defined Type Conversions

```c++
struct MyType {
  operator destination-type() const {
    // return a destination-type here
  }
};
```

- Example:

```c++
struct ReadOnlyInt {
  ReadOnlyInt(int val) : val{val} {}
  operator int() const {
    return val;
  }

  private:
    const int val;
};
```

- Use `explicit` wherever you can

## Constant Expressions

- expressions that can be evaluated at compile time
- A constant (`const` or `constexpr`) _CANNOT_ be left uninitialized
- recommended!
- `constexpr`
- _CANNOT_ have side effects
- the compiler is compelled to compute the expression if all information required is present
- if initialization is done at compile time, there can be _NO_ data races on that object in a multi-threaded system
- _all_ `constexpr` expressions are `const` because they are _always_ fixed at runtime
- Avoid **magic values**
  - manually calculated constants copy and pasted directly into source code
- the **conditional-expression operator** `?:` - selection in a constant expression
  - first, the condition is evaluated
  - then the selected alternative is evaluated
  - the alternative _not_ selected is _not_ evaluated - might even not be a constant expression
  - one branch may be a `throw`-expression
- `const` vs. `constexpr`
  - `const` can be initialized by a _non_-constant expression
    - in that case, the `const` _CANNOT_ be used as a constant expression
- a constant expression must start out with an integral value, a floating-point value, or an enumerator
  - then they can be combined using operators, etc.
  - `string` is _NOT_ a constant expression - not a literal type!

### Literal Types

- A class with a `constexpr` constructor is called a \***\*literal type\*\***
- To be `constexpr`, the constructor must:
  - have _empty_ body
  - all members must be initialized by potentially constant expressions
- for a member function, `constexpr` implies `const`, so the `const` in `constexpr member_func() const {}` is _not_ necessary
- arrays _can_ be `constexpr`
- The address of a statically allocated object (such as a global variable) is a constant
  - but its value is assigned by the linker (_NOT_ the compiler)
  - compiler does not know the value of the address constant

## Volatile Expressions

- `volatile`
  - tells the compiler that every access through this expression must be treated as a visible side effect
  - access cannot be optimized out or reordered with another visible side effect
- **dead store**
- **redundant load**

```c++
int foo(volatile int& x) {
  x = 10; // dead store
  x = 20;
  auto y = x;
  y = x; // redundant load
  return y;
}
```

- `volatile` does **NOT** guarantee thread safety!
  - different from `atomic`
