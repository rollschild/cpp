#include <cstdint>
#include <cstdio>

constexpr uint8_t max(uint8_t a, uint8_t b) { return a > b ? a : b; }
constexpr uint8_t max(uint8_t a, uint8_t b, uint8_t c) {
  return max(max(a, b), max(a, c));
}
constexpr uint8_t min(uint8_t a, uint8_t b) { return a < b ? a : b; }
constexpr uint8_t min(uint8_t a, uint8_t b, uint8_t c) {
  return min(min(a, b), min(a, c));
}

constexpr float modulo(float dividend, float divisor) {
  const auto quotient = dividend / divisor;
  return divisor * (quotient - static_cast<uint8_t>(quotient));
}

struct Color {
  float H, S, V;
};

constexpr Color rgb_to_hsv(uint8_t r, uint8_t g, uint8_t b) {
  Color color{};
  const auto c_max = max(r, g, b);
  color.V = c_max / 255.0f;

  const auto c_min = min(r, g, b);
  const auto delta = color.V - c_min / 255.0f;
  color.S = c_max == 0 ? 0 : delta / color.V;

  if (c_max == c_min) {
    color.H = 0;
    return color;
  }

  if (c_max == r) {
    color.H = (g / 255.0f - b / 255.0f) / delta;
  } else if (c_max == g) {
    color.H = (b / 255.0f - r / 255.0f) / delta + 2.0f;
  } else if (c_max == b) {
    color.H = (r / 255.0f - g / 255.0f) / delta + 4.0f;
  }

  color.H *= 60.0f;
  color.H = color.H >= 0.0f ? color.H : color.H + 360.0f;
  color.H = modulo(color.H, 360.0f);

  return color;
}

int main() {
  auto black = rgb_to_hsv(0, 0, 0);
  auto white = rgb_to_hsv(255, 255, 255);
  auto red = rgb_to_hsv(255, 0, 0);
  auto green = rgb_to_hsv(0, 255, 0);
  auto blue = rgb_to_hsv(0, 0, 255);

  printf("Color: %f, %f, %f\n", black.H, black.S, black.V);
  printf("Color: %f, %f, %f\n", white.H, white.S, white.V);
  printf("Color: %f, %f, %f\n", red.H, red.S, red.V);
  printf("Color: %f, %f, %f\n", green.H, green.S, green.V);
  printf("Color: %f, %f, %f\n", blue.H, blue.S, blue.V);
}
