#include <cctype>
#include <cstdio>
#include <iostream>
#include <istream>
#include <map>
#include <ostream>
#include <sstream>
#include <string>

using std::cerr;
using std::cin;
using std::cout;
using std::map;
using std::string;

double term(bool);
double prim(bool);

int num_of_errors;

double error(const string& s) {
  num_of_errors++;
  cerr << "ERROR!: " << s << '\n';
  return 1;
}

enum class Kind : char {
  name,
  number,
  end,
  plus = '+',
  minus = '-',
  mul = '*',
  div = '/',
  print = ';',
  assign = '=',
  lp = '(',
  rp = ')',
};

struct Token {
  Kind kind;
  string string_value;
  double number_value;
};

class TokenStream {
 public:
  TokenStream(std::istream& s) : ip{&s}, owns{false} {}
  TokenStream(std::istream* p) : ip{p}, owns{true} {}
  ~TokenStream() { close(); }

  Token get();                                 // read and return next token
  Token get_v2();                              // read and return next token
  const Token& current() const { return ct; }  // most recently read token

  void set_input(std::istream& s) {
    close();
    ip = &s;
    owns = false;
  }
  void set_input(std::istream* p) {
    close();
    ip = p;
    owns = true;
  }

 private:
  void close() {
    if (owns) {
      delete ip;
    }
  }
  std::istream* ip;
  bool owns;
  // has a default value
  // `current()` should _not_ be called before `get()`
  // but if it does, a well defined Token will be returned
  Token ct{Kind::end};
};

/*
 * read a character,
 * use that character to decide what kind of token needs to be composed,
 * read more characters when needed,
 * return a Token representing the chars read
 */
Token TokenStream::get() {
  // by default, `operator>>` skips whitespaces
  // and leaves the value of `ch` unchanged if input operation failed
  char ch{0};
  *ip >> ch;
  switch (ch) {
    case 0:
      // equivalent to `return ct = {Kind::end, 0, 0};`
      return ct = {Kind::end};
    case ';':
    case '*':
    case '/':
    case '+':
    case '-':
    case '(':
    case ')':
    case '=':
      return ct = {static_cast<Kind>(ch)};
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '.':
      // put it back into the input stream
      ip->putback(ch);
      *ip >> ct.number_value;
      ct.kind = Kind::number;
      return ct;
    default:
      // name, name = , or error
      if (std::isalpha(ch)) {
        ip->putback(ch);
        *ip >> ct.string_value;
        ct.kind = Kind::name;
        return ct;
      }
      error("Bad token!");
      return ct = {Kind::print};
  }
}

Token TokenStream::get_v2() {
  // by default, `operator>>` skips whitespaces
  // and leaves the value of `ch` unchanged if input operation failed
  char ch{0};

  do {  // skip whitespaces except '\n'
    if (!ip->get(ch)) {
      return ct = {Kind::end};
    }
  } while (ch != '\n' && isspace(ch));

  switch (ch) {
    case ';':
    case '\n':
      return ct = {Kind::print};
    case '*':
    case '/':
    case '+':
    case '-':
    case '(':
    case ')':
    case '=':
      return ct = {static_cast<Kind>(ch)};
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '.':
      // put it back into the input stream
      ip->putback(ch);
      *ip >> ct.number_value;
      ct.kind = Kind::number;
      return ct;
    default:
      // name, name = , or error
      if (std::isalpha(ch)) {
        ct.string_value = ch;
        while (ip->get(ch)) {
          if (isalnum(ch)) {
            ct.string_value += ch;
          } else {
            ip->putback(ch);
            break;
          }
        }
        ct.kind = Kind::name;
        return ct;
      }
      error("Bad token!");
      return ct = {Kind::print};
  }
}

TokenStream ts{cin};  // use input from cin

map<string, double> table;

/*
 * Addition and subtraction
 * looking for terms to add/subtract
 */
double expr(bool get) {
  double left = term(get);
  for (;;) {
    switch (ts.current().kind) {
      case Kind::plus:
        left += term(true);
        break;
      case Kind::minus:
        left -= term(true);
        break;
      default:
        return left;
    }
  }
}

/*
 * Similar to `expr()` but handles mult/div
 */
double term(bool get) {
  double left = prim(get);
  for (;;) {
    switch (ts.current().kind) {
      case Kind::mul:
        left *= prim(true);
        break;
      case Kind::div:
        // this tests true for all non-zero values
        // pretty clever I'd say
        if (auto d = prim(true)) {
          left /= d;
          break;
        }
        return error("divide by 0!");
      default:
        return left;
    }
  }
}

/*
 * Handles a **primary**
 * Always reads one more Token than it uses to analyze its primary expression
 */
double prim(bool get) {
  if (get) {
    ts.get_v2();  // read the next token
  }

  switch (ts.current().kind) {
    case Kind::number: {
      double v = ts.current().number_value;
      ts.get_v2();
      return v;
    }
    case Kind::name: {
      double& v = table[ts.current().string_value];
      if (ts.get_v2().kind == Kind::assign) {
        v = expr(true);
      }
      return v;
    }
    case Kind::lp: {
      auto e = expr(true);
      if (ts.current().kind != Kind::rp) {
        return error("')' expected!");
      }
      ts.get_v2();
      return e;
    }
    default:
      return error("primary expected!");
  }
}

void calculate() {
  for (;;) {
    ts.get_v2();
    if (ts.current().kind == Kind::end) break;
    if (ts.current().kind == Kind::print) continue;
    cout << expr(false) << '\n';
  }
}

int main(int argc, char* argv[]) {
  // std::cout << "plus: " << Kind::plus << std::endl;
  // printf("name: %c\n", Kind::name);  // <empty>
  // printf("plus: %c\n", Kind::plus);  // +

  switch (argc) {
    case 1:
      break;
    case 2:
      ts.set_input(new std::istringstream{argv[1]});
      break;
    default:
      error("Too many arguments!");
      return 1;
  }
  table["pi"] = 3.1415926535897932385;
  table["e"] = 2.7182818284590452354;

  calculate();

  return num_of_errors;
}
