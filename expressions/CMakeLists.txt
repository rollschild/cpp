add_executable(calculator)
target_sources(calculator PRIVATE calculator.cc)

add_executable(select_middle_bits)
target_sources(select_middle_bits PRIVATE select_middle_bits.cpp)
