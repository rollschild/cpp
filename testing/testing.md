# Testing

- Unit tests
- Integration tests
- Acceptance tests
- Performance tests
- **stub**
  - if some dependency of the unit under test is irrelevant, an empty implementation can be provided that performs some default behavior
- **test harness**
  - code that executes unit tests
- You should have a single assertion per test
- floating-point assertions
  - since floating-point numbers entail rounding errors,
  - it's not a good idea to check for equality using `operator==`
  - the more robust way is to test whether the difference between the two numbers is arbitrarily small
  - use the `Approx` class from Catch2

## Example: Autonomous Vehicle

- **service bus architecture**

## Frameworks

### Catch

- `REQUIRE_THROWS`: requires that the contained expression throw an exception

### Google Test

#### Test Fixtures

- Google Test's approach is to formulate **test fixture classes** when a common setup is involved
- these fixtuers are classes that inherit from the `::testing::Test` class

## Mocking Frameworks

- **dependency injection**
  - the `AutoBrake` class depends on an `IServiceBus`, which gets injected using the constructor of `AutoBrake`
- Mocking frameworks enable the decoupling of the mock's declaration from the mock's test-specific definition

### Mocking an Interface

- Need to take each `virtual` function of the interface and transmute it into a macro
- for non-`const` methods, use `MOCK_METHOD*`
- for `const` methods, use `MOCK_CONST_METHOD*`

### Expectations

- similar to an assertion for a mock object
- expresses the circumstances in which the mock expects to be called and what it should do in response
- declared with the `EXPECT_CALL` macro
- **naggy mock**
  - default
  - produces warnings for _uninteresting_ calls
- **nice mock**
  - will not produce warnings for _uninteresting_ calls
- **strict mock**
  - will fail the test if _any_ call is made to the mock for which you don't have a corresponding `EXPECT_CALL`

```c++
MockServiceBus naggy_mock; // default
::testing::NiceMock<MockServiceBus> nice_mock;
::testing::StrictMock<MockServiceBus> strict_mock;
```

- General rule: **use nice mocks**

### Matchers

- you can use `::testing::_` object, which matches _any_ value
- a more selective matcher: `::testing::A`
  - will match only if a method is invoked with a particular type of parameter
  - `A<MyType>`
- `::testing::Field`
  - allows you to inspect fields on arguments passed to the mock

### Cardinality

- how many times the mock should respond to calls
- `Times`
  - number of times a mock should expect to be called
  - `Times(Exactly(1))`

### Actions

- specifies how the mock should respond to expectations
- `Invoke`
  - enables you to pass an `Invocable` that will get called with the exact arguments passed into the mock's method
  - **invocation parameters**: parameters the mocked method received at runtime
