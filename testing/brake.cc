#include <cstdio>
#include <functional>
#include <stdexcept>

constexpr void assert_that(bool statement, const char* message) {
  if (!statement) throw std::runtime_error{message};
}

struct SpeedUpdate {
  double velocity_mps;
};

struct CarDetected {
  double distance_m;
  double velocity_mps;
};

struct BrakeCommand {
  double time_to_collision_s;
};

struct ServiceBus {
  void publish(const BrakeCommand&);
};

using SpeedUpdateCallback = std::function<void(const SpeedUpdate&)>;
using CarDetectedCallback = std::function<void(const CarDetected&)>;

// ServiceBus Interface
struct IServiceBus {
  virtual ~IServiceBus() = default;
  virtual void publish(const BrakeCommand&) = 0;
  virtual void subscribe(SpeedUpdateCallback) = 0;
  virtual void subscribe(CarDetectedCallback) = 0;
};

struct MockServiceBus : IServiceBus {
  void publish(const BrakeCommand& cmd) override {
    commands_published++;
    last_command = cmd;
  }
  void subscribe(SpeedUpdateCallback callback) override {
    speed_update_callback = callback;
  }
  void subscribe(CarDetectedCallback callback) override {
    car_detected_callback = callback;
  }

  BrakeCommand last_command{};
  int commands_published{};
  SpeedUpdateCallback speed_update_callback{};
  CarDetectedCallback car_detected_callback{};
};

// This class subscribes to `SpeedUpdate` and `CarDetected` events on the
// service bus
// template <typename T>
struct AutoBrake {
  // constructor
  // this design pattern is called dependency injection
  explicit AutoBrake(IServiceBus& bus)
      : speed_mps{}, collision_threashold_s{5} {
    bus.subscribe(
        [this](const SpeedUpdate& update) { speed_mps = update.velocity_mps; });
    bus.subscribe([this, &bus](const CarDetected& cd) {  // this is the callback
      const auto relative_velocity_mps = speed_mps - cd.velocity_mps;
      const auto time_to_collision_s = cd.distance_m / relative_velocity_mps;
      if (time_to_collision_s > 0 &&
          time_to_collision_s <= collision_threashold_s) {
        bus.publish(BrakeCommand{time_to_collision_s});
      }
    });
  }
  // void observe(const SpeedUpdate& sd) { speed_mps = sd.velocity_mps; }
  // void observe(const CarDetected& cd) {
  //   const auto relative_velocity_mps = speed_mps - cd.velocity_mps;
  //   const auto time_to_collision_s = cd.distance_m / relative_velocity_mps;
  //   if (time_to_collision_s > 0 &&
  //       time_to_collision_s <= collision_threashold_s) {
  //     publish(BrakeCommand{time_to_collision_s});
  //   }
  // }
  void set_collision_threshold_s(double x) {
    if (x < 1) throw std::runtime_error{"Collision less than 1!"};
    collision_threashold_s = x;
  }
  double get_collision_threshold_s() const { return collision_threashold_s; }
  double get_speed_mps() const { return speed_mps; }

 private:
  double collision_threashold_s;
  double speed_mps;
};

void initial_speed_is_zero() {
  // AutoBrake auto_brake{[](const BrakeCommand&) {}};
  MockServiceBus bus{};
  AutoBrake auto_brake{bus};
  assert_that(auto_brake.get_speed_mps() == 0L, "Initial speed not equal to 0");
}
void initial_sensitivity_is_five() {
  // AutoBrake auto_brake{[](const BrakeCommand&) {}};
  MockServiceBus bus{};
  AutoBrake auto_brake{bus};
  assert_that(auto_brake.get_collision_threshold_s() == 5L,
              "initial sensitivity is not 5");
}

// sensitivity must be always be greater than 1
void sensitivity_greater_than_1() {
  // AutoBrake auto_brake{[](const BrakeCommand&) {}};
  MockServiceBus bus{};
  AutoBrake auto_brake{bus};
  try {
    auto_brake.set_collision_threshold_s(0.5L);
  } catch (const std::exception&) {
    return;
  }

  assert_that(false, "no exception thrown");
}

// car's speed should be saved between speed udpates
void speed_is_saved() {
  // AutoBrake auto_brake{[](const BrakeCommand&) {}};
  MockServiceBus bus{};
  AutoBrake auto_brake{bus};
  bus.speed_update_callback(SpeedUpdate{100L});
  assert_that(100L == auto_brake.get_speed_mps(), "speed not saved to 100");
  bus.speed_update_callback(SpeedUpdate{50L});
  assert_that(50L == auto_brake.get_speed_mps(), "speed not saved to 50");
  bus.speed_update_callback(SpeedUpdate{0L});
  assert_that(0L == auto_brake.get_speed_mps(), "speed not saved to 0");
}

void alert_when_imminent() {
  MockServiceBus bus{};
  AutoBrake auto_brake{bus};
  // AutoBrake auto_brake{[&brake_commands_published](const BrakeCommand&) {
  //   brake_commands_published++;
  // }};
  auto_brake.set_collision_threshold_s(10L);
  bus.speed_update_callback(SpeedUpdate{100L});
  bus.car_detected_callback(CarDetected{100L, 0L});
  assert_that(bus.commands_published == 1, "brake commands published not one");
  assert_that(bus.last_command.time_to_collision_s == 1L,
              "time to collision not computed correctly");
}

void no_alert_when_not_imminent() {
  // int brake_commands_published{};
  // AutoBrake auto_brake{[&brake_commands_published](const BrakeCommand&) {
  //   brake_commands_published++;
  // }};
  MockServiceBus bus{};
  AutoBrake auto_brake{bus};
  auto_brake.set_collision_threshold_s(2L);
  bus.speed_update_callback(SpeedUpdate{100L});
  bus.car_detected_callback(CarDetected{1000L, 50L});
  assert_that(bus.commands_published == 0, "brake command published");
}

// it accepts a function pointer
// pay attention to its syntax
void run_test(void (*unit_test)(), const char* name) {
  try {
    unit_test();
    printf("[+] Test %s successful.\n", name);
  } catch (const std::exception& e) {
    printf("[-] Test failure in %s! %s.\n", name, e.what());
  }
}

int main() {
  // ServiceBus bus;
  // AutoBrake auto_brake{[&bus](const auto& cmd) { bus.publish(cmd); }};

  /* while (true) {  // service bus's event loop
   *   auto_brake.observe(SpeedUpdate{10L});
   *   auto_brake.observe(CarDetected{250L, 25L});
   * } */

  run_test(initial_speed_is_zero, "initial speed is 0");
  run_test(initial_sensitivity_is_five, "initial sensitivity is 5");
  run_test(sensitivity_greater_than_1, "sensitivity greater than 1");
  run_test(speed_is_saved, "speed is saved");
  run_test(alert_when_imminent, "alert when imminent");
  run_test(no_alert_when_not_imminent, "no alert when not imminent");
}
